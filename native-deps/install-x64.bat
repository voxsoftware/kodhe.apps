@echo off
del /S x64/package.json
del /S x64/node_modules

cp package.json x64
cd x64 
rem Versión de Electron.
set npm_config_target=1.8.4
rem La arquitectura de Electron, puede ser ia32 o x64.
set npm_config_arch=x64
set npm_config_target_arch=x64
rem Descargar encabezados para Electron.
set npm_config_disturl=https://atom.io/download/electron
rem Informe a node-pre-gyp que estamos construyendo para Electron.
set npm_config_runtime=electron
rem Informe a node-pre-gyp que construya el módulo desde el código fuente.
set npm_config_build_from_source=true
rem Instale todas las dependencias y almacene el caché en ~/.electron-gyp.
set HOME=~/.electron-gyp
rem echo %npm_config_build_from_source%

npm update 
set HOME=
cd ..