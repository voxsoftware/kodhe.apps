

import obfuscator from 'javascript-obfuscator'
var Fs= core.System.IO.Fs
var JavaScriptObfuscator = require('javascript-obfuscator')
import Path from 'path'

var dirs=["functions"]
var dirs2=[""]

var options={
    compact: true,
    controlFlowFlattening: false,
    deadCodeInjection: false,
    debugProtection: false,
    debugProtectionInterval: false,
    disableConsoleOutput: true,
    identifierNamesGenerator: 'hexadecimal',
    log: false,
    renameGlobals: false,
    rotateStringArray: true,
    selfDefending: true,
    stringArray: true,
    stringArrayEncoding: false,
    stringArrayThreshold: 0.75,
    unicodeEscapeSequence: false
}


var obfuscate= async (dir,recursive)=>{
	var base= Path.join(__dirname, "src-out")
	
	var files= await Fs.async.readdir(Path.join(base, dir))
	var file, stat, content, result,ufile
	for(var i=0;i<files.length;i++){
		file=files[i]
		ufile= Path.join(base, dir, file)
		stat= await Fs.async.stat(ufile)
		if(stat.isDirectory(ufile)){
			if(recursive!==false)
			await obfuscate(Path.join(dir, file))
		}
		else if(file.endsWith(".js") && file!="host.execute.js"){
			
			try{
				content= await Fs.async.readFile(ufile,"utf8")
				if(content && content.trim()){
					result=JavaScriptObfuscator.obfuscate(content, options)
					await Fs.async.writeFile(ufile,result.getObfuscatedCode())
				}
			}catch(e){
				vw.error("Error obfuscating: ", ufile, e)
			}
			
		}
	}
	
}

var obfuscateAll= async ()=>{
	var dir
	for(var i=0;i<dirs.length;i++){
		dir=dirs[i]
		await obfuscate(dir)
	}
	for(var i=0;i<dirs2.length;i++){
		dir=dirs2[i]
		await obfuscate(dir, false)
	}
}


module.exports= obfuscateAll()