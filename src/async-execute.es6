
export default async ( funcs)=>{
	if(funcs instanceof Array){
		for(var i=0;i<funcs.length;i++){
			try{
				await funcs[i]()
			}catch(e){
				vw.error(e)
			}
		}
	}
	else{
		await funcs()
	}
}
