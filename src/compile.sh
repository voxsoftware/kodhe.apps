
#!/usr/bin/env bash
# Comando para copiar entre servidor

SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
vox="node vox-core/cli"
$vox -transpile --in "$DIR/backend/App" --out "$DIR/backend-dist/App"
$vox -transpile --in "$DIR/backend/Modules" --out "$DIR/backend-dist/Modules"
