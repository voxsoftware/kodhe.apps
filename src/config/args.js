
var config = {
    "folder": "KodheApps",
    "createManager": true,
    "autoUpdate": true,
    "updateUrl": "https://apps.kodhe.com",
    "port": 49602
}

if(global._defaultArgs){
    
    var Path= require("path")
    var path1= Path.normalize(Path.join(__dirname,".."))
    var pathname= Path.basename(path1)
    if(pathname!="KodheApps")
        config= global._defaultArgs
}
module.exports= config