"use strict";

var _path = _interopRequireDefault(require("path"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var version = '010403';

var F = function () {
  var _ref = _asyncToGenerator(regeneratorRuntime.mark(function _callee(body) {
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            process.env.DEFAULT_DIR = _path.default.join(__dirname, "..");
            G();

          case 2:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, this);
  }));

  return function F(_x) {
    return _ref.apply(this, arguments);
  };
}();

var G = function G() {
  if (global.installedVersion < version) {
    global.mainWindow.reload();
    global.mainWindow.show();
    global.installedVersion = version;
  }
};

module.exports = function (global) {
  F.global = global;
  return F;
};