"use strict";

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var F, eval1;

eval1 = function () {
  var _ref = _asyncToGenerator(regeneratorRuntime.mark(function _callee(uglobal, body, app, idSite) {
    var global, f, F;
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.next = 2;
            return uglobal.getSiteContext(idSite);

          case 2:
            global = _context.sent;
            uglobal = null;

            F = function F() {};

            F.global = global;

            if (!body.code) {
              _context.next = 20;
              break;
            }

            f = eval(body.code);
            if (f) f.global = global;

            if (!(typeof f == "function")) {
              _context.next = 15;
              break;
            }

            _context.next = 12;
            return f(body);

          case 12:
            return _context.abrupt("return", _context.sent);

          case 15:
            _context.next = 17;
            return f;

          case 17:
            return _context.abrupt("return", _context.sent);

          case 18:
            _context.next = 24;
            break;

          case 20:
            if (!body.script) {
              _context.next = 24;
              break;
            }

            _context.next = 23;
            return global.userFunction(body.script).invoke(body);

          case 23:
            return _context.abrupt("return", _context.sent);

          case 24:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, this);
  }));

  return function eval1(_x, _x2, _x3, _x4) {
    return _ref.apply(this, arguments);
  };
}();

F = function () {
  var _ref2 = _asyncToGenerator(regeneratorRuntime.mark(function _callee2(body) {
    var global, idSite, denied, prefix, ecode, ucode, app, appInfo;
    return regeneratorRuntime.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            global = F.global;
            idSite = global.UserContext.idSite = body.idSite || "user-api";
            denied = false;
            prefix = new Buffer(idSite).toString("hex");
            ecode = '';

            if (body.kodhe_apps_code) {
              ecode = body.kodhe_apps_code;
            }

            if (!(ecode && !denied)) {
              _context2.next = 13;
              break;
            }

            _context2.t0 = prefix;
            _context2.next = 10;
            return global.UserFunction("machine.id").invoke();

          case 10:
            _context2.t1 = _context2.sent.id;
            ucode = _context2.t0 + _context2.t1;
            if (ucode != ecode) denied = true;

          case 13:
            if (!denied && !ecode) denied = true;

            if (!denied) {
              _context2.next = 16;
              break;
            }

            throw global.Exception.create("Access denied to use machine resources").putCode("ACCESS_DENIED");

          case 16:
            if (!body.appinfo) {
              _context2.next = 23;
              break;
            }

            appInfo = global.UserFunction("get.appcache");
            _context2.next = 20;
            return appInfo.load();

          case 20:
            _context2.next = 22;
            return appInfo.execute(body);

          case 22:
            app = _context2.sent;

          case 23:
            _context2.next = 25;
            return eval1(global, body, app, idSite);

          case 25:
            return _context2.abrupt("return", _context2.sent);

          case 26:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2, this);
  }));

  return function F(_x5) {
    return _ref2.apply(this, arguments);
  };
}();

module.exports = function (global) {
  F.global = global;
  return F;
};