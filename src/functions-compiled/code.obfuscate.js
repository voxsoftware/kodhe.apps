"use strict";

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

module.exports = function (global) {
  return function () {
    var _ref = _asyncToGenerator(regeneratorRuntime.mark(function _callee(body) {
      'javascript-obfuscator';

      var JavaScriptObfuscator, obfuscationResult;
      return regeneratorRuntime.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              _context.next = 2;
              return global.UserContext.require('javascript-obfuscator', '0.17.1');

            case 2:
              JavaScriptObfuscator = _context.sent;
              obfuscationResult = JavaScriptObfuscator.obfuscate(body.code, {
                reservedNames: ["global", "F"],
                compact: true,
                controlFlowFlattening: false,
                deadCodeInjection: false,
                debugProtection: false,
                debugProtectionInterval: false,
                disableConsoleOutput: false,
                identifierNamesGenerator: 'hexadecimal',
                renameGlobals: false,
                rotateStringArray: true,
                selfDefending: false,
                stringArray: true,
                stringArrayEncoding: true,
                stringArrayThreshold: 0.75,
                unicodeEscapeSequence: false
              });
              return _context.abrupt("return", obfuscationResult.getObfuscatedCode());

            case 5:
            case "end":
              return _context.stop();
          }
        }
      }, _callee, this);
    }));

    return function (_x) {
      return _ref.apply(this, arguments);
    };
  }();
};