"use strict";

var _os = _interopRequireDefault(require("os"));

var _path = _interopRequireDefault(require("path"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var Fs = core.System.IO.Fs;
var F;

F = function () {
  var _ref = _asyncToGenerator(regeneratorRuntime.mark(function _callee(body) {
    var global, app, kodheappsFolder, home, documentFolder, realAppFolder, appFolder, port, name, id, cid, url, req, data, response, a, result, p0, prefix, customicon, customiconw, info, w, ic, electron, BrowserWindow, retry, machineId;
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            global = F.global;
            app = body.app || body;
            kodheappsFolder = require(__dirname + "/../config/args").folder;
            home = require("os").homedir();
            documentFolder = _path.default.normalize(home + '/Documents/');
            realAppFolder = _path.default.join(documentFolder, kodheappsFolder);
            appFolder = _path.default.join(documentFolder, kodheappsFolder, 'org.kodhe.apps');
            process.env.BASE_DIR = appFolder;
            port = require(__dirname + "/../config/0port");

            if (app.path) {
              name = _path.default.basename(app.path);
              app.link = 'http://127.0.0.1:' + port + '/site/' + name;
            }

            if (!app.link) {
              _context.next = 115;
              break;
            }

            _context.next = 13;
            return global.UserFunction("machine.id").invoke();

          case 13:
            id = _context.sent.id;
            url = app.link + "/static/kodhe.app.json";
            req = new core.VW.Http.Request(url);
            _context.prev = 16;
            _context.next = 19;
            return req.getResponseAsync();

          case 19:
            response = _context.sent;
            a = true;
            data = response.body;

            if (typeof data == "string") {
              data = JSON.parse(data);
            }

            if (data.name) {
              _context.next = 25;
              break;
            }

            throw global.Exception.create("No name provided to application");

          case 25:
            if (!data.error) {
              _context.next = 27;
              break;
            }

            throw data.error;

          case 27:
            if (!(data.localcache == 'preferred' || data.localcache == "always")) {
              _context.next = 41;
              break;
            }

            if (!(!app.path || app.updating === "automatic" || body.update)) {
              _context.next = 41;
              break;
            }

            _context.prev = 29;
            _context.next = 32;
            return global.UserFunction("app.cache").invoke({
              url: data.cacheurl,
              name: data.name
            });

          case 32:
            result = _context.sent;
            _context.next = 38;
            break;

          case 35:
            _context.prev = 35;
            _context.t0 = _context["catch"](29);
            console.error("Failed updating Local app: ", _context.t0);

          case 38:
            app.path = _path.default.join(realAppFolder, data.name);
            app.link = "http://127.0.0.1:" + port + "/site/" + data.name;
            if (data.windowicon) app.windowicon = data.windowicon;

          case 41:
            _context.next = 43;
            return global.UserFunction("app.start.process").invoke(data);

          case 43:
            p0 = _context.sent;
            prefix = data.id || data.name;
            prefix = new Buffer(prefix).toString("hex");
            cid = prefix + id;
            customicon = app.windowicon && app.windowicon.png && app.path ? _path.default.join(app.path, app.windowicon.png) : null;
            customiconw = app.windowicon && app.windowicon.ico && app.path ? _path.default.join(app.path, app.windowicon.ico) : null;
            if (req.innerRequest && req.innerRequest.callback) delete req.innerRequest.callback;
            global.publicContext.apps = global.publicContext.apps || [];
            info = global.publicContext.apps[data.name] = global.publicContext.apps[data.name] || {};

            if (!(data.localcache && data.scripts && data.scripts.open)) {
              _context.next = 72;
              break;
            }

            _context.prev = 53;
            req = new core.VW.Http.Request(app.link + data.scripts.open);
            req.method = 'POST';
            req.body = {
              app: app,
              port: p0.port,
              kodhe_apps_code: cid
            };
            _context.next = 59;
            return req.getResponseAsync();

          case 59:
            response = _context.sent;

            if (response.body) {
              _context.next = 62;
              break;
            }

            throw global.Exception.create("Invalid RESPONSE");

          case 62:
            _context.next = 67;
            break;

          case 64:
            _context.prev = 64;
            _context.t1 = _context["catch"](53);
            throw _context.t1;

          case 67:
            _context.prev = 67;
            if (req.innerRequest && req.innerRequest.callback) delete req.innerRequest.callback;
            return _context.finish(67);

          case 70:
            _context.next = 77;
            break;

          case 72:
            data.window = data.window || {};
            ic = customicon || global.iconpath;
            w = info.window;

            if (!w) {
              electron = require('electron');
              BrowserWindow = electron.BrowserWindow;
              w = new BrowserWindow({
                width: data.window.width || 600,
                height: data.window.height || 580,
                "minWidth": data.window.minWidth || 560,
                "minHeight": data.window.minHeight || 580,
                maxWidth: data.window.maxWidth,
                maxHeight: data.window.maxHeight,
                show: false,
                icon: ic,
                nodeIntegration: true
              });
              info.window = w;

              if (_os.default.platform() == "win32") {
                if (customiconw) {
                  w.setAppDetails({
                    appId: data.versionedName || data.name,
                    appIconPath: customiconw,
                    relaunchCommand: '"' + process.execPath + '"' + " --exec " + data.name,
                    relaunchDisplayName: data.secureDisplayName || data.name
                  });
                }
              }

              w.loadURL(app.link + "?kodhe_apps_code=" + encodeURIComponent(cid) + "&port=" + p0.port + "&port1=" + port);
              w.on("close", function () {
                delete info.window;
              });
            }

            if (!body.hidden) w.show();

          case 77:
            retry = 2;

          case 78:
            if (!(retry > 0)) {
              _context.next = 105;
              break;
            }

            _context.prev = 79;
            _context.next = 82;
            return global.UserContext.require("node-machine-id", "1.1.10");

          case 82:
            machineId = _context.sent;
            data.id = machineId.machineIdSync();
            req = new core.VW.Http.Request(app.link + "/api/function/c/app.localmode");
            req.method = 'POST';
            req.body = {
              app: data,
              id: data.id,
              port: p0.port,
              port1: port,
              kodhe_apps_code: cid
            };
            _context.next = 89;
            return req.getResponseAsync();

          case 89:
            response = _context.sent;

            if (response.body) {
              _context.next = 92;
              break;
            }

            throw global.Exception.create("Invalid RESPONSE");

          case 92:
            retry = 0;
            _context.next = 100;
            break;

          case 95:
            _context.prev = 95;
            _context.t2 = _context["catch"](79);
            retry--;

            if (!(retry == 0)) {
              _context.next = 100;
              break;
            }

            throw _context.t2;

          case 100:
            _context.prev = 100;
            if (req.innerRequest && req.innerRequest.callback) delete req.innerRequest.callback;
            return _context.finish(100);

          case 103:
            _context.next = 78;
            break;

          case 105:
            _context.next = 112;
            break;

          case 107:
            _context.prev = 107;
            _context.t3 = _context["catch"](16);

            if (!a) {
              _context.next = 111;
              break;
            }

            throw global.Exception.create("No es una aplicación válida: " + _context.t3.message).putCode("INVALID_APP");

          case 111:
            throw _context.t3;

          case 112:
            _context.prev = 112;
            if (req.innerRequest && req.innerRequest.callback) delete req.innerRequest.callback;
            return _context.finish(112);

          case 115:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, this, [[16, 107, 112, 115], [29, 35], [53, 64, 67, 70], [79, 95, 100, 103]]);
  }));

  return function F(_x) {
    return _ref.apply(this, arguments);
  };
}();

var y = function y(local) {
  local.iconpath = global.iconpath;
};

module.exports = function (global) {
  y(global);
  F.global = global;
  return F;
};