"use strict";

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var F = function () {
  var _ref = _asyncToGenerator(regeneratorRuntime.mark(function _callee(body) {
    var global, crons, apps, i, cw;
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            global = F.global;
            crons = global.publicContext.crons = global.publicContext.crons || {};

            if (global.context && global.context.response) {
              global.context.response.setTimeout(300000);
            }

            if (!(body && (body.firsttime || body.exec))) {
              _context.next = 22;
              break;
            }

            _context.next = 6;
            return global.UserFunction("app.list").invoke({});

          case 6:
            apps = _context.sent;
            i = 0;

          case 8:
            if (!(i < apps.length)) {
              _context.next = 22;
              break;
            }

            console.info("APP FOUND. ", apps[i]);

            if (!body.firsttime) {
              _context.next = 16;
              break;
            }

            if (!(body.startup && apps[i].startup == "boot" || apps[i].startup == 'always')) {
              _context.next = 14;
              break;
            }

            _context.next = 14;
            return global.UserFunction("app.check").invoke({
              app: apps[i],
              hidden: body.exec ? body.exec != apps[i].name : true
            });

          case 14:
            _context.next = 19;
            break;

          case 16:
            if (!(apps[i].name == body.exec)) {
              _context.next = 19;
              break;
            }

            _context.next = 19;
            return global.UserFunction("app.check").invoke({
              app: apps[i],
              update: body.update,
              hidden: !!body.hidden
            });

          case 19:
            i++;
            _context.next = 8;
            break;

          case 22:
            cw = require(__dirname + "/../config/args");

            if (!cw.autoUpdate) {
              _context.next = 35;
              break;
            }

            if (!(body.startup || !global.publicContext.cronUpdated || Date.now() - global.publicContext.cronUpdated > 60 * 60 * 1000)) {
              _context.next = 35;
              break;
            }

            _context.prev = 25;
            _context.next = 28;
            return global.UserFunction("app.check").invoke({
              app: {
                link: cw.updateUrl,
                name: 'org.kodhe.apps',
                id: 'org.kodhe.apps'
              },
              update: true,
              hidden: true
            });

          case 28:
            _context.next = 32;
            break;

          case 30:
            _context.prev = 30;
            _context.t0 = _context["catch"](25);

          case 32:
            _context.prev = 32;
            global.publicContext.cronUpdated = Date.now();
            return _context.finish(32);

          case 35:
            if (!body.exit) {
              _context.next = 37;
              break;
            }

            return _context.abrupt("return");

          case 37:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, this, [[25, 30, 32, 35]]);
  }));

  return function F(_x) {
    return _ref.apply(this, arguments);
  };
}();

module.exports = function (global) {
  F.global = global;
  return F;
};