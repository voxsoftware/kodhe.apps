"use strict";

var _F;

_F = function F(body) {
  var app;

  try {
    app = _F.global.publicContext.apps[body.name || body.idSite];
  } catch (e) {}

  return app;
};

module.exports = function (global) {
  _F.global = global;
  return _F;
};