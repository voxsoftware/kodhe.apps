"use strict";

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var F;

F = function () {
  var _ref = _asyncToGenerator(regeneratorRuntime.mark(function _callee(data) {
    var uglobal, pr_name, task, reopen, port;
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            uglobal = F.global;

            if (!(data.name == "org.kodhe.apps")) {
              _context.next = 3;
              break;
            }

            return _context.abrupt("return");

          case 3:
            data.processName = data.processName || "default";
            pr_name = data.processName;
            uglobal.publicContext.prs = uglobal.publicContext.prs || {};
            uglobal.publicContext.prsi = uglobal.publicContext.prsi || {};

          case 7:
            if (!uglobal.publicContext.prsi[pr_name]) {
              _context.next = 12;
              break;
            }

            _context.next = 10;
            return core.VW.Task.sleep(100);

          case 10:
            _context.next = 7;
            break;

          case 12:
            _context.prev = 12;

            if (uglobal.publicContext.prs[pr_name]) {
              _context.next = 28;
              break;
            }

            uglobal.publicContext.prsi[pr_name] = true;
            task = new core.VW.Task();
            reopen = true;
            port = data.port || "0";

            if (!(port == 49603 && pr_name != "default")) {
              _context.next = 20;
              break;
            }

            throw uglobal.Exception.create("Port " + port + " is reserved");

          case 20:
            if (pr_name == "default") {
              port = undefined;
              reopen = true;
            }

            uglobal.publicContext.prs[pr_name] = global.startNewProcess(port, reopen, task);

            if (pr_name == "default") {
              uglobal.publicContext.prs[data.name] = uglobal.publicContext.prs[pr_name];
            }

            _context.next = 25;
            return task;

          case 25:
            uglobal.publicContext.prs[pr_name].port = _context.sent;
            _context.next = 29;
            break;

          case 28:
            if (pr_name == "default") {
              uglobal.publicContext.prs[data.name] = uglobal.publicContext.prs[pr_name];
            }

          case 29:
            _context.next = 34;
            break;

          case 31:
            _context.prev = 31;
            _context.t0 = _context["catch"](12);
            throw _context.t0;

          case 34:
            _context.prev = 34;
            delete uglobal.publicContext.prsi[pr_name];
            return _context.finish(34);

          case 37:
            return _context.abrupt("return", {
              port: uglobal.publicContext.prs[pr_name].port
            });

          case 38:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, this, [[12, 31, 34, 37]]);
  }));

  return function F(_x) {
    return _ref.apply(this, arguments);
  };
}();

module.exports = function (global) {
  F.global = global;
  return F;
};