"use strict";

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var F;

var Os = require("os");

F = function () {
  var _ref = _asyncToGenerator(regeneratorRuntime.mark(function _callee(body) {
    var global, idSite, denied, prefix, ecode, cw, ucode, name, uname, p, keys;
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            global = F.global;
            global.publicContext.prs = global.publicContext.prs || {};
            body.idSite = body.idSite || body.name;
            idSite = global.UserContext.idSite = body.idSite || "user-api";
            denied = false;
            prefix = new Buffer(idSite).toString("hex");
            ecode = '';

            if (body.kodhe_apps_code) {
              ecode = body.kodhe_apps_code;
            }

            cw = require(__dirname + "/../config/args");

            if (!(ecode && !denied)) {
              _context.next = 16;
              break;
            }

            _context.t0 = prefix;
            _context.next = 13;
            return global.UserFunction("machine.id").invoke();

          case 13:
            _context.t1 = _context.sent.id;
            ucode = _context.t0 + _context.t1;
            if (ucode != ecode) denied = true;

          case 16:
            if (!denied && !ecode) denied = true;

            if (!denied) {
              _context.next = 19;
              break;
            }

            throw global.Exception.create("Access denied to use machine resources").putCode("ACCESS_DENIED");

          case 19:
            name = body.processName || body.name;
            uname = body.processName || "default";
            delete global.publicContext.prsi[name];
            p = global.publicContext.prs[name];

            if (p) {
              if (p.port == 49603) {
                delete global.publicContext.prs[name];
                keys = Object.keys(global.publicContext.prs);

                if (keys.length == 1 && keys[0] == "default") {
                  if (body.quit) {
                    if (cw.service === false) {
                      if (Os.platform() != "darwin") require("electron").app.quit();
                      process._force = true;
                      process.exit(0);
                      process.kill(process.pid);
                    }
                  }
                }
              } else {
                p.kill('SIGINT');
                delete global.publicContext.prs[name];
                keys = Object.keys(global.publicContext.prs);

                if (keys.length == 0) {
                  if (body.quit) {
                    if (cw.service === false) {
                      if (Os.platform() != "darwin") require("electron").app.quit();
                      process._force = true;
                      process.exit(0);
                      process.kill(process.pid);
                    }
                  }
                }
              }
            }

          case 24:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, this);
  }));

  return function F(_x) {
    return _ref.apply(this, arguments);
  };
}();

module.exports = function (global) {
  F.global = global;
  return F;
};