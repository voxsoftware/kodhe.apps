"use strict";

var _path = _interopRequireDefault(require("path"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var Fs = core.System.IO.Fs;

var F = function () {
  var _ref = _asyncToGenerator(regeneratorRuntime.mark(function _callee2(body) {
    var global, maindir, dirs1, dirs2, files, dir1, updated, procesar, intBuffer, name, buf1, buf2, id, file, ucontent, ast, parser, i;
    return regeneratorRuntime.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            global = F.global;
            maindir = _path.default.join(__dirname, "..");
            dirs1 = {}, dirs2 = {};
            files = [];
            updated = body.updated;

            procesar = function () {
              var _ref2 = _asyncToGenerator(regeneratorRuntime.mark(function _callee(dirname) {
                var dirs, stat, udir, re, i;
                return regeneratorRuntime.wrap(function _callee$(_context) {
                  while (1) {
                    switch (_context.prev = _context.next) {
                      case 0:
                        _context.next = 2;
                        return Fs.async.readdir(dirname);

                      case 2:
                        dirs = _context.sent;
                        i = 0;

                      case 4:
                        if (!(i < dirs.length)) {
                          _context.next = 23;
                          break;
                        }

                        if (dirs[i].startsWith(".")) {
                          _context.next = 20;
                          break;
                        }

                        udir = _path.default.join(dirname, dirs[i]);
                        _context.next = 9;
                        return Fs.async.stat(udir);

                      case 9:
                        stat = _context.sent;

                        if (!stat.isDirectory()) {
                          _context.next = 18;
                          break;
                        }

                        re = _path.default.relative(maindir, dirname);

                        if (!(re != "dependencies" && re != "functions-compiled" && re != "node_modules" && re != "async-execute.es6" && re != "compile.sh" && re != "yarn.lock")) {
                          _context.next = 16;
                          break;
                        }

                        dirs1[udir] = stat;
                        _context.next = 16;
                        return procesar(udir);

                      case 16:
                        _context.next = 20;
                        break;

                      case 18:
                        if (stat.mtime.getTime() > updated) updated = stat.mtime.getTime();
                        files.push({
                          file: udir,
                          stat: stat
                        });

                      case 20:
                        i++;
                        _context.next = 4;
                        break;

                      case 23:
                      case "end":
                        return _context.stop();
                    }
                  }
                }, _callee, this);
              }));

              return function procesar(_x2) {
                return _ref2.apply(this, arguments);
              };
            }();

            _context2.next = 8;
            return procesar(maindir);

          case 8:
            global.context.response.setHeader("x-app-updated", updated);
            global.context.response.setHeader("x-app-version", global.constants.version);
            dirs2 = dirs1;

            if (!(updated <= body.updated)) {
              _context2.next = 13;
              break;
            }

            return _context2.abrupt("return", global.context.response.end());

          case 13:
            for (id in dirs2) {
              name = _path.default.relative(maindir, id);

              if (name && dirs2[id]) {
                name = '\0' + name;
                buf1 = Buffer.from(name);
                buf2 = Buffer.from(core.safeJSON.stringify(dirs2[id]));
                intBuffer = Buffer.alloc(4);
                intBuffer.writeInt32LE(0, 0);
                global.context.response.write(intBuffer);
                intBuffer = Buffer.alloc(4);
                intBuffer.writeInt32LE(buf1.length, 0);
                global.context.response.write(intBuffer);
                global.context.response.write(buf1);
                intBuffer = Buffer.alloc(4);
                intBuffer.writeInt32LE(buf2.length, 0);
                global.context.response.write(intBuffer);
                global.context.response.write(buf2);
              }
            }

            parser = global.publicContext.ecmaParser = global.publicContext.ecmaParser || new core.VW.Ecma2015.Parser();
            i = 0;

          case 16:
            if (!(i < files.length)) {
              _context2.next = 68;
              break;
            }

            file = files[i];
            name = _path.default.relative(maindir, file.file);
            ucontent = null;

            if (!(name.startsWith("dependencies/") || name.startsWith("node_modules/") || name.startsWith("functions-compiled/"))) {
              _context2.next = 22;
              break;
            }

            return _context2.abrupt("continue", 65);

          case 22:
            if (!(name.startsWith("functions") && (name.endsWith(".es6") || name.endsWith(".js")))) {
              _context2.next = 43;
              break;
            }

            if (!name.endsWith(".es6")) {
              _context2.next = 34;
              break;
            }

            if (!Fs.sync.exists(file.file.substring(0, file.file.length - 3) + "js")) {
              _context2.next = 26;
              break;
            }

            return _context2.abrupt("continue", 65);

          case 26:
            _context2.next = 28;
            return Fs.async.readFile(file.file, 'utf8');

          case 28:
            ucontent = _context2.sent;
            name = name.substring(0, name.length - 3) + "js";
            ast = parser.parse(ucontent);
            ucontent = ast.code;
            _context2.next = 37;
            break;

          case 34:
            _context2.next = 36;
            return Fs.async.readFile(file.file, 'utf8');

          case 36:
            ucontent = _context2.sent;

          case 37:
            if (!(!name.endsWith("/host.execute.js") && !name.endsWith("/get.appcache.js"))) {
              _context2.next = 41;
              break;
            }

            _context2.next = 40;
            return global.UserFunction("code.obfuscate").invoke({
              code: ucontent
            });

          case 40:
            ucontent = _context2.sent;

          case 41:
            ucontent = Buffer.from(ucontent);
            ast = null;

          case 43:
            buf1 = Buffer.from(name);
            buf2 = Buffer.from(core.safeJSON.stringify(file.stat));
            intBuffer = Buffer.alloc(4);
            intBuffer.writeInt32LE(ucontent ? ucontent.length : file.stat.size, 0);
            global.context.response.write(intBuffer);
            intBuffer = Buffer.alloc(4);
            intBuffer.writeInt32LE(buf1.length, 0);
            global.context.response.write(intBuffer);
            global.context.response.write(buf1);
            intBuffer = Buffer.alloc(4);
            intBuffer.writeInt32LE(buf2.length, 0);
            global.context.response.write(intBuffer);
            global.context.response.write(buf2);

            if (ucontent) {
              _context2.next = 64;
              break;
            }

            _context2.t0 = global.context.response;
            _context2.next = 60;
            return Fs.async.readFile(file.file);

          case 60:
            _context2.t1 = _context2.sent;

            _context2.t0.write.call(_context2.t0, _context2.t1);

            _context2.next = 65;
            break;

          case 64:
            global.context.response.write(ucontent);

          case 65:
            i++;
            _context2.next = 16;
            break;

          case 68:
            global.context.response.end();

          case 69:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2, this);
  }));

  return function F(_x) {
    return _ref.apply(this, arguments);
  };
}();

module.exports = function (global) {
  F.global = global;
  return F;
};