"use strict";

var _os = _interopRequireDefault(require("os"));

var _path = _interopRequireDefault(require("path"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var Fs = core.System.IO.Fs;

var F = function () {
  var _ref = _asyncToGenerator(regeneratorRuntime.mark(function _callee(body) {
    var mainWindow, commandLine, next, startArgs, cw, revise, home, kodhe_hide, content, cron;
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            body = body || {};
            mainWindow = global.mainWindow;
            commandLine = body.commandLine;
            cw = require(__dirname + "/../config/args");

            revise = function revise(cmd) {
              next = '';
              var startArgs;
              var Command = new core.VW.CommandLine.Parser();
              Command.addParameter('hide', true, false);
              Command.addParameter('check', true, false);
              Command.addParameter('startup', true, false);
              Command.addParameter('exec', true, null);
              Command.addParameter('appargs', true, null);
              Command.parse(null, false, cmd);
              startArgs = Command.getAllParameters();
              startArgs.startup = startArgs.startup !== false;
              if (startArgs.exec) startArgs.hide = true;
              console.log("START ARGS:", startArgs);
              return {
                next: next,
                startArgs: startArgs
              };
            };

            if (!body.getfunc) {
              _context.next = 7;
              break;
            }

            return _context.abrupt("return", revise);

          case 7:
            if (!true) {
              _context.next = 39;
              break;
            }

            console.info("commandLine", commandLine);
            next = revise(commandLine);
            startArgs = next.startArgs;
            next = next.next;

            if (mainWindow && next) {
              mainWindow.loadURL(global.url + next);
            }

            if (!(startArgs.check !== false)) {
              _context.next = 35;
              break;
            }

            home = _os.default.homedir();
            kodhe_hide = _path.default.join(home, "kodhe-start-hide");

            if (!Fs.sync.exists(kodhe_hide)) {
              _context.next = 35;
              break;
            }

            _context.next = 19;
            return Fs.async.readFile(kodhe_hide, 'utf8');

          case 19:
            content = _context.sent;

            if (content == 1) {
              startArgs.hide = true;
            }

            _context.prev = 21;
            _context.next = 24;
            return Fs.async.unlink(kodhe_hide);

          case 24:
            _context.next = 35;
            break;

          case 26:
            _context.prev = 26;
            _context.t0 = _context["catch"](21);
            _context.prev = 28;
            _context.next = 31;
            return Fs.async.writeFile(kodhe_hide, '0');

          case 31:
            _context.next = 35;
            break;

          case 33:
            _context.prev = 33;
            _context.t1 = _context["catch"](28);

          case 35:
            if (cw.start && cw.start.name && !startArgs.exec) {
              startArgs.exec = cw.start.name;
              startArgs.hide = true;
            }

            if (mainWindow && startArgs.hide === false) {
              mainWindow.show();
              if (mainWindow.isMinimized()) mainWindow.restore();
              mainWindow.focus();
            }

            console.info("startArgs", startArgs);

            if (startArgs.exec) {
              cron = function cron(startArgs) {
                var req = new core.VW.Http.Request(global.url + "api/function/c/cron.0tasks");
                req.analizeResponse = false;
                req.method = 'POST';
                req.contentType = 'application/json';
                req.body = startArgs;
                req.getResponseAsync();
              };

              startArgs.exit = true;
              cron(startArgs);
            }

          case 39:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, this, [[21, 26], [28, 33]]);
  }));

  return function F(_x) {
    return _ref.apply(this, arguments);
  };
}();

module.exports = function (global) {
  return F;
};