"use strict";

var _url = _interopRequireDefault(require("url"));

var _path = _interopRequireDefault(require("path"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var Global;

var F = function () {
  var _ref = _asyncToGenerator(regeneratorRuntime.mark(function _callee(body) {
    var global, url, uri, request, parts, server;
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            global = F.global;
            url = global.context.request.url;
            uri = _url.default.parse(url);
            request = global.context.request;
            parts = uri.pathname.split("/").filter(function (a) {
              return !!a;
            });

            if (!(parts[0] == "static" && parts[1] == "kodhe.app.json")) {
              _context.next = 14;
              break;
            }

            server = new core.VW.Http.StaticServe.Server();
            server.addPath(_path.default.join(__dirname, ".."));
            global.context.request.url = "/kodhe.app.json";
            _context.next = 11;
            return server.handle(global.context);

          case 11:
            global.context._body.stopdefault = true;
            _context.next = 17;
            break;

          case 14:
            _context.next = 16;
            return global.context.continueAsync();

          case 16:
            return _context.abrupt("return", _context.sent);

          case 17:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, this);
  }));

  return function F(_x) {
    return _ref.apply(this, arguments);
  };
}();

module.exports = function (global) {
  F.global = global;
  return F;
};