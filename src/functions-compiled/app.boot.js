"use strict";

var _os = _interopRequireDefault(require("os"));

var _path = _interopRequireDefault(require("path"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var F = function F(body) {};

var Fs = core.System.IO.Fs;

var L = function () {
  var _ref = _asyncToGenerator(regeneratorRuntime.mark(function _callee(body) {
    var uglobal, finish, electron, app, home, documentFolder, dirsConfig, content, line, doc, kodheappsFolder, appFolder, mainWindow, cw, defaultApps, n1, n2, stat, d2, dirs, i, y, next, startArgs, revise, url, port, BrowserWindow, path, iconpath, iconpath2, kodhe_hide, createWindow, req, cron, Int;
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            createWindow = function _ref2() {
              mainWindow = new BrowserWindow({
                width: 650,
                height: 580,
                "minWidth": 560,
                "minHeight": 580,
                icon: iconpath,
                nodeIntegration: true,
                show: false
              });
              global.mainWindow = mainWindow;
              mainWindow.hide();

              if (_os.default.platform() == "win32") {
                mainWindow.setAppDetails({
                  appId: "org.kodhe.apps",
                  appIconPath: _path.default.join(__dirname, "..", "build", "icons", "app.ico"),
                  relaunchCommand: '"' + process.execPath + '"',
                  relaunchDisplayName: "Kodhe Apps"
                });
              }

              var addGoodMenu = function addGoodMenu() {
                var Menu = require("electron").Menu;

                var subm = [{
                  label: "About Application",
                  selector: "orderFrontStandardAboutPanel:"
                }, {
                  type: "separator"
                }, {
                  label: "Quit",
                  accelerator: "CmdOrCtrl+Q",
                  click: function click() {
                    global.appQuit();
                  }
                }];

                if (_os.default.platform() != "darwin") {
                  subm = [{
                    label: "Quit",
                    accelerator: "CmdOrCtrl+Q",
                    click: function click() {
                      global.appQuit();
                    }
                  }];
                }

                var template = [{
                  label: "Application",
                  submenu: subm
                }, {
                  label: "Edit",
                  submenu: [{
                    label: "Undo",
                    accelerator: "CmdOrCtrl+Z",
                    selector: "undo:"
                  }, {
                    label: "Redo",
                    accelerator: "Shift+CmdOrCtrl+Z",
                    selector: "redo:"
                  }, {
                    type: "separator"
                  }, {
                    label: "Cut",
                    accelerator: "CmdOrCtrl+X",
                    selector: "cut:"
                  }, {
                    label: "Copy",
                    accelerator: "CmdOrCtrl+C",
                    selector: "copy:"
                  }, {
                    label: "Paste",
                    accelerator: "CmdOrCtrl+V",
                    selector: "paste:"
                  }, {
                    label: "Select All",
                    accelerator: "CmdOrCtrl+A",
                    selector: "selectAll:"
                  }]
                }];
                var template2 = [{
                  label: "Edit",
                  submenu: [{
                    label: "Undo",
                    accelerator: "CmdOrCtrl+Z",
                    selector: "undo:"
                  }, {
                    label: "Redo",
                    accelerator: "Shift+CmdOrCtrl+Z",
                    selector: "redo:"
                  }, {
                    type: "separator"
                  }, {
                    label: "Cut",
                    accelerator: "CmdOrCtrl+X",
                    selector: "cut:"
                  }, {
                    label: "Copy",
                    accelerator: "CmdOrCtrl+C",
                    selector: "copy:"
                  }, {
                    label: "Paste",
                    accelerator: "CmdOrCtrl+V",
                    selector: "paste:"
                  }, {
                    label: "Select All",
                    accelerator: "CmdOrCtrl+A",
                    selector: "selectAll:"
                  }]
                }];

                if (_os.default.platform() == "darwin") {
                  Menu.setApplicationMenu(Menu.buildFromTemplate(template));
                } else {
                  mainWindow.setMenu(Menu.buildFromTemplate(template));
                }
              };

              addGoodMenu();
              setTimeout(function () {
                if (startArgs.hide === false && cw.createManager) mainWindow.show();
              }, 2000);
              mainWindow.on('close', function (event) {
                if (!app.isQuiting) {
                  event.preventDefault();
                  mainWindow.reload();
                  mainWindow.hide();
                }

                return false;
              });
              uglobal.publicContext["127.0.0.1"] = uglobal.publicContext["127.0.0.1"] || {};

              var finish = uglobal.publicContext["127.0.0.1"].finish = function () {
                if (global.onexit) {
                  var t = require("./async-execute").default(global.onexit);

                  var task = new core.VW.Task();

                  if (t.then) {
                    t.then(function () {
                      task.finish();
                      process.exit(0);
                      process.kill(process.pid);
                    }).catch(function (e) {
                      task.exception = e;
                      task.finish();
                      process.exit(0);
                      process.kill(process.pid);
                    });
                  } else {
                    process.exit(0);
                    process.kill(process.pid);
                  }

                  return task;
                } else {
                  process.exit(0);
                  process.kill(process.pid);
                }
              };

              global.appQuit = function () {
                app.isQuiting = true;
                app.quit();
                finish();
              };

              if (cw.createManager) {
                var appIcon = new electron.Tray(iconpath2);
                var contextMenu = electron.Menu.buildFromTemplate([{
                  label: 'Abrir gestor',
                  click: function click() {
                    mainWindow.show();
                  }
                }]);
                appIcon.setContextMenu(contextMenu);
              }

              mainWindow.on('show', function () {
                appIcon && appIcon.setHighlightMode('selection');
              });

              if (cw.createManager) {
                mainWindow.loadURL(url);
              }

              mainWindow.on('closed', function () {
                mainWindow = null;
              });
            };

            uglobal = F.global;
            electron = require('electron');
            app = electron.app;
            home = require("os").homedir();
            documentFolder = _path.default.normalize(home + '/Documents/');

            if (!(_os.default.platform() == "linux" && !Fs.sync.exists(documentFolder))) {
              _context.next = 28;
              break;
            }

            dirsConfig = _path.default.join(home, ".config", "user-dirs.dirs");

            if (!Fs.sync.exists(dirsConfig)) {
              _context.next = 21;
              break;
            }

            _context.next = 11;
            return Fs.async.readFile(dirsConfig, 'utf8');

          case 11:
            content = _context.sent;
            content = content.split("\n");

          case 13:
            if (!((line = content.shift()) !== undefined)) {
              _context.next = 21;
              break;
            }

            if (!line.startsWith("XDG_DOCUMENTS_DIR")) {
              _context.next = 19;
              break;
            }

            doc = line.split("=")[1];
            doc = doc.replace(/\$([\w|\_]+)/ig, function (a, b) {
              return process.env[b];
            });
            if (doc.startsWith('"')) doc = JSON.parse(doc);
            return _context.abrupt("break", 21);

          case 19:
            _context.next = 13;
            break;

          case 21:
            if (!doc) {
              _context.next = 26;
              break;
            }

            _context.next = 24;
            return Fs.async.symlink(doc, documentFolder.substring(0, documentFolder.length - (documentFolder.endsWith("/") ? 1 : 0)));

          case 24:
            _context.next = 28;
            break;

          case 26:
            _context.next = 28;
            return Fs.async.mkdir(documentFolder);

          case 28:
            kodheappsFolder = require(__dirname + "/../config/args").folder;
            appFolder = _path.default.join(documentFolder, kodheappsFolder);

            if (Fs.sync.exists(appFolder)) {
              _context.next = 33;
              break;
            }

            _context.next = 33;
            return Fs.async.mkdir(appFolder);

          case 33:
            mainWindow = null;
            cw = require(__dirname + "/../config/args");
            defaultApps = _path.default.join(__dirname, "..", "default_apps");

            if (!Fs.sync.exists(defaultApps)) {
              _context.next = 65;
              break;
            }

            _context.next = 39;
            return Fs.async.readdir(defaultApps);

          case 39:
            dirs = _context.sent;
            i = 0;

          case 41:
            if (!(i < dirs.length)) {
              _context.next = 65;
              break;
            }

            _context.next = 44;
            return Fs.async.stat(_path.default.join(defaultApps, dirs[i]));

          case 44:
            stat = _context.sent;

            if (!stat.isDirectory()) {
              _context.next = 62;
              break;
            }

            n1 = _path.default.join(appFolder, dirs[i]);

            if (Fs.sync.exists(_path.default.join(n1, "kodhe.app.json"))) {
              _context.next = 62;
              break;
            }

            n2 = _path.default.join(defaultApps, dirs[i]);
            _context.next = 51;
            return Fs.async.readdir(n2);

          case 51:
            d2 = _context.sent;

            if (Fs.sync.exists(n1)) {
              _context.next = 55;
              break;
            }

            _context.next = 55;
            return Fs.async.mkdir(n1);

          case 55:
            y = 0;

          case 56:
            if (!(y < d2.length)) {
              _context.next = 62;
              break;
            }

            _context.next = 59;
            return require("fs-extra").copy(_path.default.join(n2, d2[y]), _path.default.join(n1, d2[y]));

          case 59:
            y++;
            _context.next = 56;
            break;

          case 62:
            i++;
            _context.next = 41;
            break;

          case 65:
            _context.next = 67;
            return uglobal.UserFunction("app.boot.cmd").invoke({
              getfunc: true
            });

          case 67:
            revise = _context.sent;
            next = revise(process.argv);
            startArgs = next.startArgs;
            next = next.next;
            port = require(__dirname + "/../config/0port");
            url = "http://127.0.0.1:" + port + "/";
            global.url = url;
            BrowserWindow = electron.BrowserWindow;
            path = require('path');
            iconpath = __dirname + "/../favicon.png";
            iconpath2 = __dirname + "/../favicon.2.png";
            home = _os.default.homedir();
            kodhe_hide = _path.default.join(home, "kodhe-start-hide");

            if (!Fs.sync.exists(kodhe_hide)) {
              _context.next = 99;
              break;
            }

            _context.next = 83;
            return Fs.async.readFile(kodhe_hide, 'utf8');

          case 83:
            content = _context.sent;

            if (content == 1) {
              startArgs.hide = true;
            }

            _context.prev = 85;
            _context.next = 88;
            return Fs.async.unlink(kodhe_hide);

          case 88:
            _context.next = 99;
            break;

          case 90:
            _context.prev = 90;
            _context.t0 = _context["catch"](85);
            _context.prev = 92;
            _context.next = 95;
            return Fs.async.writeFile(kodhe_hide, '0');

          case 95:
            _context.next = 99;
            break;

          case 97:
            _context.prev = 97;
            _context.t1 = _context["catch"](92);

          case 99:
            global.iconpath = iconpath;

            global.createWindow = function () {
              if (mainWindow == null && cw.createManager) {
                createWindow();
                app.on('window-all-closed', function () {
                  if (process.platform !== 'darwin') {
                    app.quit();
                  }

                  finish();
                });
              }
            };

            if (!cw.start) {
              _context.next = 118;
              break;
            }

            req = new core.VW.Http.Request(url + "api/function/c/app.check");
            _context.prev = 103;
            req.method = 'POST';
            req.contentType = 'application/json';
            req.body = cw.start;
            cw.start.hidden = startArgs.hide !== false;
            _context.next = 110;
            return req.getResponseAsync();

          case 110:
            _context.next = 115;
            break;

          case 112:
            _context.prev = 112;
            _context.t2 = _context["catch"](103);
            throw _context.t2;

          case 115:
            _context.prev = 115;
            req && req.innerRequest && delete req.innerRequest.callback;
            return _context.finish(115);

          case 118:
            cron = function cron(startArgs) {
              var req = new core.VW.Http.Request(url + "api/function/c/cron.0tasks");
              req.analizeResponse = false;
              req.method = 'POST';
              req.contentType = 'application/json';
              req.body = startArgs;
              req.getResponseAsync();
            };

            Int = setInterval(cron, 120000);
            Int.unref();
            startArgs.firsttime = true;
            cron(startArgs);

          case 123:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, this, [[85, 90], [92, 97], [103, 112, 115, 118]]);
  }));

  return function L(_x) {
    return _ref.apply(this, arguments);
  };
}();

module.exports = function (global) {
  F.global = global;
  return L;
};