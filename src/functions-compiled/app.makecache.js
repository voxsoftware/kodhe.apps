"use strict";

var _path = _interopRequireDefault(require("path"));

var _zlib = _interopRequireDefault(require("zlib"));

var _fs = _interopRequireDefault(require("fs"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var Fs = core.System.IO.Fs;

var F = function () {
  var _ref = _asyncToGenerator(regeneratorRuntime.mark(function _callee2(body) {
    var global, maindir, dirs1, dirs2, files, dir1, updated, streamOut, key, invalid, options, procesar, intBuffer, name, buf1, buf2, enc, bufs, id, task, file, ucontent, ast, parser, i;
    return regeneratorRuntime.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            global = F.global;
            maindir = _path.default.join(__dirname, "..");
            dirs1 = {}, dirs2 = {};
            files = [];
            updated = body.updated;
            streamOut = global.context.response;
            body = body || {};
            key = {
              permission: 'obfuscate'
            };
            streamOut.setTimeout(300000);
            invalid = ["functions-compiled", "dependencies/vox-core", "dependencies/native-deps", "dependencies/node-modules", "dependencies/instructions.MD", "node_modules", "compile.sh", "yarn.lock"];
            options = [];

            procesar = function () {
              var _ref2 = _asyncToGenerator(regeneratorRuntime.mark(function _callee(dirname) {
                var dirs, stat, udir, re, i;
                return regeneratorRuntime.wrap(function _callee$(_context) {
                  while (1) {
                    switch (_context.prev = _context.next) {
                      case 0:
                        _context.next = 2;
                        return Fs.async.readdir(dirname);

                      case 2:
                        dirs = _context.sent;
                        i = 0;

                      case 4:
                        if (!(i < dirs.length)) {
                          _context.next = 23;
                          break;
                        }

                        if (dirs[i].startsWith(".")) {
                          _context.next = 20;
                          break;
                        }

                        udir = _path.default.join(dirname, dirs[i]);
                        _context.next = 9;
                        return Fs.async.stat(udir);

                      case 9:
                        stat = _context.sent;
                        re = _path.default.relative(maindir, dirname);

                        if (!stat.isDirectory()) {
                          _context.next = 19;
                          break;
                        }

                        if (!(invalid.indexOf(re) < 0)) {
                          _context.next = 17;
                          break;
                        }

                        options.push(re);
                        dirs1[udir] = stat;
                        _context.next = 17;
                        return procesar(udir);

                      case 17:
                        _context.next = 20;
                        break;

                      case 19:
                        if (invalid.indexOf(re) < 0) {
                          options.push(re);
                          if (stat.mtime.getTime() > updated) updated = stat.mtime.getTime();
                          files.push({
                            file: udir,
                            stat: stat
                          });
                        }

                      case 20:
                        i++;
                        _context.next = 4;
                        break;

                      case 23:
                      case "end":
                        return _context.stop();
                    }
                  }
                }, _callee, this);
              }));

              return function procesar(_x2) {
                return _ref2.apply(this, arguments);
              };
            }();

            _context2.next = 14;
            return procesar(maindir);

          case 14:
            global.context.response.setHeader("x-app-updated", updated);
            global.context.response.setHeader("x-app-version", global.constants.version);
            dirs2 = dirs1;

            if (!(updated <= body.updated)) {
              _context2.next = 19;
              break;
            }

            return _context2.abrupt("return", global.context.response.end());

          case 19:
            enc = global.context.request.headers["accept-encoding"];

            if (enc && enc.indexOf("gzip") >= 0) {
              streamOut.setHeader("content-encoding", "gzip");
              streamOut = _zlib.default.createGzip();
              streamOut.pipe(global.context.response);
            }

            bufs = [];

            for (id in dirs2) {
              name = _path.default.relative(maindir, id);

              if (name && dirs2[id]) {
                name = '\0' + name;
                buf1 = Buffer.from(name);
                buf2 = Buffer.from(core.safeJSON.stringify(dirs2[id]));
                intBuffer = Buffer.alloc(4);
                intBuffer.writeInt32LE(0, 0);
                bufs.push(intBuffer);
                intBuffer = Buffer.alloc(4);
                intBuffer.writeInt32LE(buf1.length, 0);
                bufs.push(intBuffer);
                bufs.push(buf1);
                intBuffer = Buffer.alloc(4);
                intBuffer.writeInt32LE(buf2.length, 0);
                bufs.push(intBuffer);
                bufs.push(buf2);
              }
            }

            task = new core.VW.Task();

            if (!(streamOut.write(Buffer.concat(bufs)) === false)) {
              _context2.next = 28;
              break;
            }

            streamOut.once("drain", task.finish.bind(task));
            _context2.next = 28;
            return task;

          case 28:
            parser = global.publicContext.ecmaParser = global.publicContext.ecmaParser || new core.VW.Ecma2015.Parser();
            i = 0;

          case 30:
            if (!(i < files.length)) {
              _context2.next = 94;
              break;
            }

            bufs = [];
            file = files[i];
            name = _path.default.relative(maindir, file.file);
            ucontent = null;
            _context2.prev = 35;

            if (!(name.startsWith("functions") && (name.endsWith(".es6") || name.endsWith(".js")))) {
              _context2.next = 58;
              break;
            }

            if (!name.endsWith(".es6")) {
              _context2.next = 47;
              break;
            }

            if (!Fs.sync.exists(file.file.substring(0, file.file.length - 3) + "js")) {
              _context2.next = 40;
              break;
            }

            return _context2.abrupt("continue", 91);

          case 40:
            _context2.next = 42;
            return Fs.async.readFile(file.file, 'utf8');

          case 42:
            ucontent = _context2.sent;
            name = name.substring(0, name.length - 3) + "js";

            if (ucontent) {
              ast = parser.parse(ucontent);
              ucontent = ast.code;
            }

            _context2.next = 50;
            break;

          case 47:
            _context2.next = 49;
            return Fs.async.readFile(file.file, 'utf8');

          case 49:
            ucontent = _context2.sent;

          case 50:
            if (!ucontent) {
              _context2.next = 55;
              break;
            }

            if (!(key.permission.indexOf("obfuscate") >= 0)) {
              _context2.next = 55;
              break;
            }

            _context2.next = 54;
            return global.UserFunction("code.obfuscate").invoke({
              code: ucontent
            });

          case 54:
            ucontent = _context2.sent;

          case 55:
            ucontent = ucontent || "";
            ucontent = Buffer.from(ucontent);
            ast = null;

          case 58:
            _context2.next = 63;
            break;

          case 60:
            _context2.prev = 60;
            _context2.t0 = _context2["catch"](35);
            throw global.Exception.create("Failed processing file: " + file.file + ", error: " + _context2.t0.message).putCode(_context2.t0.code);

          case 63:
            buf1 = Buffer.from(name);
            buf2 = Buffer.from(core.safeJSON.stringify(file.stat));
            intBuffer = Buffer.alloc(4);
            intBuffer.writeInt32LE(ucontent ? ucontent.length : file.stat.size, 0);
            bufs.push(intBuffer);
            intBuffer = Buffer.alloc(4);
            intBuffer.writeInt32LE(buf1.length, 0);
            bufs.push(intBuffer);
            bufs.push(buf1);
            intBuffer = Buffer.alloc(4);
            intBuffer.writeInt32LE(buf2.length, 0);
            bufs.push(intBuffer);
            bufs.push(buf2);

            if (ucontent) {
              _context2.next = 84;
              break;
            }

            _context2.t1 = bufs;
            _context2.next = 80;
            return Fs.async.readFile(file.file);

          case 80:
            _context2.t2 = _context2.sent;

            _context2.t1.push.call(_context2.t1, _context2.t2);

            _context2.next = 85;
            break;

          case 84:
            bufs.push(ucontent);

          case 85:
            task = new core.VW.Task();
            bufs = Buffer.concat(bufs);

            if (!(streamOut.write(bufs) === false)) {
              _context2.next = 91;
              break;
            }

            streamOut.once("drain", task.finish.bind(task));
            _context2.next = 91;
            return task;

          case 91:
            i++;
            _context2.next = 30;
            break;

          case 94:
            streamOut.end();
            task = new core.VW.Task();
            global.context.response.on("finish", task.finish.bind(task));
            _context2.next = 99;
            return task;

          case 99:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2, this, [[35, 60]]);
  }));

  return function F(_x) {
    return _ref.apply(this, arguments);
  };
}();

module.exports = function (global) {
  F.global = global;
  return F;
};