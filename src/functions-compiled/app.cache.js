"use strict";

var _path = _interopRequireDefault(require("path"));

var _fs = _interopRequireDefault(require("fs"));

var _zlib = _interopRequireDefault(require("zlib"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var Fs = core.System.IO.Fs;

var F = function () {
  var _ref = _asyncToGenerator(regeneratorRuntime.mark(function _callee(body) {
    var global, url, fsi, cr, fse, kodheappsFolder, home, documentFolder, appFolder, tapp, tapp2, versiondir, currentConfigFile, config, version, tempfile, req, st, task, updated, stream, stream2, breader, len1, file, len2, len3, buf1, size, buf2, len4, cstat, newdir, files, i, c1;
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            global = F.global;
            url = "".concat(body.url);
            fsi = require("fs-extra");

            cr = function cr(func) {
              return function () {
                var task = new core.VW.Task();
                var args = Array.prototype.slice.call(arguments);
                args.push(function (er, data) {
                  if (er) task.exception = er;
                  task.result = data;
                  return task.finish();
                });
                func.apply(fsi, args);
                return task;
              };
            };

            fse = {
              copy: cr(fsi.copy),
              mkdir: cr(fsi.mkdir),
              rename: cr(fsi.rename),
              remove: cr(fsi.remove)
            };
            _context.prev = 5;
            global.publicContext.is_caching = true;
            global.publicContext.caching = global.publicContext.caching || {};

            if (!global.publicContext.caching[body.name]) {
              _context.next = 10;
              break;
            }

            return _context.abrupt("return");

          case 10:
            global.publicContext.caching[body.name] = true;
            kodheappsFolder = require(__dirname + "/../config/args").folder;
            home = require("os").homedir();
            documentFolder = _path.default.normalize(home + '/Documents/');
            appFolder = _path.default.join(documentFolder, kodheappsFolder);
            tapp = _path.default.join(appFolder, global.uniqid());
            tapp2 = _path.default.join(appFolder, body.name);
            versiondir = _path.default.join(appFolder, "old_versions");
            currentConfigFile = _path.default.join(tapp2, "__app__cache.json");
            tempfile = _path.default.join(global.UserContext.getHomeDir(), global.uniqid());

            if (!Fs.sync.exists(currentConfigFile)) {
              _context.next = 28;
              break;
            }

            _context.next = 23;
            return Fs.async.readFile(currentConfigFile, 'utf8');

          case 23:
            config = _context.sent;
            config = JSON.parse(config);
            config.url = url;
            _context.next = 29;
            break;

          case 28:
            config = {
              url: url,
              updated: 0,
              version: '0'
            };

          case 29:
            req = new core.VW.Http.Request(config.url);
            req.headers["accept-encoding"] = 'gzip';
            req.userAgent = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.81 Safari/537.36';
            updated = 0;
            task = new core.VW.Task();
            _context.prev = 34;
            req.method = 'POST';
            req.body = {
              updated: config.updated
            };
            req.analizeResponse = false;
            req.beginGetResponse();
            st = _fs.default.createWriteStream(tempfile);
            req.innerRequest.on("response", function (r) {
              updated = parseInt(r.headers["x-app-updated"]);
              version = r.headers["x-app-version"];

              if (r.headers["content-encoding"] && r.headers["content-encoding"] == "gzip") {
                var gzip = _zlib.default.createGunzip();

                req.innerRequest.pipe(gzip);
                gzip.on("error", function (e) {
                  task.exception = e;
                  task.finish();
                });
                gzip.pipe(st);
              } else {
                req.innerRequest.pipe(st);
              }
            });
            st.on("finish", function () {
              task.finish();
            });
            st.on("error", function (er) {
              task.exception = er;
              task.finish();
            });
            req.innerRequest.on("error", function (er) {
              task.exception = er;
              task.finish();
            });
            _context.next = 46;
            return task;

          case 46:
            _context.next = 51;
            break;

          case 48:
            _context.prev = 48;
            _context.t0 = _context["catch"](34);
            throw _context.t0;

          case 51:
            _context.prev = 51;
            if (req.innerRequest) delete req.innerRequest.callback;
            return _context.finish(51);

          case 54:
            if (Fs.sync.exists(tempfile)) {
              _context.next = 56;
              break;
            }

            throw global.Exception.create("Updating from url failed");

          case 56:
            if (!(updated <= config.updated)) {
              _context.next = 58;
              break;
            }

            return _context.abrupt("return");

          case 58:
            if (!Fs.sync.exists(tapp)) {
              Fs.sync.mkdir(tapp);
            }

            _context.prev = 59;
            stream = new core.System.IO.FileStream(tempfile, core.System.IO.FileMode.Open, core.System.IO.FileAccess.Read);
            stream.position = 0;
            breader = new core.System.IO.BinaryReader(stream);
            _context.next = 65;
            return Fs.async.stat(tempfile);

          case 65:
            size = _context.sent;
            size = size.size;

          case 67:
            if (!(stream.position < size)) {
              _context.next = 111;
              break;
            }

            _context.next = 70;
            return breader.readInt32Async();

          case 70:
            len1 = _context.sent;
            _context.next = 73;
            return breader.readInt32Async();

          case 73:
            len3 = _context.sent;
            buf2 = new Buffer(len3);
            _context.next = 77;
            return stream.readAsync(buf2, 0, buf2.length);

          case 77:
            len2 = _context.sent;

            if (!(len2 != buf2.length)) {
              _context.next = 80;
              break;
            }

            throw global.Exception.create("Updating from url failed. File: " + file + ", failed getting data");

          case 80:
            file = buf2.toString();
            _context.next = 83;
            return breader.readInt32Async();

          case 83:
            len4 = _context.sent;
            buf2 = new Buffer(len4);
            _context.next = 87;
            return stream.readAsync(buf2, 0, buf2.length);

          case 87:
            len2 = _context.sent;

            if (!(len2 != buf2.length)) {
              _context.next = 90;
              break;
            }

            throw global.Exception.create("Updating from url failed. File: " + file + ", failed getting stat");

          case 90:
            cstat = JSON.parse(buf2.toString());

            if (!file.startsWith("\0")) {
              _context.next = 97;
              break;
            }

            file = file.substring(1);
            vw.warning("FILE:: ", file);
            if (!Fs.sync.exists(_path.default.join(tapp, file))) Fs.sync.mkdir(_path.default.join(tapp, file));
            _context.next = 109;
            break;

          case 97:
            vw.warning("FILE:: ", file);
            buf1 = new Buffer(len1);
            _context.next = 101;
            return stream.readAsync(buf1, 0, buf1.length);

          case 101:
            len2 = _context.sent;

            if (!(len2 != buf1.length)) {
              _context.next = 104;
              break;
            }

            throw global.Exception.create("Updating from url failed. File: " + file + ", failed getting data");

          case 104:
            stream2 = new core.System.IO.FileStream(_path.default.join(tapp, file), core.System.IO.FileMode.OpenOrCreate, core.System.IO.FileAccess.Write);
            _context.next = 107;
            return stream2.writeAsync(buf1, 0, buf1.length);

          case 107:
            stream2.close();
            stream2 = null;

          case 109:
            _context.next = 67;
            break;

          case 111:
            _context.next = 119;
            break;

          case 113:
            _context.prev = 113;
            _context.t1 = _context["catch"](59);

            if (!Fs.sync.exists(tapp)) {
              _context.next = 118;
              break;
            }

            _context.next = 118;
            return fse.remove(tapp);

          case 118:
            throw _context.t1;

          case 119:
            _context.prev = 119;
            if (stream) stream.close();
            if (stream2) stream2.close();
            return _context.finish(119);

          case 123:
            if (!Fs.sync.exists(versiondir)) Fs.sync.mkdir(versiondir);

            if (!Fs.sync.exists(tapp2)) {
              _context.next = 166;
              break;
            }

            _context.next = 127;
            return Fs.async.writeFile(_path.default.join(tapp2, "__app__disabled"), "1");

          case 127:
            newdir = _path.default.join(versiondir, body.name + "_v" + (config.version || ""));

            if (!Fs.sync.exists(newdir)) {
              _context.next = 131;
              break;
            }

            _context.next = 131;
            return fse.remove(newdir);

          case 131:
            _context.next = 133;
            return fse.copy(tapp2, newdir);

          case 133:
            _context.next = 135;
            return Fs.async.readdir(tapp2);

          case 135:
            files = _context.sent;
            i = 0;

          case 137:
            if (!(i < files.length)) {
              _context.next = 144;
              break;
            }

            if (!(files[i] != "kodhe.app.json")) {
              _context.next = 141;
              break;
            }

            _context.next = 141;
            return fse.remove(_path.default.join(tapp2, files[i]));

          case 141:
            i++;
            _context.next = 137;
            break;

          case 144:
            _context.next = 146;
            return Fs.async.readdir(tapp);

          case 146:
            files = _context.sent;
            i = 0;

          case 148:
            if (!(i < files.length)) {
              _context.next = 155;
              break;
            }

            if (!(files[i] != "kodhe.app.json")) {
              _context.next = 152;
              break;
            }

            _context.next = 152;
            return fse.copy(_path.default.join(tapp, files[i]), _path.default.join(tapp2, files[i]));

          case 152:
            i++;
            _context.next = 148;
            break;

          case 155:
            c1 = '';

            if (!Fs.sync.exists(_path.default.join(tapp, "kodhe.app.json"))) {
              _context.next = 160;
              break;
            }

            _context.next = 159;
            return Fs.async.readFile(_path.default.join(tapp, "kodhe.app.json"));

          case 159:
            c1 = _context.sent;

          case 160:
            _context.next = 162;
            return Fs.async.writeFile(_path.default.join(tapp2, "kodhe.app.json"), c1);

          case 162:
            _context.next = 164;
            return fse.remove(tapp);

          case 164:
            _context.next = 168;
            break;

          case 166:
            _context.next = 168;
            return Fs.async.rename(tapp, tapp2);

          case 168:
            config.updated = updated;
            config.version = version;
            _context.next = 172;
            return Fs.async.writeFile(currentConfigFile, core.safeJSON.stringify(config));

          case 172:
            return _context.abrupt("return", {
              path: tapp,
              updated: updated,
              name: body.name
            });

          case 175:
            _context.prev = 175;
            _context.t2 = _context["catch"](5);
            throw _context.t2;

          case 178:
            _context.prev = 178;
            global.publicContext.is_caching = false;
            global.publicContext.caching[body.name] = false;

            if (!Fs.sync.exists(tapp)) {
              _context.next = 184;
              break;
            }

            _context.next = 184;
            return fse.remove(tapp);

          case 184:
            if (!Fs.sync.exists(tempfile)) {
              _context.next = 187;
              break;
            }

            _context.next = 187;
            return Fs.async.unlink(tempfile);

          case 187:
            return _context.finish(178);

          case 188:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, this, [[5, 175, 178, 188], [34, 48, 51, 54], [59, 113, 119, 123]]);
  }));

  return function F(_x) {
    return _ref.apply(this, arguments);
  };
}();

module.exports = function (global) {
  F.global = global;
  return F;
};