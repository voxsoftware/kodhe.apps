"use strict";

var _os = _interopRequireDefault(require("os"));

var _path = _interopRequireDefault(require("path"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var Fs = core.System.IO.Fs;
var F;

F = function () {
  var _ref = _asyncToGenerator(regeneratorRuntime.mark(function _callee(body) {
    var global, home, documentFolder, kodheappsFolder, data, folder, stat, ufolder, file, content, apps, file2, stat1, folders, i;
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            global = F.global;
            home = require("os").homedir();
            documentFolder = _path.default.normalize(home + '/Documents/');
            kodheappsFolder = require(__dirname + "/../config/args").folder;
            data = {
              documentFolder: documentFolder,
              appFolder: _path.default.join(documentFolder, kodheappsFolder)
            };

            if (Fs.sync.exists(data.appFolder)) {
              _context.next = 8;
              break;
            }

            _context.next = 8;
            return Fs.async.mkdir(data.appFolder);

          case 8:
            apps = [];
            _context.next = 11;
            return Fs.async.readdir(data.appFolder);

          case 11:
            folders = _context.sent;
            folders = folders.filter(function (a) {
              return a != "org.kodhe.apps";
            });
            i = 0;

          case 14:
            if (!(i < folders.length)) {
              _context.next = 42;
              break;
            }

            folder = folders[i];
            ufolder = _path.default.join(data.appFolder, folder);

          case 17:
            if (!(global.publicContext.caching && global.publicContext.caching[folder])) {
              _context.next = 22;
              break;
            }

            _context.next = 20;
            return core.VW.Task.sleep(100);

          case 20:
            _context.next = 17;
            break;

          case 22:
            _context.next = 24;
            return Fs.async.stat(ufolder);

          case 24:
            stat = _context.sent;

            if (!stat.isDirectory()) {
              _context.next = 39;
              break;
            }

            file = _path.default.join(ufolder, "kodhe.app.json");
            file2 = _path.default.join(ufolder, "__app__disabled");

          case 28:
            if (!(global.publicContext.caching && global.publicContext.caching[folder])) {
              _context.next = 33;
              break;
            }

            _context.next = 31;
            return core.VW.Task.sleep(100);

          case 31:
            _context.next = 28;
            break;

          case 33:
            if (!(Fs.sync.exists(file) && !Fs.sync.exists(file2))) {
              _context.next = 39;
              break;
            }

            _context.next = 36;
            return Fs.async.readFile(file, 'utf8');

          case 36:
            content = _context.sent;

            try {
              content = JSON.parse(content);
            } catch (e) {
              content = null;
            }

            if (content != null && content.name != "org.kodhe.apps") {
              content.path = ufolder;
              apps.push(content);
            }

          case 39:
            i++;
            _context.next = 14;
            break;

          case 42:
            return _context.abrupt("return", apps);

          case 43:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, this);
  }));

  return function F(_x) {
    return _ref.apply(this, arguments);
  };
}();

module.exports = function (global) {
  F.global = global;
  return F;
};