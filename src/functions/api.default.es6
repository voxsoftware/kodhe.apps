var Global
import Url from 'url'
import Path from 'path'


var F= async function(body){
	
	var global= F.global
	var url = global.context.request.url
	var uri = Url.parse(url)
	var request= global.context.request
	
	
	var parts= uri.pathname.split("/").filter((a)=>!!a)
	if(parts[0]=="static" && parts[1]=="kodhe.app.json"){
		
		var server= new core.VW.Http.StaticServe.Server()
		server.addPath(Path.join(__dirname, ".."))
		global.context.request.url= "/kodhe.app.json"
		await server.handle(global.context)
		
		global.context._body.stopdefault= true
		
	}
	
	
	else{
		return await global.context.continueAsync()
	}

}

module.exports=function(global){
	F.global=global
	return F
}