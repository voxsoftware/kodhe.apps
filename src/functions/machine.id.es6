

var F
F= async function(app){
	var global= F.global 
	var machineId= await global.UserContext.require("node-machine-id","1.1.10")
	return {
		id: await machineId.machineId()
	}
}


module.exports=function(global){
	F.global= global 
	return F 
}