/*global core*/
var F

F= async function(data){
    
    var global= F.global 
    global.publicContext.prs=global.publicContext.prs||{}
    
    var response=[]
    for(var id in global.publicContext.prs){
        response.push({
            id, 
            port: global.publicContext.prs[id].port
        })
    }
    return response
    
    
}

module.exports=function(global){
    F.global= global 
    return F 
}