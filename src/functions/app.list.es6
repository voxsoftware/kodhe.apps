
import Os from 'os'
import Path from 'path'
var Fs= core.System.IO.Fs

var F
F= async function(body){
	var global= F.global 
	
	
	
	
	var home = require("os").homedir();
	var documentFolder = Path.normalize(home + '/Documents/')
	var kodheappsFolder= require(__dirname + "/../config/args").folder
	var data= {
		documentFolder,
		appFolder: Path.join(documentFolder, kodheappsFolder)
	}
	
	if(!Fs.sync.exists(data.appFolder))
		await Fs.async.mkdir(data.appFolder)
	
	
	var folder, stat, ufolder, file, content, apps=[], file2, stat1
	var folders= await Fs.async.readdir(data.appFolder)
	
	folders=folders.filter(function(a){
		return a!="org.kodhe.apps"
	})
	
	for(var i=0;i<folders.length;i++){
		folder= folders[i]
		ufolder= Path.join(data.appFolder, folder)
		
		while(global.publicContext.caching && global.publicContext.caching[folder]){
			await core.VW.Task.sleep(100)
		}
		
		
		stat=await Fs.async.stat(ufolder)
		if(stat.isDirectory()){
			
			file= Path.join(ufolder,"kodhe.app.json")
			file2= Path.join(ufolder,"__app__disabled")
			
			
			while(global.publicContext.caching && global.publicContext.caching[folder]){
				await core.VW.Task.sleep(100)
			}
			
			
			if(Fs.sync.exists(file) && !Fs.sync.exists(file2)){
				
				//stat1= 
				content=await Fs.async.readFile(file,'utf8')
				try{
					content= JSON.parse(content)
				}catch(e){
					content=null 
				}
				
				if(content!=null && content.name!="org.kodhe.apps"){
					content.path= ufolder
					apps.push(content)
				}
			}
			
		}
	}
	
	return apps
	
}
module.exports=function(global){
	F.global= global 
	return F 
}