/*global core*/
import Os from 'os'
import Path from 'path'
var Fs= core.System.IO.Fs

var F

F= async function(body){
    var global= F.global 
    
    var app= body.app || body
    
    var kodheappsFolder= require(__dirname + "/../config/args").folder
    
    var home = require("os").homedir()
    var documentFolder = Path.normalize(home + '/Documents/')
    var realAppFolder= Path.join(documentFolder, kodheappsFolder)
    var appFolder= Path.join(documentFolder, kodheappsFolder,'org.kodhe.apps')
    
    process.env.BASE_DIR=appFolder 
    var port= require(__dirname + "/../config/0port")
    
    
    if(app.path){
        var name= Path.basename(app.path)
        //var dir= Path.join(Path.dirname(app.path), name)
        //process.env.BASE_DIR=dir 
        app.link='http://127.0.0.1:' + port + '/site/' + name 
    }
    
    
    if(app.link){
        
        var id= (await global.UserFunction("machine.id").invoke()).id
        var cid
        
        
        var url= app.link  + "/static/kodhe.app.json"
        var req= new core.VW.Http.Request(url), data, response, a
        try{
            
            response=await req.getResponseAsync()
            a= true
            data= response.body 
            if(typeof data=="string"){
                data= JSON.parse(data)
            }
            
            if(!data.name){
                throw global.Exception.create("No name provided to application")
            }
            if(data.error){
                throw data.error
            }
            
            
            if(data.localcache == 'preferred' || data.localcache=="always"){
                
                
                if( (!app.path) || app.updating === "automatic" || body.update){
                    try{
                        var cacheurl= data.cacheurl 
                        if(!cacheurl.startsWith("http:") && !cacheurl.startsWith("https:")){
                            if(!cacheurl.startsWith("/") && !app.link.endsWith("/"))
                                cacheurl = "/" + cacheurl
                            cacheurl= app.link  + cacheurl
                        }
                        var result= await global.UserFunction("app.cache").invoke({
                            url: cacheurl,
                            key: app.key  || data.key,
                            name: data.name
                        })
                    }
                    catch(e){
                        app.path= Path.join(realAppFolder, data.name)
                        if(!Fs.sync.exists(app.path) ||  body.showerror)
                            throw e
                        console.error("Failed updating Local app: ", e)
                    }
                    app.path= Path.join(realAppFolder, data.name)
                    app.link= "http://127.0.0.1:"+port+"/site/" + data.name
                    
                   
                    if(data.windowicon)
                        app.windowicon=data.windowicon
                }
                
                
            }
            
            
            
            
            var p0= await global.UserFunction("app.start.process").invoke(data)
            
            
            var prefix= data.id || data.name
            prefix= (new Buffer(prefix)).toString("hex")
            cid=  prefix + id
            
            var customicon= ( (app.windowicon && app.windowicon.png && app.path) ? Path.join(app.path, app.windowicon.png) : null )
            var customiconw= ( (app.windowicon && app.windowicon.ico && app.path) ? Path.join(app.path, app.windowicon.ico) : null )
            
            /*
            if(customicon &&Fs.sync.exists(customicon)){
                var nc=Path.join(global.UserContext.getHomeDir(), data.name + "-icon.png")
                if(Fs.sync.exists(nc)){
                    try{
                        await Fs.async.unlink(nc)
                    }catch(e){
                        console.error("error deleting icon ")
                    }
                }
                try{
                    require("fs-extra").copy(customicon,nc)
                    
                }catch(e){
                    console.error("error deleting icon ")
                }
                customicon=nc
                
            }*/
            
            
            
            if(req.innerRequest && req.innerRequest.callback)
                delete req.innerRequest.callback 
                
                
            
            global.publicContext.apps=global.publicContext.apps||[]
            var info= global.publicContext.apps[data.name]= global.publicContext.apps[data.name]||{}
            
            
            if(data.localcache && data.scripts && data.scripts.open){
                
                
                
                try{
                    
                    req= new core.VW.Http.Request(app.link+ data.scripts.open )
                    req.method='POST'
                    req.body= {
                        app, 
                        port: p0.port,
                        kodhe_apps_code: cid
                    }
                    response= await req.getResponseAsync()
                    if(!response.body)
                        throw global.Exception.create("Invalid RESPONSE")
                    
                    
                }
                catch(e){
                    throw e 
                }
                finally{
                    if(req.innerRequest && req.innerRequest.callback)
                        delete req.innerRequest.callback 
                }
            
                
            }
            else{
            
                // OPEN APP IN NEW WINDOW ...
                var w,ic 
                
                data.window= data.window||{}
                ic=customicon  || global.iconpath
                w= info.window 
                if(!w){
                    const electron = require('electron')
                    const BrowserWindow = electron.BrowserWindow
                    var wargs= {
                      width: data.window.width || 600,
                      height: data.window.height || 580,
                      "minWidth": data.window.minWidth || 560,
                      "minHeight": data.window.minHeight|| 580,
                      
                      show: false,
                      icon: ic,
                      nodeIntegration:true
                    }
                    if(data.window.maxWidth)
                        wargs.maxWidth= data.window.maxWidth
                    if(data.window.maxHeight)
                        wargs.maxHeight= data.window.maxHeight
                    
                    w=new BrowserWindow(wargs)
                    info.window= w
                    
                    if(Os.platform()=="win32"){
                        
                        if(customiconw){
                            
                            w.setAppDetails({
                                appId: data.versionedName || data.name,
                                appIconPath: customiconw,
                                relaunchCommand: '"' +process.execPath+'"' + " --exec " + data.name,
                                relaunchDisplayName: data.secureDisplayName || data.name
                            }) 
                            //w.setAppDetails({})
                        }
                        
                        
                    }
                    
                    
                    w.loadURL(app.link + "?kodhe_apps_code="+encodeURIComponent(cid)+"&port=" + p0.port + "&port1=" + port)
                    w.on("close",function(){
                        delete info.window
                    })
                    
                    
                }
                if(!body.hidden)
                    w.show()
            }
            
            
            
            
                
            
            // SEND APP INFO 
            var retry=2
            while(retry>0){
                
                try{
                    
                    var machineId= await global.UserContext.require("node-machine-id","1.1.10")
                    data.id= machineId.machineIdSync()
                    
                    req= new core.VW.Http.Request(app.link+"/api/function/c/app.localmode")
                    req.method='POST'
                    req.body= {
                        app: data, 
                        id: data.id, 
                        port: p0.port,
                        port1: port,
                        kodhe_apps_code: cid
                    } 
                    response= await req.getResponseAsync()
                    if(!response.body)
                        throw global.Exception.create("Invalid RESPONSE")
                    
                    
                    retry=0
                    
                }
                catch(e){
                    retry--
                    if(retry==0)
                        throw e 
                }
                finally{
                    if(req.innerRequest && req.innerRequest.callback)
                        delete req.innerRequest.callback 
                }
            }
            
            
            
            
        }
        catch(e){
            if(a)
                throw global.Exception.create("No es una aplicación válida: " + e.message).putCode("INVALID_APP")
            throw e 
        }
        finally{
            if(req.innerRequest && req.innerRequest.callback)
                delete req.innerRequest.callback 
        }
    }
}

var y=function(local){
    local.iconpath= global.iconpath
}
module.exports=function(global){
    y(global)
    F.global= global 
    return F 
}