import Path from 'path'

F=(body)->
	global= F.global 
	file= Path.join __dirname, "..", "..", "dist-update", body.path
	path= Path.dirname file 
	name= Path.basename file
    
	server= new core.VW.Http.StaticServe.Server()
	server.fixedHeaders = server.fixedHeaders or {}
	server.fixedHeaders["content-disposition"]= "attachment;filename=#{name}"
	server.addPath path 
	global.context.request.url= "/#{name}"
	await server.handle(global.context)
    
	body._stopdefault = body.stopdefault = true

module.exports= (global)->
	F.global= global 
	F
    