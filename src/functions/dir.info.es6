import Os from 'os'
import Path from 'path'
var Fs= core.System.IO.Fs
var F
F=  async function(body){
	var global= F.global 
	
	var home 
	home = require("os").homedir()
	
	var kodheappsFolder= require(__dirname + "/../config/args").folder
	var documentFolder = Path.normalize(home + '/Documents/')
	var appFolder= Path.join(documentFolder, kodheappsFolder)
	if(!Fs.sync.exists(appFolder))
		await Fs.async.mkdir(appFolder)
	return {
		documentFolder,
		appFolder
	}
}
module.exports=function(global){
	F.global= global 
	return F 
}