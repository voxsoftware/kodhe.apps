import Path from 'path'
Fs= core.System.IO.Fs

F= (body)->
    global= F.global 
    ctx= await global.getSiteContext "kodhe-packager"
    outpath= Path.join(__dirname,"..", "public", "js")
    
    data= await ctx.userFunction("packager.compile").invoke 
        path: Path.join __dirname, "..", "frontend", "src","script","program"
    
    await Fs.async.writeFile Path.join(outpath, "program.js"), data.code
    
module.exports= (global)->
    F.global= global 
    F 