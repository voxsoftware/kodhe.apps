import Path from 'path'
var F= async function(body){
	var global= F.global 
	var crons= global.publicContext.crons=global.publicContext.crons||{}
	
	if(global.context && global.context.response){
		global.context.response.setTimeout(300000)
	}
	var apps, app
	if(body&&(body.firsttime||body.exec)){
		
		apps= await global.UserFunction("app.list").invoke({})
		// checking startup
		
		for(var i=0;i<apps.length;i++){
			console.info("APP FOUND. ", apps[i])
			if(body.firsttime){
				if((body.startup && apps[i].startup=="boot") || apps[i].startup=='always'){
					
					await global.UserFunction("app.check").invoke({
						app:apps[i],
						hidden:  body.exec? body.exec != apps[i].name : true
					})
					
				}
			}
			else{
				if(apps[i].name== body.exec){
					await global.UserFunction("app.check").invoke({
						app:apps[i],
						update: body.update,
						hidden: !!body.hidden
					})
				}
			}
		}
	}
	
	
	var cw= require(__dirname + "/../config/args")
	if(cw.autoUpdate){
		if(body.startup || ! global.publicContext.cronUpdated || (Date.now() - global.publicContext.cronUpdated > 60*60*1000) ){
			
			// check changes ...
			try{
				await global.UserFunction("app.check").invoke({
					app:{
						link: cw.updateUrl,
						name: 'org.kodhe.apps',
						id: 'org.kodhe.apps'
					},
					update: true,
					hidden: true
				})
			}catch(e){
				
			}
			finally{
				global.publicContext.cronUpdated= Date.now()
			}
			
		}
	}
	
	if(body.exit)
		return 
		
	
	if(!apps){
		var appns= global.publicContext.prse
		var appnp= global.publicContext.prs
		var namesbyport={}
		var pname,papp
		for(var id in appns){
			pname= appns[id]
			papp= appnp[pname]
			if(papp){
				namesbyport[papp.port]= namesbyport[papp.port]||[]
				namesbyport[papp.port].push(id)
			}
		}
		
		//console.info("Namesbyport",namesbyport)
		for(var id in namesbyport){
			var req= new core.VW.Http.Request("http://127.0.0.1:"+id + "/api/function/c/cron.1tasks")
			req.method='POST'
			req.contentType='application/json'
			req.body= {
				names:namesbyport[id]
			}
			req.analizeResponse= false 
			req.beginGetResponse()
		}
		
	}
	
	
}
module.exports= function(global){
	
	F.global= global
	return F 
}