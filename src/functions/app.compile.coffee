import Os from 'os'
import Path from 'path'
import Child from 'child_process'
Fs= core.System.IO.Fs

F= (body)->
	global= F.global 
	arch= body?.arch or Os.arch()
	platform= body?.platform or Os.platform()
	fs= await global.UserContext.require "fs-extra", "7.0.1"
	body.appconfig= body.appconfig ? {}
	appid= body.appconfig.id  ? 'org.kodhe.apps'	
	appname= body.appconfig.name ? "KodheApps"
	appfoldername= body.appconfig.foldername ? "kodhe.apps"
	path_v= Path.normalize(__dirname + "/../package.json")	
	delete require.cache[path_v]
	obfuscate= no

	dirini= Path.join __dirname,".."
	dirfin= Path.join __dirname,"..", "..", "src-out"
	
	convertEs6ToJs= ( file )->
		
		ctx= await global.getSiteContext "kowix"
		return await ctx.userFunction("script.compile").invoke
			file: file
	
	host= global.context?.request?.headers?.host
	if host is "127.0.0.1:3016"
		PublicIp= await global.UserContext.require("public-ip","3.0.0")
		host= await PublicIp.v4()
		host+=":3016"
	else if host is "127.0.0.1"
		PublicIp= await global.UserContext.require("public-ip","3.0.0")
		host= await PublicIp.v4()


	site= await global.getSite()
	https= (global.context?.request?.headers["cf-visitor"]?.indexOf("https") or -1) >= 0
	url= (if https then "https://" else "http://") + "#{host}/site/#{appfoldername}/api/function/c/file.serve?"
	

	copyFunctions= (dirini, dirfin, file)->
		await fs.mkdir( Path.join dirfin, file)
		files3= await Fs.async.readdir(Path.join dirini,file)
		for file0 in files3
			ucontent= ''
			ufile0= Path.join dirini, file, file0 
			stat= await Fs.async.stat ufile0 
			if stat.isDirectory()
				await copyFunctions(Path.join(dirini, file), Path.join(dirfin,file), file0)

			else
				if file0.endsWith(".es6") or file0.endsWith(".coffee")
					ast= await convertEs6ToJs Path.join(dirini,file,file0)
					ucontent= ast.code 
				else if file0.endsWith(".js")
					ucontent= await Fs.async.readFile Path.join(dirini,file,file0), 'utf8'
				
				if ucontent 
					
					if file0.indexOf("host.execute") < 0 and obfuscate
						ucontent= await global.UserFunction("code.obfuscate").invoke
							code: ucontent
					
					if not file0.endsWith(".js")
						newname= file0.substring(0, file0.length-3) + "js"
						if file0.endsWith(".coffee")
							newname= file0.substring(0, file0.length-6) + "js"
					else
						newname= file0	
					await Fs.async.writeFile Path.join(dirfin,file,newname), ucontent
				else 
					await fs.copy Path.join(dirini,file,file0), Path.join(dirfin,file,file0)
		
	
	execute= ()->
		files1= await Fs.async.readdir(dirini)
		files2= await Fs.async.readdir(dirfin)
		
		for file in files2
			if file isnt '.gitignore'
				await fs.remove( Path.join dirfin, file)
		
		for file in files1
			if file is "functions"
				await copyFunctions dirini, dirfin, file

			# copy default apps 
			else if file is "default_apps"
				files3= await Fs.async.readdir(Path.join dirini, file)
				for file2 in files3 
					ufile2=Path.join(dirini,file,file2)
					if not ufile2.startsWith(".")
						stat= await Fs.async.stat(ufile2)
						if stat.isDirectory()
							files4= await Fs.async.readdir(ufile2)
							for file3 in files4 
								if file3 is "functions"
									stat= await Fs.async.stat Path.join(ufile2, file3)
									if stat.isDirectory()
										await copyFunctions(ufile2, Path.join(dirfin, file, file2), "functions")
									else 
										await fs.copy(Path.join(ufile2,file3) , Path.join(dirfin, file, file2, file3))
								else 
									await fs.copy(Path.join(ufile2,file3) , Path.join(dirfin, file, file2, file3))

						else
							await fs.copy(ufile2, Path.join(dirfin, file, file2))
					

			else if file is "dependencies"
				file0= Path.join(dirfin, file)
				file1= Path.join(dirini, file)
				fs.mkdirSync(file0) if not fs.existsSync(file0)
					
				# await fs.copy(Path.join(dirini, files1[i], "kowix"), Path.join(dirfin, files1[i], "kowix"))
				await fs.copy(Path.join(file1, "vox-core"), Path.join(file0, "vox-core"))
				await fs.copy(Path.join(file1, "node_modules"), Path.join(file0, "node_modules"))
				await fs.copy(Path.join(file1, "package.json"), Path.join(file0, "package.json"))
				await fs.copy(Path.join(file1, "kowix.json"), Path.join(file0, "kowix.json"))
				await fs.copy(Path.join(dirini, "..", "native-deps", arch), Path.join(file0, "native-deps"))
				
				
				files3= await Fs.async.readdir(Path.join file1, "kowix")
				await fs.mkdir( Path.join file0, "kowix")
				for file2 in files3 
					if file2 isnt "functions"
						await fs.copy(Path.join(file1, "kowix", file2), Path.join(file0, "kowix", file2))
					else 
						await copyFunctions(Path.join(file1, "kowix"),Path.join(file0, "kowix"), file2)
				# copiar functions ...
				###
				files3= await Fs.async.readdir(Path.join file1, "kowix", "functions")
				await fs.mkdir( Path.join file0, "kowix","functions")
				for file2 in files3 
					ucontent= ''
					if file2.endsWith(".es6") or file2.endsWith(".coffee")
						ast= await convertEs6ToJs Path.join(file1,"kowix","functions",file2)
						ucontent= ast.code 
					else if file2.endsWith(".js")
						ucontent= await Fs.async.readFile Path.join(file1,"kowix","functions",file2), 'utf8'
					
					if ucontent 
						if obfuscate 
							ucontent= await global.UserFunction("code.obfuscate").invoke
								code: ucontent
							
						newname= file2.substring(0, file2.length-3) + "js"
						if file2.endsWith(".coffee")
							newname= file2.substring(0, file2.length-6) + "js"
							
						await Fs.async.writeFile Path.join(file0,"kowix","functions",newname), ucontent
					else 
						await fs.copy Path.join(file1,"kowix","functions",file2), Path.join(file0,"kowix","functions",file2)
				###

			else if file is "frontend"
				await fs.mkdir( Path.join dirfin, "frontend")
				await fs.mkdir( Path.join dirfin, "frontend", "src")
				await fs.copy(Path.join(dirini, "frontend", "src","views"), Path.join(dirfin, "frontend","src","views"))
			
			else if file is "node_modules"
				console.log("here", Path.join(dirini, "..", "..", "kodhe.apps", "src", file))
				await fs.copy(Path.join(dirini, "..", "..", "kodhe.apps", "src", file), Path.join(dirfin, file))
			else if file isnt "functions-compiled" and file isnt ".gitignore"
				await fs.copy(Path.join(dirini, file), Path.join(dirfin, file))
	
	await execute()
	find= (path)->
		PATH= process.env.PATH
		PATHS= PATH.split(";")
		
		for PATH in PATHS
			if Os.platform() isnt "win32"
				p= Path.join PATH, path 
				if Fs.sync.exists p 
					return p 
			else 
				p= Path.join PATH, path + ".exe"
				if Fs.sync.exists p 
					return p 
				
				p= Path.join PATH, path + ".cmd"
				if Fs.sync.exists p 
					return p 
				
				p= Path.join PATH, path + ".bat"
				if Fs.sync.exists p 
					return p 
					
		if Os.platform() isnt "win32"
			return path 
		
		throw global.Exception.create("Path #{path} not found").putCode("NOT_FOUND")
	
	if platform is "win32"
		errs=[]
		pro= Child.spawn find("yarn"), ["dist-win-#{arch}"],
			cwd: dirfin
		task= new core.VW.Task()
		pro.on "error", (e)->
			task.exception= e 
			task.finish()
		pro.stdout.on "data", (d)->
			process.stdout.write d 
		pro.stderr.on "data", (d)->
			process.stderr.write d 
		pro.on "exit", ()->
			if errs.length 
				task.exception= Buffer.concat(errs).toString()
			task.finish()
		await task 
		
		
	else 
		
		if platform isnt Os.platform()
			throw global.Exception.create("Platform #{platform} not supported on this OS").putCode("NOT_SUPPORTED")
		
		errs=[]
		pro= Child.spawn find("yarn"), ["dist-#{arch}"],
			cwd: dirfin
		task= new core.VW.Task()
		pro.on "error", (e)->
			task.exception= e 
			task.finish()
		pro.stdout.on "data", (d)->
			process.stdout.write d 
		pro.stderr.on "data", (d)->
			process.stderr.write d 
		pro.on "exit", ()->
			if errs.length 
				task.exception= Buffer.concat(errs).toString()
			task.finish()
		await task 
	
	

	upload= ()->
		version= require(path_v).version 	
		if platform is "win32"
			distUpdate= __dirname + "/../../dist-update/win-" + arch
			if not Fs.sync.exists(distUpdate)
				Fs.sync.mkdir(distUpdate)
			
			if Fs.sync.exists(distUpdate + "/#{appname}-Setup-" + arch + ".exe")
				await fs.remove(distUpdate + "/#{appname}-Setup-" + arch + ".exe")
			
			filein= Path.join __dirname, "..", "..", "dist", "#{appname} Setup " + version + ".exe"
			fileout= Path.join(distUpdate, "#{appname}-Setup-" + arch + ".exe")
			if Fs.sync.exists filein 
				await Fs.async.rename filein, fileout 
			
			
			if Fs.sync.exists fileout 
				diskCtx= await global.getSiteContext "disk.kodhe.com"
				drive= await diskCtx.userFunction("drive").invoke 
					account: 'main'
					action: 'get'
				
				folder=await drive.getFolderOrCreateAndShare("projects/#{appfoldername}/win/#{arch}")
				file= await drive.list 	
					q: "name = '#{appname}-Setup-#{arch}.exe' and '#{folder.id}' in parents"			
				if file.files?[0]	
					await drive.delete
						fileId: file.files?[0].id 
				
				await drive.createFile
					resource:
						name: "#{appname}-Setup-#{arch}.exe"
						mimeType:  "text/plain"
						parents: [folder.id]
					media: 
						body: fs.createReadStream fileout 
						mimeType: 'text/plain'

				return "https://storage-ns01.d0stream.com/.drive/Projects/#{appfoldername}/win/#{arch}/#{appname}-Setup-#{arch}.exe"

		if platform is "darwin"
			distUpdate= __dirname + "/../../dist-update/darwin"
			if not Fs.sync.exists(distUpdate)
				Fs.sync.mkdir(distUpdate)
			
			if Fs.sync.exists(distUpdate + "/#{appname}.dmg")
				await fs.remove(distUpdate + "/#{appname}.dmg")
			
			filein= Path.join __dirname, "..", "..", "dist", "#{appname}-#{version}.dmg"
			fileout= distUpdate + "/#{appname}.dmg"

			if Fs.sync.exists filein 
				await Fs.async.rename filein, fileout 
			
			
			if fs.existsSync fileout 
				diskCtx= await global.getSiteContext "disk.kodhe.com"
				drive= await diskCtx.userFunction("drive").invoke 
					account: 'main'
					action: 'get'
				
				folder=await drive.getFolderOrCreateAndShare("projects/#{appfoldername}/darwin")
				file= await drive.list 	
					q: "name = '#{appname}.dmg' and '#{folder.id}' in parents"			
				if file.files?[0]	
					await drive.delete
						fileId:file.files?[0].id 
				
				await drive.createFile
					resource:
						name: "#{appname}.dmg"
						mimeType:  "text/plain",
						parents: [folder.id]
					media: 
						body: fs.createReadStream fileout 
						mimeType: 'text/plain'


				return "https://storage-ns01.d0stream.com/.drive/Projects/#{appfoldername}/darwin/#{appname}.dmg"
			
		if platform is "linux"
			deb1=  if arch is "x64" then "amd64" else "i386"
		
			distUpdate= __dirname + "/../../dist-update/linux-#{arch}"
			if not Fs.sync.exists(distUpdate)
				Fs.sync.mkdir(distUpdate)
			
			
				
			
			if Fs.sync.exists(distUpdate + "/#{appname}-#{deb1}.deb")
				await fs.remove(distUpdate + "/#{appname}-#{deb1}.deb")
			
			if Fs.sync.exists(distUpdate + "/#{appname}.run")
				await fs.remove(distUpdate + "/#{appname}.run")
				
				
			filein= Path.join __dirname, "..", "..", "dist", "#{appid}_#{version}_#{deb1}.deb"
			fileoutname= "#{appname}-#{deb1}.deb"
			fileout= distUpdate + "/" + fileoutname
			if Fs.sync.exists filein 
				await Fs.async.rename filein, fileout
			else 
				filein= Path.join __dirname, "..", "..", "dist", "#{appname} #{version}.AppImage"
				if Fs.sync.exists filein 
					fileoutname= "#{appname}.run"
					fileout=distUpdate + "/" + fileoutname
					await Fs.async.rename filein, fileout 
			
			
			if Fs.sync.exists fileout 

				
				diskCtx= await global.getSiteContext "disk.kodhe.com"
				drive= await diskCtx.userFunction("drive").invoke 
					account: 'main'
					action: 'get'
				
				folder=await drive.getFolderOrCreateAndShare("projects/#{appfoldername}/linux/#{arch}")
				file= await drive.list 	
					q: "name = '#{fileoutname}' and '#{folder.id}' in parents"			
				if file.files?[0]	
					await drive.delete
						fileId: file.files?[0].id 
				
				await drive.createFile
					resource:
						name: fileoutname
						mimeType:  "text/plain"
						parents: [folder.id]
					media: 
						body: fs.createReadStream fileout 
						mimeType: 'text/plain'
				return "https://storage-ns01.d0stream.com/.drive/Projects/#{appfoldername}/linux/#{arch}/#{fileoutname}"
			
	
	urlu= await upload()
	if urlu 
		return 
			url: urlu 

module.exports= (global)->
	F.global= global 
	F
						
				