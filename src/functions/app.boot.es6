/*global core, Varavel*/
var F= function(body){}
import Os from 'os'
var Fs= core.System.IO.Fs 
import Path from 'path'

var L= async function(body){
    var uglobal= F.global 
    var finish
    const electron = require('electron')


    // Module to control application life.
    const app = electron.app
    
    
    
    
    var home = require("os").homedir();
    var documentFolder = Path.normalize(home + '/Documents/')
    if(Os.platform()=="linux" && !Fs.sync.exists(documentFolder)){
        var dirsConfig= Path.join(home, ".config", "user-dirs.dirs")
        var content,line, doc
        if(Fs.sync.exists(dirsConfig)){
            content= await Fs.async.readFile(dirsConfig,'utf8')
            content= content.split("\n")
            while((line=content.shift())!==undefined){
                if(line.startsWith("XDG_DOCUMENTS_DIR")){
                    doc= line.split("=")[1]
                    doc= doc.replace(/\$([\w|\_]+)/ig, function(a,b){
                        return process.env[b]
                    })
                    if(doc.startsWith('"'))
                        doc=JSON.parse(doc)
                    break 
                }
            }
        }
        if(doc)
            await Fs.async.symlink(doc, documentFolder.substring(0, documentFolder.length-(documentFolder.endsWith("/")?1:0)))
        else 
            await Fs.async.mkdir(documentFolder)
    }
    
    
    var kodheappsFolder= require(__dirname + "/../config/args").folder
    var appFolder= Path.join(documentFolder, kodheappsFolder)
    if(!Fs.sync.exists(appFolder))
        await Fs.async.mkdir(appFolder)
    
    
    var mainWindow = null
    var cw= require(__dirname + "/../config/args")
    
    
    var defaultApps= Path.join(__dirname,"..","default_apps")
    var n1,n2, stat, d2
    if(Fs.sync.exists(defaultApps)){
        var dirs= await Fs.async.readdir(defaultApps)
        for(var i=0;i<dirs.length;i++){
            stat= await Fs.async.stat(Path.join(defaultApps,dirs[i]))
            if(stat.isDirectory()){
                n1= Path.join(appFolder, dirs[i])
                if(!Fs.sync.exists(Path.join(n1, "kodhe.app.json"))){
                	n2= Path.join(defaultApps,dirs[i])
                	d2= await Fs.async.readdir(n2)
                	if(!Fs.sync.exists(n1))
                		await Fs.async.mkdir(n1)
                	for(var y=0;y<d2.length;y++){
                		await require("fs-extra").copy(Path.join(n2,d2[y]), Path.join(n1, d2[y]))	
                	}
                }
            }
        }
    }
    
    
    
    var next, startArgs
    var revise = await uglobal.UserFunction("app.boot.cmd").invoke({
        getfunc: true 
    })
    
    
    next = revise(process.argv)
    startArgs= next.startArgs
    next=next.next
    


    var url
    var port = require(__dirname + "/../config/0port")
    url  = "http://127.0.0.1:" + port + "/"
    global.url= url 


    // Module to create native browser window.
    const BrowserWindow = electron.BrowserWindow
    const path = require('path')

    //const url = require('url')

    // Keep a global reference of the window object, if you don't, the window will
    // be closed automatically when the JavaScript object is garbage collected.

    var iconpath = __dirname + "/../favicon.png"
    var iconpath2 = __dirname + "/../favicon.2.png"
    var home= Os.homedir()
    var kodhe_hide= Path.join(home,"kodhe-start-hide")
    var content
    
    
    
    if(Fs.sync.exists(kodhe_hide)){
        content= await Fs.async.readFile(kodhe_hide,'utf8')
        if(content==1){
            startArgs.hide= true 
        }
        try{
            await Fs.async.unlink(kodhe_hide)
        }catch(e){
            try{
                await Fs.async.writeFile(kodhe_hide,'0')
            }catch(e){}
        }
    }
    
    
    
    /*
    if(Os.platform()=="darwin")
        iconpath= __dirname + "/256x256.png.hpx";
    */


    global.iconpath = iconpath

    function createWindow() {



        mainWindow = new BrowserWindow({
            width: 650,
            height: 580,
            "minWidth": 560,
            "minHeight": 580,
            icon: iconpath,
            nodeIntegration: true,
            show: false
        })


        mainWindow.on("hide", function(){
            mainWindow.hidden1= true 
        })
        mainWindow.on("show", function(){
            mainWindow.hidden1= false 
        })

        global.mainWindow = mainWindow
        //mainWindow.setMenu(null)
        mainWindow.hide()
        
        if(Os.platform()=="win32"){
            mainWindow.setAppDetails({
                appId: "org.kodhe.apps",
                appIconPath: Path.join(__dirname,"..","build", "icons","app.ico"),
                relaunchCommand: '"' +process.execPath+'"',
                relaunchDisplayName: "Kodhe Apps"
            }) 
        }

        var addGoodMenu = function() {
            var Menu = require("electron").Menu
            var subm=[
                { label: "About Application", selector: "orderFrontStandardAboutPanel:" },
                { type: "separator" },
                { label: "Quit", accelerator: "CmdOrCtrl+Q", click: function() { global.appQuit(); } }
            ]
            if(Os.platform()!="darwin"){
                subm=[
                    { label: "Quit", accelerator: "CmdOrCtrl+Q", click: function() { global.appQuit(); } }
                ]
            }
            var template = [{
                label: "Application",
                submenu: subm
            }, {
                label: "Edit",
                submenu: [
                    { label: "Undo", accelerator: "CmdOrCtrl+Z", selector: "undo:" },
                    { label: "Redo", accelerator: "Shift+CmdOrCtrl+Z", selector: "redo:" },
                    { type: "separator" },
                    { label: "Cut", accelerator: "CmdOrCtrl+X", selector: "cut:" },
                    { label: "Copy", accelerator: "CmdOrCtrl+C", selector: "copy:" },
                    { label: "Paste", accelerator: "CmdOrCtrl+V", selector: "paste:" },
                    { label: "Select All", accelerator: "CmdOrCtrl+A", selector: "selectAll:" }
                ]
            }];
            
            var template2 = [{
                label: "Edit",
                submenu: [
                    { label: "Undo", accelerator: "CmdOrCtrl+Z", selector: "undo:" },
                    { label: "Redo", accelerator: "Shift+CmdOrCtrl+Z", selector: "redo:" },
                    { type: "separator" },
                    { label: "Cut", accelerator: "CmdOrCtrl+X", selector: "cut:" },
                    { label: "Copy", accelerator: "CmdOrCtrl+C", selector: "copy:" },
                    { label: "Paste", accelerator: "CmdOrCtrl+V", selector: "paste:" },
                    { label: "Select All", accelerator: "CmdOrCtrl+A", selector: "selectAll:" }
                ]
            }];
            
            

            if(Os.platform()=="darwin"){
                Menu.setApplicationMenu(Menu.buildFromTemplate(template))   
            }
            else{
                mainWindow.setMenu(Menu.buildFromTemplate(template))
            }
            
        }
        addGoodMenu()


        setTimeout(function() {
                
            if(startArgs.hide===false && cw.createManager)
                mainWindow.show()
        }, 2000)
        
        
        /*
      mainWindow.on('minimize',function(event){
            event.preventDefault()
                mainWindow.hide();
        });

    */


        mainWindow.on('close', function(event) {
            if (!app.isQuiting) {
                event.preventDefault()
                mainWindow.reload()
                mainWindow.hide()
            }
            return false;
        });

        //var QueryBase = Varavel.Project.App.QueryBase
        //QueryBase.vars = QueryBase.vars || {}
        //QueryBase.vars["127.0.0.1"] = {}
        uglobal.publicContext["127.0.0.1"]=uglobal.publicContext["127.0.0.1"]||{}   
        var finish = uglobal.publicContext["127.0.0.1"].finish = function() {
            if (global.onexit) {

                var t = require("./async-execute").default(global.onexit)
                var task = new core.VW.Task()
                if (t.then) {
                    t.then(function() {
                        task.finish()
                        process.exit(0)
                        process.kill(process.pid)
                    }).catch(function(e) {
                        task.exception = e
                        task.finish()
                        process.exit(0)
                        process.kill(process.pid)
                    })
                }
                else {
                    process.exit(0)
                    process.kill(process.pid)
                }
                return task
            }
            else {
                process.exit(0)
                process.kill(process.pid)
            }
        }

        
        global.appQuit = function() {
            app.isQuiting = true;
            app.quit();
            finish()
        }
        
        
        
        if(cw.createManager){
            var appIcon = new electron.Tray(iconpath2)
            var contextMenu = electron.Menu.buildFromTemplate([
    
                {
                    label: 'Abrir gestor',
                    click: function() {
                        mainWindow.show();
                    }
                }
                //,
                //{ label: 'Salir', click: global.appQuit }
            ]);
            appIcon.setContextMenu(contextMenu);
        }
        


            
        mainWindow.on('show', function() {
            appIcon &&appIcon.setHighlightMode('selection')
        })

        if(cw.createManager){
            // and load the index.html of the app.
            mainWindow.loadURL(url)
        }
        
        // Open the DevTools.
        // mainWindow.webContents.openDevTools()

        // Emitted when the window is closed.
        mainWindow.on('closed', function() {
            // Dereference the window object, usually you would store windows
            // in an array if your app supports multi windows, this is the time
            // when you should delete the corresponding element.
            mainWindow = null
        })


    }




    // This method will be called when Electron has finished
    // initialization and is ready to create browser windows.
    // Some APIs can only be used after this event occurs.
    

    // Quit when all windows are closed.
    

    global.createWindow=function(){
        if(mainWindow==null && cw.createManager){
            createWindow()
            
            app.on('window-all-closed', function() {
                // On OS X it is common for applications and their menu bar
                // to stay active until the user quits explicitly with Cmd + Q
                if (process.platform !== 'darwin') {
                    app.quit()
                }
        
                finish()
            })
        }
    }

    // In this file you can include the rest of your app's specific main process
    // code. You can also put them in separate files and require them here.

    if(cw.start ){
        var req = new core.VW.Http.Request(url + "api/function/c/app.check")
        try{
            //req.analizeResponse = false
            req.method='POST'
            req.contentType='application/json'
            req.body=cw.start
            cw.start.hidden= startArgs.hide!==false
            await req.getResponseAsync()
        }catch(e){
            throw e
        }finally{
            req&&req.innerRequest && (delete req.innerRequest.callback)
        }
    }



    var cron = function(startArgs) {
        var req = new core.VW.Http.Request(url + "api/function/c/cron.0tasks")
        req.analizeResponse = false
        req.method='POST'
        req.contentType='application/json'
        req.body=startArgs
        req.getResponseAsync()
    }
    var Int = setInterval(cron, 120000)
    Int.unref()
    startArgs.firsttime= true
    cron(startArgs)

}

module.exports=function(global){
    F.global= global 
    return L
}