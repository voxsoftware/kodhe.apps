
var F= async function(body){
	var names= body.names
	var global= F.global 
	var name,ctx,ex
	ex=function(ctx,name){
		ctx.userFunction("cron.0tasks").invoke({}).catch(function(e){
			console.error("Error in cron for: ", name, e)
		})
	}
	for(var i=0;i<names.length;i++){
		name= names[i]
		ctx= await global.getSiteContext(name)
		ex(ctx,name)
	}
	
	
}
module.exports= function(global){
	F.global= global
	return F 
}