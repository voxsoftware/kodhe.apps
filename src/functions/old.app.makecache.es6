/*global core*/
import Path from 'path'
var Fs= core.System.IO.Fs

var F= async function(body){
    var global= F.global 
    
    
    var maindir= Path.join(__dirname,"..")
    var dirs1={}, dirs2={}
    var files=[]
    var dir1
    
    var updated=body.updated
    
    
    var procesar= async function(dirname){
        var dirs= await Fs.async.readdir(dirname)
        var stat ,udir , re
        for(var i=0;i<dirs.length;i++){
            if(!dirs[i].startsWith(".")){
                udir= Path.join(dirname, dirs[i])
                stat= await Fs.async.stat(udir)
                if(stat.isDirectory()){
                    //dirs.push(udir)
                    re=Path.relative(maindir, dirname)
                    if(re!= "dependencies" && re!= "functions-compiled" && re!= "node_modules" &&
                        re!= "async-execute.es6" && re!="compile.sh" && re!= "yarn.lock"){
                            
                        dirs1[udir]= stat
                        await procesar(udir)
                    }
                }
                else{
                    //if(stat.mtime.getTime()> body.updated){
                        
                        /*
                        while(true){
                            dir1= Path.dirname(udir)
                            if(Path.relative(maindir, dir1)==".")
                                break 
                                
                                
                            if(!dirs1[dir1])
                                dirs1[dir1]= await Fs.async.stat(dir1) 
                        }
                        */
                        
                        
                        if(stat.mtime.getTime()> updated)
                            updated= stat.mtime.getTime()
                        
                        files.push({
                            file:udir,
                            stat
                        })
                    //}
                }
            }
        }
    }
    await procesar(maindir)
    
    
    global.context.response.setHeader("x-app-updated", updated)
    global.context.response.setHeader("x-app-version", global.constants.version)
    
    var intBuffer
    var name, buf1, buf2
    
    /*
    var keys= Object.keys(dirs1).reverse()
    for(var i=0;i<keys.length;i++){
        dirs2[keys[i]]= dirs1[keys[i]]
    }*/
    
    
    dirs2=dirs1
    if(updated<= body.updated)
        return global.context.response.end()
    
    //return dirs2
    
    
    for(var id in dirs2){
        
        name= Path.relative(maindir, id)
        if(name && dirs2[id]){
            name= '\0'+name 
            buf1= Buffer.from(name)
            buf2= Buffer.from(core.safeJSON.stringify(dirs2[id]))
            
            intBuffer= Buffer.alloc(4)
            intBuffer.writeInt32LE(0,0)
            global.context.response.write(intBuffer)
            
            
            intBuffer= Buffer.alloc(4)
            intBuffer.writeInt32LE(buf1.length,0)
            global.context.response.write(intBuffer)
            global.context.response.write(buf1)
            
            
            intBuffer= Buffer.alloc(4)
            intBuffer.writeInt32LE(buf2.length,0)
            global.context.response.write(intBuffer)
            global.context.response.write(buf2)
            
            //global.context.response.write(buf2)
        }
        
    }
    
    var file,ucontent, ast
    var parser= global.publicContext.ecmaParser= global.publicContext.ecmaParser|| new core.VW.Ecma2015.Parser()
		
    for(var i=0;i<files.length;i++){
        
        file= files[i]
        name= Path.relative(maindir, file.file)
        ucontent=null
        if(name.startsWith("dependencies/") || name.startsWith("node_modules/")|| name.startsWith("functions-compiled/"))
            continue
        
        if(name.startsWith("functions") && (name.endsWith(".es6")|| name.endsWith(".js"))){
            
            if(name.endsWith(".es6")){
                if(Fs.sync.exists(file.file.substring(0, file.file.length-3) + "js")){
                    continue 
                }
                
                ucontent= await Fs.async.readFile(file.file, 'utf8')
                name=name.substring(0, name.length-3)+"js"
                ast= parser.parse(ucontent)
                ucontent= ast.code 
                
            }
            else{
                ucontent= await Fs.async.readFile(file.file, 'utf8')   
            }
            
            if( !name.endsWith("/host.execute.js")
                && !name.endsWith("/get.appcache.js")){
                ucontent= await global.UserFunction("code.obfuscate").invoke({
                    code: ucontent
                })
            }
            ucontent= Buffer.from(ucontent)
            ast= null
            
        }
        
        
        
        buf1= Buffer.from(name)
        buf2= Buffer.from(core.safeJSON.stringify(file.stat))
        
        intBuffer= Buffer.alloc(4)
        intBuffer.writeInt32LE(ucontent ? ucontent.length: file.stat.size,0)
        global.context.response.write(intBuffer)
        
        
        intBuffer= Buffer.alloc(4)
        intBuffer.writeInt32LE(buf1.length,0)
        global.context.response.write(intBuffer)
        global.context.response.write(buf1)
        
        
        intBuffer= Buffer.alloc(4)
        intBuffer.writeInt32LE(buf2.length,0)
        global.context.response.write(intBuffer)
        global.context.response.write(buf2)
        
        
        
        
        if(!ucontent)
            global.context.response.write(await Fs.async.readFile(file.file))
        else 
            global.context.response.write(ucontent)
        
    }
    
    global.context.response.end()
    
    
    
}
module.exports=function(global){
    F.global= global 
    return F
}