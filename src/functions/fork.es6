import Child from 'child_process'
var other = function(port, reopen, task) {
	var allargs= arguments
	var cw= global._defaultArgs
	var env = {},
		closed
	for (var id in process.env) {
		env[id] = process.env[id]
	}
	env.INTERFACE = "false"
	if (!env.SERVER_PORT) {
		env.SERVER_PORT = cw.port || "49602"
	}
	env.ELECTRON_RUN_AS_NODE = "1"
	env.SERVER_PORT =  port===undefined ? (parseInt(env.SERVER_PORT) + 1).toString() : port


	var file = "node-dev.js"
	if (global.PROD_MODE) {
		file = "node.js"
	}
	
	var wn
	var p = Child.spawn(process.execPath, [__dirname + "/../" + file], {
		env
	})

	p.stdout.on("data", function(b) {
		process.stdout.write("PID [" + p.pid + "] STDOUT: ")
		
		
		if(wn){
			if(/\d/.test(b.toString()[0])){
				port= parseInt(b.toString().trim())
				task.result= port
				task.finish()
				task=null
				wn= false
			}
		}
		
		else if(task){
			if(b.toString().toUpperCase().indexOf("SERVER HTTP")>=0 &&
				b.toString().toUpperCase().indexOf("127.0.0.1")>=0){
				
				
				var port=0 
				var y= b.toString().lastIndexOf(":")
				port= b.toString().substring(y+1)
				if(!port){
					wn= true
					return 
				}
				//console.info("NEW SERVER?? ", port)
				//process.exit() 
				
				port= parseInt(port)
				task.result= port
				task.finish()
				task=null
			}
		}
		process.stdout.write(b)
	})

	p.stderr.on("data", function(b) {
		process.stderr.write("PID [" + p.pid + "] STDERR: ")
		process.stderr.write(b)
	})


	p.on("exit", function() {
		if (!closed && reopen && !p._force)
			process.nextTick(function(){
				other(allargs[0], allargs[1],allargs[2])
			})
	})
	
	p.on("error",function(er){
		if(task){
			task.exception= er 
			task.finish()
			task=null
		}
	})

	process.on("exit", function() {
		closed = true
		p.kill()

		setTimeout(function() {
			p.kill('SIGKILL')
		}, 100)

	})
	
	return p 
}
var F= async function(){
	return other 
}
module.exports= function(global){
	return F
}