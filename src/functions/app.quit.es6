/*global core*/
var F
var Os= require("os")
F= async function(body){
    
    var global= F.global 
    global.publicContext.prs=global.publicContext.prs||{}
    
    body.idSite= body.idSite || body.name
    var idSite=  body.idSite  || "user-api"
    var denied= false 
    var prefix= (new Buffer(idSite)).toString("hex")
    var ecode= ''
    if(body.kodhe_apps_code){
        ecode= body.kodhe_apps_code
    }
    var cw= require(__dirname + "/../config/args")
    
    
    if(ecode && !denied ){
        var ucode= prefix + (await global.UserFunction("machine.id").invoke()).id
        if(ucode!= ecode)
            denied= true
    }
    if(!denied && !ecode)
        denied=true
    if(denied)
        throw global.Exception.create("Access denied to use machine resources").putCode("ACCESS_DENIED")
        
        
    var name= body.processName||body.name
    var uname=body.processName||"default"
    
    
    delete global.publicContext.prsi[name]
    
    var p=global.publicContext.prs[name]
    if(p){
        
        if(p.port== 49603){
            
            delete global.publicContext.prs[name]
            var keys= Object.keys(global.publicContext.prs)
            if(keys.length==1 && keys[0]=="default"){
                if(body.quit){
                    // kodheapps is a framework also for web, is a service
                    // so don't close all process
                    if(cw.service===false){ 

                        if(Os.platform()!="darwin")
                            require("electron").app.quit()
                        
                        process._force= true
                        process.exit(0)
                        process.kill(process.pid)
                        
                    }
                }
            }
            
        }
        else{
            
            p.kill('SIGINT')
            delete global.publicContext.prs[name]
            var keys= Object.keys(global.publicContext.prs)
            if(keys.length==0){
                if(body.quit){
                    // kodheapps is a framework also for web, is a service
                    // so don't close all process
                    if(cw.service===false)  {
                        if(Os.platform()!="darwin")
                            require("electron").app.quit()
                        
                        process._force= true
                        process.exit(0)
                        process.kill(process.pid)
                    }
                }
            }
            
        }
        
    }
    
    
}

module.exports=function(global){
    F.global= global 
    return F 
}