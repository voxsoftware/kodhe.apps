/*global core*/
import Os from 'os'
var Fs= core.System.IO.Fs 
import Path from 'path'
var F= async function(body){
    body=body ||{}
    var mainWindow= global.mainWindow
    var commandLine= body.commandLine 
    var next, startArgs
    
    var cw= require(__dirname + "/../config/args")
    var revise = (cmd) => {
		next= ''
		var startArgs
		var Command = new core.VW.CommandLine.Parser()
		Command.addParameter('hide', true, false)
		Command.addParameter('check', true, false)
		Command.addParameter('startup', true, false)
		Command.addParameter('exec', true, null)
		Command.addParameter('appargs', true, null)
		Command.parse(null, false, cmd)
		
		startArgs= Command.getAllParameters()
		startArgs.startup= startArgs.startup!==false
		if(startArgs.exec)
			startArgs.hide= true
		
		console.log("START ARGS:", startArgs)
		return {
			next,
			startArgs
		}
	}
	
	if(body.getfunc)
	    return revise
    
    
    // Someone tried to run a second instance, we should focus our window.
	if (true) {
		console.info("commandLine", commandLine)
		next = revise(commandLine)
		startArgs=next.startArgs
		next= next.next
		if (mainWindow && next) {
			 mainWindow.loadURL(global.url + next)
		}
		
		
		if(startArgs.check!==false){
			var home= Os.homedir()
			var kodhe_hide= Path.join(home,"kodhe-start-hide")
			var content
			
			if(Fs.sync.exists(kodhe_hide)){
				content= await Fs.async.readFile(kodhe_hide,'utf8')
				if(content==1){
					startArgs.hide= true 
				}
				try{
					await Fs.async.unlink(kodhe_hide)
				}catch(e){
					try{
						await Fs.async.writeFile(kodhe_hide,'0')
					}catch(e){}
				}
			}	
		}
		
		
		if(cw.start &&cw.start.name && !startArgs.exec){
			startArgs.exec= cw.start.name 
			startArgs.hide= true
		}
		

		if(mainWindow && startArgs.hide===false){
			if(!body.macosActivate || mainWindow.hidden1){
				mainWindow.show()
				
				if (mainWindow.isMinimized()) mainWindow.restore()
					mainWindow.focus()
			}
		}
		
		
		
		
		
		console.info("startArgs", startArgs)
		
		
		if(startArgs.exec){
			var cron = function(startArgs) {
				var req = new core.VW.Http.Request(global.url + "api/function/c/cron.0tasks")
				req.analizeResponse = false
				req.method='POST'
				req.contentType='application/json'
				req.body=startArgs
				req.getResponseAsync()
			}
			startArgs.exit= true
			cron(startArgs)
		}
		
			
	}
		
}

module.exports= function(global){
	return F
}