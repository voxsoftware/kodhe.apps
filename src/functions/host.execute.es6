var F, eval1


eval1= async function(uglobal, body, app, idSite){
	
	var global = await uglobal.getSiteContext(idSite)
	uglobal= null 
		
	
	var f
	
	var F= function(){}
	F.global= global
	
	
	if(body.code){
		f=  eval(body.code)
		
			
		
		if(typeof f=="function"){
			f.global= global 
			return await f(body)
		}
		else {
			return await f
		}
	}
	else if(body.script){
		return await global.userFunction(body.script).invoke(body)
	}
	
}

F= async function(body){
	var global= F.global 
	
	
	var idSite=  body.idSite  || "user-api"
	var denied= false 
	var prefix= (new Buffer(idSite)).toString("hex")
	var ecode= ''
	if(body.kodhe_apps_code){
		ecode= body.kodhe_apps_code
	}
	
	
	if(ecode && !denied ){
		var ucode= prefix + (await global.UserFunction("machine.id").invoke()).id
		if(ucode!= ecode)
			denied= true
	}
	if(!denied && !ecode)
		denied=true
	if(denied)
		throw global.Exception.create("Access denied to use machine resources").putCode("ACCESS_DENIED")
	
	
	
	var app 
	if(body.appinfo){
		
		var appInfo= global.UserFunction("get.appcache")
		await appInfo.load()
		app= await appInfo.execute(body)
		
	}
	
	/*
	var LocalUserFunction= global.UserFunction 
	var LocalContext 
	LocalContext= global.publicContext 
	global.publicContext= LocalContext[idSite]= LocalContext[idSite]||{}
	global.globalContext= LocalContext[".global"]= LocalContext[".global"]||{}
	
	
	
	var f=  eval(body.code)
	if(typeof f=="function"){
		f.global= global
		return await f(body)

	}
	else {
		return await f
	}*/
	
	return await eval1(global, body, app, idSite)
	
}
module.exports=function(global){
	F.global= global 
	return F 
}