/*global core*/
var F

F= async function(data){
    
    var uglobal= F.global 
    if(data.name=="org.kodhe.apps")
        return 
        
    var fork= await uglobal.userFunction("fork").invoke()        
    data.processName=data.processName||"default"
    var pr_name= data.processName 
    
    uglobal.publicContext.prs=uglobal.publicContext.prs||{}
    uglobal.publicContext.prsi=uglobal.publicContext.prsi||{}
    uglobal.publicContext.prse=uglobal.publicContext.prse||{}
    
    
    while(uglobal.publicContext.prsi[pr_name]){
        await core.VW.Task.sleep(100)
    }
    
    try{
        if(!uglobal.publicContext.prs[pr_name]){
            
            uglobal.publicContext.prsi[pr_name]= true 
            
            // create process ...
            var task= new core.VW.Task()
            var reopen= true
            var port= data.port || "0"
            if(port==49603 && pr_name!="default"){
                throw uglobal.Exception.create("Port " + port + " is reserved")
            }
            if(pr_name=="default"){
                port=undefined
                reopen= true 
            }
            
            uglobal.publicContext.prs[pr_name] = fork(port, reopen, task)
            if(pr_name=="default"){
                uglobal.publicContext.prs[data.name]=     uglobal.publicContext.prs[pr_name]
            }
            uglobal.publicContext.prs[pr_name].port= await task
            uglobal.publicContext.prse[data.name]= pr_name  
            
            
        }
        else{
            if(pr_name=="default"){
                uglobal.publicContext.prs[data.name]=     uglobal.publicContext.prs[pr_name]
            }
            uglobal.publicContext.prse[data.name]= pr_name
        }
        
    }catch(e){
        throw e 
    }
    finally{
        delete uglobal.publicContext.prsi[pr_name]
    }
    
    return {
        port: uglobal.publicContext.prs[pr_name].port
    }
    
}

module.exports=function(global){
    F.global= global 
    return F 
}