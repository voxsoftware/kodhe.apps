
import Path from 'path'
import Zlib from 'zlib'
Fs= core.System.IO.Fs
import fs from 'fs'

F= (body={})->
    global= F.global 
    maindir= Path.join __dirname, ".."
    files=[]
    updated= parseInt body.updated
    streamOut= global.context.response
    body.key=body.key or 0
    keys= global.constants.key
    key= keys.filter (a)->  a.id is body.key 
    throw global.Exception.create "Invalid key. Don't have permission to download" if not key.length 
    
    key=key[0]
    
    streamOut.setTimeout 300000 
    
    invalid=[
        ".git",
        "functions-compiled",
        "dependencies/vox-core",
        "dependencies/kowix/.git",
        "dependencies/native-deps",
        "dependencies/node-modules",
        "dependencies/instructions.MD",
        "node_modules",
        "compile.sh",
        "yarn.lock"
    ]
    
    dirs1= {}
    
    options=[]
    relativefiles= []
    relativedir= []

    procesar=  (dirname)->
        dirs= await Fs.async.readdir dirname       
        
        for dir, i in dirs 
            if not dir.startsWith "."
                udir= Path.join dirname, dir 
                stat= await Fs.async.stat udir 
                re= Path.relative maindir,dirname 
                
                if stat.isDirectory() 
                    if invalid.indexOf(re) < 0
                        options.push re 
                        dirs1[udir]= stat 
                        await procesar udir 
                    
                else 
                    if invalid.indexOf(re) < 0
                        options.push re 
                        if stat.mtime.getTime() > updated
                            updated= stat.mtime.getTime()
                        relativefiles.push  
                            file: Path.join re, dir 
                            stat: stat 

                        files.push
                            file:udir
                            stat:stat 
                 
    await procesar maindir
    
    
    global.context.response.setHeader("x-app-updated", updated)
    global.context.response.setHeader("x-app-version", global.constants.version)
    
    
    dirs2=dirs1
    for id, stat  of dirs2 
        name= Path.relative(maindir, id)
        relativedir.push 
            type: 'folder'
            file: name 
            stat: stat 
    relativefiles= relativedir.concat relativefiles


    if parseInt(body?.structure) is 1 
        return
            maindir: maindir
            files: relativefiles 
            updated: updated 
            version: global.constants.version 


    return global.context.response.end() if updated <= body.updated
    
    
    enc= global.context.request.headers["accept-encoding"]
    enc=null
    if enc and enc.indexOf("gzip") >= 0 
        streamOut.setHeader("content-encoding","gzip")
        streamOut= Zlib.createGzip()
        streamOut.pipe(global.context.response)
    
    
    bufs=[]
    for id of dirs2
        name= Path.relative(maindir, id)
        
        if name and dirs2[id] 
            
            
            
            name= '\0'+name 
            buf1= Buffer.from(name)
            buf2= Buffer.from(core.safeJSON.stringify(dirs2[id]))
            
            intBuffer= Buffer.alloc(4)
            intBuffer.writeInt32LE(0,0)
            bufs.push(intBuffer)
            
            
            intBuffer= Buffer.alloc(4)
            intBuffer.writeInt32LE(buf1.length,0)
            bufs.push(intBuffer)
            bufs.push(buf1)
            
            
            intBuffer= Buffer.alloc(4)
            intBuffer.writeInt32LE(buf2.length,0)
            bufs.push(intBuffer)
            bufs.push(buf2)
            
    
    
    task= new core.VW.Task()
    if streamOut.write(Buffer.concat(bufs)) is no 
        streamOut.once "drain", task.finish.bind(task)
        await task 
    
    
    
    convertEs6ToJs= ( file )->
        
        ctx= await global.getSiteContext "kowix"
        return await ctx.userFunction("script.compile").invoke
            file: file.file 
            mtime: file.stat.mtime.getTime()
            
    
    
    # Ya no usar más el parser de vox-core que no es completo ...
    #var parser= global.publicContext.ecmaParser= global.publicContext.ecmaParser|| new core.VW.Ecma2015.Parser()
		
	
    for file,i in files 
        bufs=[]
        name= Path.relative maindir, file.file 
        ucontent= null 
	    
        try 
            if (name.indexOf("functions") >= 0) and (name.endsWith(".es6") or name.endsWith(".coffee") or name.endsWith(".js"))
                if name.endsWith(".es6") or name.endsWith(".coffee")
                    if Fs.sync.exists file.file.substring(0,file.file.length - 3) + "js"
                        continue 
	                
                    #ucontent= await Fs.async.readFile(file.file, 'utf8')   
                    if (not name.endsWith(".coffee")) or (key.permission.indexOf("obfuscate") >= 0)
                        if name.endsWith ".coffee"
                            name= (name.substring 0, name.length-6)+"js"
                        else 
                            name= (name.substring 0, name.length-3)+"js"
                        ast= await convertEs6ToJs file
                        ucontent= ast.code 
                
                else 
                    ucontent= await Fs.async.readFile(file.file, 'utf8')   
                
                
                
                if ucontent
                    try 
                        if key.permission.indexOf("obfuscate") >= 0 and name.indexOf("host.execute") < 0
                            ucontent= await global.UserFunction("code.obfuscate").invoke
                                code: ucontent
                    catch e 
                        # nothing ignore error
                ucontent= ucontent or " " 
                ucontent= Buffer.from ucontent
                ast= null
                
            
        catch e 
            throw global.Exception.create("Failed processing file: "+ file.file + ", error: " + e.message).putCode(e.code)
        
        
        buf1= Buffer.from(name)
        buf2= Buffer.from(core.safeJSON.stringify(file.stat))
        
        
        
        intBuffer= Buffer.alloc(4)
        intBuffer.writeInt32LE (if ucontent then ucontent.length else file.stat.size), 0
        bufs.push intBuffer
        
        
        intBuffer= Buffer.alloc(4)
        intBuffer.writeInt32LE buf1.length, 0
        bufs.push intBuffer
        bufs.push buf1
        
        
        intBuffer= Buffer.alloc(4)
        intBuffer.writeInt32LE buf2.length, 0
        bufs.push intBuffer
        bufs.push buf2
        
        
        
        if not ucontent
            bufs.push(await Fs.async.readFile(file.file))
        else 
            bufs.push ucontent
        
        task= new core.VW.Task()
        bufs= Buffer.concat(bufs)
        
        if (streamOut.write bufs) is no 
            streamOut.once "drain", task.finish.bind(task)
            await task 
        
        
    streamOut.end()
    task=new core.VW.Task()
    global.context.response.on "finish", task.finish.bind(task)
    await task
    
module.exports= (global)->
    F.global= global 
    F 
    