module.exports= function(global){
	return async function(body){
		'javascript-obfuscator'
		
		var JavaScriptObfuscator = await global.UserContext.require('javascript-obfuscator','0.18.1')
		var obfuscationResult = JavaScriptObfuscator.obfuscate(body.code,
		    {
		    	reservedNames: [
		    		"global",
		    		"F"
		    	],
			    compact: true,
			    controlFlowFlattening: false,
			    deadCodeInjection: false,
			    debugProtection: false,
			    debugProtectionInterval: false,
			    disableConsoleOutput: false,
			    identifierNamesGenerator: 'hexadecimal',
			    //log: false,
			    renameGlobals: false,
			    rotateStringArray: true,
			    selfDefending: false,
			    stringArray: true,
			    stringArrayEncoding: true,
			    stringArrayThreshold: 0.75,
			    unicodeEscapeSequence: false
			}
		);
		return obfuscationResult.getObfuscatedCode()
		
	}
}