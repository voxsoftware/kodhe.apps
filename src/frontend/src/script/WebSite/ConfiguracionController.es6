
var G= core.org.kodhe.WebSite
var U= core.org.kodhe.Util
var $= core.VW.Web.JQuery

class ConfiguracionController extends U.Module{

	async ui(){


		console.info("Module: " + ConfiguracionController.name)
		this.attachEvents()
		$(".await-action").removeAttr("disabled")
		this.hashChanged()

		this.cargarConfiguracion(true)
	}


	hashChanged(){
		var scope= this.app.scope
		this.hashArgs= this.app.getParameters()
	}



	async cargarConfiguracion(printers){
		var scope= this.app.scope

		var config= await this.app.remote.get("config", {})
		if(printers)
			this.cargarImpresoras()
		scope.config= config || {}
	}

	async cargarImpresoras(includeNone){
		var scope= this.app.scope
		scope.cargandoImpresoras= true
		try{

			// Primero getCached ...
			var time= Date.now()
			var printers= await this.app.remote.get("printers", {cache: 3600000})

			// https://msdn.microsoft.com/en-us/library/aa394363(v=vs.85).aspx
			printers= printers.filter((a)=> a.StatusInfo!=4)

			var dif= Date.now()-time

			//var r= scope.selectPrint
			//scope.selectPrint= true
			await core.VW.Task.sleep(1000)
			if(!scope.printers)
				scope.printers=[]
			scope.printers.push({
				Name: '',
				vacio: true,
				selected: !scope.config.printer_name
			})
			for(var i=0;i<printers.length;i++){
				printers[i].selected=scope.config.printer_name==printers[i].Name
				scope.printers.push(printers[i])
			}
			//scope.selectPrint= r
			if(dif<4*1000){
				// Significa que hubo versión cacheada de las impresoras
				// Ahora obtener algo más reciente
				var printers= await this.app.remote.get("printers", {cache: 10})
				printers= printers.filter((a)=> a.StatusInfo!=4)
				scope.printers.removeAll()
				if(includeNone!==false){
					scope.printers.push({
						Name: '',
						vacio: true,
						selected: !scope.config.printer_name
					})
				}
				for(var i=0;i<printers.length;i++){
					printers[i].selected=scope.config.printer_name==printers[i].Name
					scope.printers.push(printers[i])
				}
			}

		}
		catch(e){
			console.error("Error al cargar impresoras:",e)
		}
		scope.cargandoImpresoras=false

	}




	/* /// /// REGIÓN EVENTS /// /// */
	async attachEvents(){
		var scope= this.app.scope, self=this
		scope.app=global.app
		scope.guardarClick= this.event_guardarClick.bind(this)
		scope.abrirImpresoras= this.event_abrirImpresoras.bind(this)
		scope.abrirVars= this.event_abrirVars.bind(this)
		scope.abrirPlantilla= this.event_abrirPlantilla.bind(this)
		scope.abrirPlantilla_2= this.event_abrirPlantilla_2.bind(this)
		scope.habilitarPlantillas= this.event_habilitarPlantillas.bind(this)
		scope.añadirPlantilla= this.event_añadirPlantilla.bind(this)
		scope.añadirVariable= function(){
			return self.event_añadirVariable(this)
		}
		scope.cancelarPlantilla= function(){
			return self.event_cancelarPlantilla(this)
		}
		scope.guardarPlantilla= function(){
			return self.event_guardarPlantilla(this)
		}
		scope.editarPlantilla= function(){
			return self.event_editarPlantilla(this)
		}
		scope.borrarPlantilla= function(){
			return self.event_borrarPlantilla(this)
		}
		scope.verContenido= function(){
			return self.event_verContenido(this)
		}

		scope.printerClick= function(){
			return self.event_printerClick($(this))
		}
		scope.observer.onValueChanged("config.printer_name",()=>{
			var printers= scope.printers,print, sel
			for(var i=0;i<printers.length;i++){
				print= printers[i]
				sel= print.Name==scope.config.printer_name
				if(sel!=print.selected){
					print.selected=sel
					scope.printers[i]= print
				}
			}
		})
		await core.VW.Task.sleep(200)
		$("#folder").on("change", this.event_folderChange.bind(this))
		scope.selectFolder= this.event_selectFolder.bind(this)
	}
	
	
	event_abrirVars(){
		var scope= this.app.scope
		scope.showVars=!scope.showVars
	}
	
	getInputSelection(el) {
	    var start = 0, end = 0, normalizedValue, range,
	        textInputRange, len, endRange;
	
	    if (typeof el.selectionStart == "number" && typeof el.selectionEnd == "number") {
	        start = el.selectionStart;
	        end = el.selectionEnd;
	    } else {
	        range = document.selection.createRange();
	
	        if (range && range.parentElement() == el) {
	            len = el.value.length;
	            normalizedValue = el.value.replace(/\r\n/g, "\n");
	
	            // Create a working TextRange that lives only in the input
	            textInputRange = el.createTextRange();
	            textInputRange.moveToBookmark(range.getBookmark());
	
	            // Check if the start and end of the selection are at the very end
	            // of the input, since moveStart/moveEnd doesn't return what we want
	            // in those cases
	            endRange = el.createTextRange();
	            endRange.collapse(false);
	
	            if (textInputRange.compareEndPoints("StartToEnd", endRange) > -1) {
	                start = end = len;
	            } else {
	                start = -textInputRange.moveStart("character", -len);
	                start += normalizedValue.slice(0, start).split("\n").length - 1;
	
	                if (textInputRange.compareEndPoints("EndToEnd", endRange) > -1) {
	                    end = len;
	                } else {
	                    end = -textInputRange.moveEnd("character", -len);
	                    end += normalizedValue.slice(0, end).split("\n").length - 1;
	                }
	            }
	        }
	    }
	
	    return {
	        start: start,
	        end: end
	    };
	}
	
	async event_añadirVariable(o){
		var a=$(o)
		var text=a.parents(".input-field").eq(0).find(".html_template")
		var pscope= a.voxscope()
		var selection= this.getInputSelection(text[0])
		var str, newstart, newend
		if(selection && selection.start< pscope.plantilla.html.length){
			newstart= selection.start+10
			str= pscope.plantilla.html.substring(0, selection.start) + "${context.VARIABLE}" + pscope.plantilla.html.substring(selection.end)
			newend= newstart+8
		}
		else{
			newstart= pscope.plantilla.html.length
			str= (pscope.plantilla.html||"") + "${context.VARIABLE}" 
			newstart+= 10
			newend= newstart+8
		}
		pscope.plantilla.html= str
		await core.VW.Task.sleep(100)
		text.focus()
		text[0].setSelectionRange(newstart, newend)
		
	}

	event_guardarPlantilla(a){
		var jq=$(a)
		var pscope= jq.voxscope()
		var scope= this.app.scope
		if(!pscope.plantilla.name || !pscope.plantilla.html)
			return this.app.messageManager.error("Por favor llene la información: Nombre y Contenido")

		if(pscope.plantilla.name.length<4)
			return this.app.messageManager.error("Nombre muy corto para la plantilla")

		var igual= scope.templates.filter((a)=>{
			return a!=pscope.plantilla &&  a.name== pscope.plantilla.name
		})
		if(igual && igual.length)
			return this.app.messageManager.error("Ya hay una plantilla con el mismo nombre")

		pscope.plantilla.editando= false
		pscope.plantilla.new= false
	}

	async event_editarPlantilla(a){
		var jq=$(a)
		var pscope= jq.voxscope()
		pscope.plantilla.editando= true
		await core.VW.Task.sleep(100)
		$(".plantillas .collection-item").eq(pscope.$index).find("input:eq(0)").focus()
	}

	event_borrarPlantilla(a){
		var jq=$(a)
		var pscope= jq.voxscope()
		var scope= this.app.scope
		scope.templates.removeValue(pscope.plantilla)
	}
	event_verContenido(a){
		var jq=$(a)
		var pscope= jq.voxscope()
		pscope.plantilla.ver=!pscope.plantilla.ver
	}
	event_cancelarPlantilla(a){
		var jq=$(a)
		var pscope= jq.voxscope()
		var scope= this.app.scope
		pscope.plantilla.editando= false
		if(pscope.plantilla.new)
			scope.templates.removeValue(pscope.plantilla)

		scope.plantilla_l= scope.templates.length
		pscope.new= false
	}

	async event_añadirPlantilla(){
		var scope= this.app.scope
		
		scope.templates.push({
			editando: true,
			new: true,
			type:1 
		})
		scope.plantilla_l=1
		var p=$(".plantillas .collection-item")
		await core.VW.Task.sleep(100)
		p.eq(p.length-1).find("input:eq(0)").focus()
	}
	
	async event_habilitarPlantillas(){
		var scope= this.app.scope
		
		scope.plantilla_s=scope.plantilla_s==1?0:1
		await core.VW.Task.sleep(200)
		scope.templates=  []
		if(scope.config.templates){
			for(var i=0;i<scope.config.templates.length;i++){
				scope.templates.push(scope.config.templates[i])
			}
		}
		scope.config.templates= scope.templates
		scope.plantilla_l= scope.templates.length
	}

	event_folderChange(){
		var scope= this.app.scope
		var e= $("#folder")[0]
		var f= e.files[0]
		var path= (f? f.path: "") || ""
		/*scope.observer.assignValue({
			name: "config.folder",
			value:path
		})*/
		scope.config.folder= path
	}
	event_selectFolder(){
		$("#folder").click()
	}
	event_abrirImpresoras(){
		var scope= this.app.scope
		scope.selectPrint= !scope.selectPrint
	}

	event_abrirPlantilla(){
		var scope= this.app.scope
		scope.plantillaVisible= !scope.plantillaVisible
	}

	event_abrirPlantilla_2(){
		var scope= this.app.scope
		scope.plantillaVisible_2= !scope.plantillaVisible_2
	}

	event_printerClick(a){
		var scope= this.app.scope
		var privateScope= a.voxscope()
		var printer= privateScope.printer
		scope.observer.assignValue({
			name: "config.printer_name",
			value: printer.Name
		})
		scope.selectPrint= false
	}

	async event_guardarClick(){
		var scope= this.app.scope
		try{
			var editando= scope.templates?scope.templates.filter(function(a){return a.editando}) : null
			if(editando && editando.length)
				return this.app.messageManager.info("Hay una plantilla de correo en edición. Debe guardar o cancelar la edición de la plantilla antes de guardar")
			
			if(scope.config.templates){
				scope.config.templates.forEach((a)=> {
					delete a.editando
					delete a.new
				})
			}
			var response= await this.app.remote.put("config",scope.config)
			if(response.message){
				this.app.messageManager[response.state!="on"?"error":"info"](response.message.text || response.message.toString())
				if(response.state!="on"){
					/*
					if($("installation").length)
						return 
					*/
				}
			}
			
			location= "#/wall" //$("installmode").text()==1?"#/shortcut":"#/wall"
		}catch(e){
			this.app.messageManager.error(e.message || e)
		}
	}

	/* /// /// REGIÓN EVENTS /// /// */


}
export default ConfiguracionController
