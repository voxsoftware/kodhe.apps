

var U= core.org.kodhe.Util
var G= core.org.kodhe.WebSite
var $= core.VW.Web.JQuery

class SideNav extends U.Module{


	get uiSideNavDom(){
		return $(".side-nav")
	}

	get uiSideNav(){
		return this.uiSideNavDom.voxsidenav()[0]
	}


	/* /// /// REGIÓN EVENTS /// /// */
	async attachEvents(){
		var scope= this.app.scope, self= this
		/*scope.params={
			"boton-atras": global.applicationConfig["boton.atras"]
		}*/
		scope["menu-click"]= ()=>{
			this.uiSideNav.open()
		}
		scope["menu-back"]= ()=>{
			window.history.back()
		}
		scope["cerrarSideNav"]= ()=>{
			this.uiSideNav.close()
		}
		scope["salir-click"]= this.eventSalir.bind(this)
		//scope["userCard"]= this.event_userCard.bind(this)


		scope.locationHost=location.hostname
		var g= async ()=>{

			if(this.ajusting)
				return


			this.ajusting=true
			await core.VW.Task.sleep(100)
			this.ajusting=false

			var y=$(".fb_pc")
			var h= y.find(".fb-page-1")

			var h2=h.clone()
			h2.show()
			h2.addClass("fb-page")
			$(".fb-page").remove()
			h2.insertAfter(y)
			//
		}

		g()


		if(!window.y1){
			var k=function(e){
				var href= $(this).attr("href")
				if(href.startsWith("#")){
					href= href.substring(1)
				}
				history.pushState({},"",href)
				if(e&&e.preventDefault)
					e.preventDefault()

				return false
			}
			$(document).click(this.event_documentClick.bind(this))

			//$(window).resize(g)


			core.VW.Web.Vox.mutation.watchAppend($("body"), (ev)=>{
				ev.jTarget.click(k)
			},"[push-state]")
			$("[push-state]").click(k)
			window.y1=true
		}
		while(true){
			await core.VW.Task.sleep(1000)
			if(global.FB){
				FB.AppEvents.logEvent("visit")
				FB.AppEvents.logEvent(
					"visit",
					1,
					{
						url: location.href
					}
				)
				break
			}
		}
		//FB.XFBML.parse()
	}

	async event_documentClick(ev){

		if(this.hiding)
			return



		var target= $(ev.target)
		var card= $(".user-info.card")
		if(card.get(0)==target.get(0))
			return
		if(card.find(ev.target).length >0)
			return


		var card2= $(".user-clic")
		if(card2.get(0)==target.get(0))
			return
		if(card2.find(ev.target).length >0)
			return

		this.hiding= true
		if(card.is(":visible"))
			card.voxanimate("zoomOutRight", 500)




		await core.VW.Task.sleep(500)
		this.hiding= false
	}


	async eventSalir(){
		var messageManager= this.app.messageManager
		try{
			this.uiSideNav.close()
			// Ocurre cuando dan click en salir ...
			await this.app.getModule("login").exit()
			this.app.redirect("login")
		}
		catch(e){
			messageManager.error("Hubo un problema al cerrar la sesión. " + e.message)
		}
	}
	/* /// /// REGIÓN EVENTS /// /// */

}

export default SideNav
