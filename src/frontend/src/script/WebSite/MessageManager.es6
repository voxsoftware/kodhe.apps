var G= core.org.kodhe.WebSite
var U= core.org.kodhe.Util
var $= core.VW.Web.JQuery

class MessageManager extends U.Module{

	info(){
		return G.Messages.info.apply(G.Messages, arguments)
	}
	warning(){
		return G.Messages.warning.apply(G.Messages, arguments)
	}
	error(){
		var v= G.Messages.error.apply(G.Messages, arguments)

		/*setTimeout(()=>{
			this.app.audio.uiAudioError.play()
		},200)
		*/

		return v
	}

	confirmClose(){
		var m= $(".modal-confirm")
		m.voxmodal()[0].close()
	}


	getDefaults(){
		var idioma= this.app.configuration.idiomaActual
		return {
			"confirmText": "Confirmar",
			"cancelText": "Cancelar"
		}
	}

	confirm(){
		arguments[0]= arguments[0]||{}
		arguments[0].defaults= this.getDefaults()
		return G.Messages.confirm.apply(G.Messages, arguments)
	}

}

export default MessageManager
