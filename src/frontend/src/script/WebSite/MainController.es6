/*global core*/
var G= core.org.kodhe.WebSite
var U= core.org.kodhe.Util
var $= core.VW.Web.JQuery

class MainController extends U.Module{

	async ui(){
		var scope= this.app.scope
		scope.arguments= window.app
		console.info("Module: " + MainController.name)
		this.attachEvents()
		$(".await-action").removeAttr("disabled")
		this.hashChanged()
	}


	hashChanged(){
		var scope= this.app.scope
		scope.prefix=global.prefix
		this.hashArgs= this.app.getParameters()
		this.getFolder()
	}
	
	
	async getFolder(){
		var scope= this.app.scope 
		
		var data= await this.app.remote.get("function/c/dir.info", {})
		scope.folder= data.appFolder
		
		var apps= await this.app.remote.get("function/c/app.list", {})
		for(var i=0;i<apps.length;i++){
			if(!apps[i].iconclass){
				apps[i].iconclass= 'fa text-yellow darken-1 fa-rocket'
			}
		}
		scope.apps= scope.apps||[]
		if(scope.apps &&scope.apps.length>1){
			scope.apps.splice(0,scope.apps.length)
		}
		for(var i=0;i<apps.length;i++){
			scope.apps.push(apps[i])
		}
		
		scope.apps_length= apps.length+1
		setTimeout(this.getFolder.bind(this), 12000)
	}


	/* /// /// REGIÓN EVENTS /// /// */
	attachEvents(){
		var scope= this.app.scope, self=this
		scope.opcion=0 
		scope.opcion0= function(){
			scope.opcion=0 
		}
		scope.ejecutarApp= function(ev){
			if(ev && ev.preventDefault)
				ev.preventDefault()
			self.ejecutarApp($(this))
		}
		
	}
	
	
	async ejecutarApp(a ){
		var pscope= a.voxscope()
		var app= pscope.app 
		try{
			pscope.app.waiting= true
			await this.app.remote.post("function/c/app.check", app)
			//global.location= app.link 
			
			global._require("electron").remote.getCurrentWindow().minimize()
			
		}catch(e){
			this.app.messageManager.error(e.message)
		}
		finally{
			pscope.app.waiting= false
		}
	}
	/* /// /// REGIÓN EVENTS /// /// */


}
export default MainController
