

var exe= (code)=>{
	eval(code)
}


var G= core.org.kodhe.WebSite
var U= core.org.kodhe.Util
var $= global.$





class RouteManager extends U.Module{


	async view(view){
		if(!view.endsWith(".html"))
			view+= ".html"

		var req= new core.VW.Http.Request(view)
		req.async= true
		var res= await req.getResponseAsync()



	//	this.enableLoading()
		await this.processHtml(res.body)

	}

	enableLoading(){
		$(".loading-container,.loading-cover").show()
		var progress= $(".loading-progress")
		if(!this.loading){
			progress.css("transition-duration", "6s")
			progress.css("width", "80%")

			this.loading= true

			this.task= new core.VW.Task()
			this.task.oncomplete= function(){

				progress.css("transition-duration", "0.7s")
				progress.css("width", "100%")
				setTimeout(function(){
					$(".loading-container,.loading-cover").hide()
				}, 800)

			}
		}
	}


	deleteAttributes(element){
		var attributes= element.get(0).attributes
		for(var i=0;i<attributes.length;i++){
			element.removeAttr(attributes[i].name)
		}
	}


	copyAttributes(element, element2){
		var attributes= element.get(0).attributes
		for(var i=0;i<attributes.length;i++){
			element2.attr(attributes[i].name, attributes[i].value)
		}
	}

	replaceAttributes(element, element2, ignore=[]){
		var attributes= element.get(0).attributes, attributes2=element2.get(0).attributes,
			e1byname={},  n
		for(var i=0;i<attributes.length;i++){
			e1byname[attributes[i].name]=attributes[i].value
		}
		var e1keys= Object.keys(e1byname)
		for(var i=0;i<attributes2.length;i++){
			n= attributes2[i].name
			v= attributes2[i].value
			if(e1keys.indexOf(n)<0){
				element2.removeAttr(n)
			}
			else if(e1byname[n]!= v && ignore.indexOf(n)<0){
				element2.attr(n, e1byname[n])
			}
		}


	}




	async processHtml(html){

		var contenido= html.replace(/(\<html\s+|\<\/html\>|\<head\s+|\<\/head\>|\<body\s+|\<\/body\>)/ig, function(v){
			v= v.toLowerCase()
			//console.info(v)
			if(v.indexOf("html")>=0)
				return v.replace("html", "vhtml")

			if(v.indexOf("head")>=0)
				return v.replace("head", "vhead")

			if(v.indexOf("body")>=0)
				return v.replace("body", "vbody")
		})

		$("iframe").attr("src","null")
		$("iframe").remove()

		//console.info(contenido)
		var jq= $(contenido)
		var body= jq.find("vbody>*")
		var main0= jq.find(".main-0")
		var main02= $(".main-0")
		var main1= jq.find(".main-1")
		var main12= $(".main-1")
		window.pe= body
		this.app.reset()
		var toAnimate
		if(main1.length+main12.length>=2){
			main12.voxanimate("fadeOut", 300)
			await core.VW.Task.sleep(300)
			main12.html(main1.html())

			//main12.voxanimate("fadeIn", 600)
			toAnimate= main12
		}
		else if(main0.length+main02.length>=2){
			main02.voxanimate("fadeOut", 300)
			await core.VW.Task.sleep(300)
			main02.html(main0.html())

			//main02.voxanimate("fadeIn", 600)
			toAnimate= main02
		}
		else{
			var toremove= $("body>*").not(".inmovible")
			toremove.remove()
			for(var i=0;i<body.length;i++){
				$("body").append(body.eq(i))
			}
		}


		//this.deleteAttributes($("body"))
		//this.copyAttributes(jq.find("vbody"), $("body"))

		//this.replaceAttributes(jq.find("vbody"), $("body"),["class"])

		var html1= jq.find("vhead").html()
		if(true){


			var meta= jq.find("vhead meta,vhead title")
			$("head meta,head title").remove()
			for(var i=0;i<meta.length;i++){
				$("head").append(meta)
			}

			//this.deleteAttributes($("html"))
			//this.copyAttributes(jq, $("html"))

			this.replaceAttributes(jq, $("html"))


			/*
			if(document.head.innerHTML!=html1)
				document.head.innerHTML=html1
			*/

			var scripts= jq.find("vhead script")
			scripts.each(function(){
				var s= $(this)
				var code= s.html()
				if(code)
					exe(code)
				code=null
				s=null
			})
		}

		if(toAnimate){
			await core.VW.Task.sleep(500)
			toAnimate.voxanimate("fadeIn", 300)
		}
		if(this.task)
			this.task.finish()

		html=null
		html1=null
		jq=null
	}



	init(pushstate){
		this.router= new core.dynvox.Router()
		this.router.pushstate=!!pushstate
		this.pushstate=!!pushstate

		var r
		for(var id in G.Routes){
			r= G.Routes[id]
			console.info(id, r)
			if(r&& r.routes){
				console.info("aqui", id)
				r.routes(this.router, this.app)
			}
		}


		this.router.use(()=>{
			return this.view("404")
		})
		return this.router.start()
	}


	getParameters(){
		var hashStr= this.router.location.hash
		if(hashStr)
			hashStr= hashStr.substring(1)

		var hash={}
		if(hashStr){
			var p,parts= hashStr.split("&"), part
			for(var i=0;i<parts.length;i++){
				part= parts[i]
				p=part.split("=")
				hash[p[0]]= p[1]
			}
		}

		var params= this.router.params
		var o= {}
		for(var id in params){
			o[id]= params[id]
		}
		for(var id in hash){
			if(!o[id])
				o[id]= hash[id]
		}

		return o
	}



	redirect(route, parameters){



		var url= (this.pushstate?"/":"#/") + route
		if(parameters){
			str=[]
			for(var id in parameters){
				str.push(id + "=" + parameters[id])
			}
			url+= "#" + str.join("&")
		}

		var base= this.app.uiUrl
		//location= base+url
		return this.router.start(undefined, base+url)
	}

}
export default RouteManager
