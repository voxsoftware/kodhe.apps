
var G= core.org.kodhe.WebSite
var L= core.org.kodhe.Locale
var U= core.org.kodhe.Util
var $= core.VW.Web.JQuery

class Configuracion extends U.Module{


  constructor(){
    U.Module.apply(this, arguments)
    this.idiomaActual= {
      formatException: (e)=>{
        return e.message||e.toString()
      }
    }
  }



	async ui(){


		console.info("Module: " + Configuracion.name)
		this.attachEvents()
		await this.app.getModule("boardcontroller").commonUi({
			"noredirect": true
		})
		await this.cargarIdiomas()


	}

	
	async apply(){

		var idioma=await this.codigoIdiomaActual()
		this.cambiarIdioma(idioma)

	}


	async cargarIdiomas(){

		var scope= this.app.scope
		var idiomas= L.Language.supported()
		var idiomaactual= await this.codigoIdiomaActual()
		for(var i=0;i<idiomas.length;i++){
			idioma= idiomas[i]
			if(idioma.code== idiomaactual)
				idioma.actual= true
		}

		scope.idiomas= idiomas
	}


	async codigoIdiomaActual(){
		if(this.$code)
			return this.$code
      /*
		var info= await this.app.remote.get("session/lang")
		if(!info.value){
			// Tratar de obtener de la configuración de usuarioActual
			info= await this.app.remote.get("preferencia/idioma")
		}
		return info.value || "Es"
    */
    return localStorage && localStorage.lang!="null" && localStorage.lang!="undefined"?localStorage.lang:"En"
	}

	get idiomaActual(){
		return this.$idioma
	}




	/// /// REGIÓN EVENTS /// ///
	attachEvents(){

		var scope= this.app.scope, self= this

		scope.cambiarIdioma= function(){
			var a= $(this)
			var lang= a.voxscope().idioma
			self.event_idioma(lang.code)
		}
	}

	async event_idioma(code){
		this.cambiarIdioma(code)
		var scope= this.app.scope
		await this.app.remote.put("session/lang",{
			value: code
		})

		await this.app.remote.put("preferencia/idioma",{
			value: code
		})

		var idiomas= scope.idiomas,idiomae
		for(var i=0;i<idiomas.length;i++){
			idiomae=idiomas[i]
			idiomae.actual= idiomae.code==code
			scope.idiomas[i]= idiomae
		}

	}

	cambiarIdioma(code){
		var scope= this.app.scope
    code= code[0].toUpperCase() + code.substring(1)
		var idioma= new L[code].Language()
		this.$code= code
		this.$idioma= idioma
		//scope.idiomaObject= idioma
		scope.idioma= idioma


	}
	/* /// /// REGIÓN EVENTS /// /// */


}
export default Configuracion
