
class Message{
	constructor(options){
		this.type= options.type
		this.msg= options.message
		this.options= options
	}


	show(){
		var type= this.type
		if(type=="confirm"){
			return Message.confirm(this.options)
		}
		else if(type=="info"||type=="msg"){
			return Message.info(this.msg)
		}
		else if(type=="warning"){
			return Message.warning(this.msg)
		}
		else if(type=="error"){
			return Message.error(this.msg)
		}

		throw new core.System.Exception("El tipo no es válido")
	}

	static createToast(jtoast, msg){
		var nj= jtoast.clone()
        nj.text(msg)
        nj.removeClass("template")
        var toast= nj.voxtoast()[0]
        toast.on("close", function(){
            nj.remove()
        })
        toast.open()
	}

	static info(msg){
		var jtoast= $(".info.template")
        console.info("Platform: ", msg)
        return Message.createToast(jtoast, msg)
	}

	static warning(msg){
		var jtoast= $(".warning.template")
        console.warn("Platform: ", msg)
        return Message.createToast(jtoast, msg)
	}

	static error(msg){
		var jtoast= $(".error.template")
        console.error("Platform: ", msg)
        return Message.createToast(jtoast, msg)
	}



	static confirm(options){

		var m= $(".modal-confirm")
		var scope= core.dynvox.Scope.get("default")
		var modalScope= core.dynvox.Scope.get("modal")
		options.text= options.text|| options.msg

		if(!options.confirmText)
			options.confirmText= options.defaults.confirmText
		if(!options.cancelText)
			options.cancelText=  options.defaults.cancelText

		if(!options.cancel){
			options.cancel= ()=>{
				m.voxmodal()[0].close()
			}
		}
		modalScope.modalConfirm=options
		m.voxmodal()[0].open()

		/*
        if(title){
            m.find(".title").text(title)
        }
        m.find("p").text(msg)
        sit= "Sí"
        nit= "No"
        if (typeof fsi=="object"){
            sit= fsi.text || sit
            fsi= fsi.func
        }
        if (typeof fno=="object"){
            nit= fno.text || nit
            fno= fno.func
        }

        m.find(".si>span").text(sit)
        m.find(".no>span").text(nit)

        this._fsi= fsi
        this._fno= fno
        m.voxmodal()[0].open()
        */
	}

}

export default Message
