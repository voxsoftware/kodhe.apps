var $= core.VW.Web.JQuery
var G= core.org.kodhe.WebSite
var U= core.org.kodhe.Util
class Application{

	static get current(){
		if(!Application.$current)
			Application.$current= new Application()
		return Application.$current
	}

	get configuration(){
		return this.getModule("configuracion")
	}


	// Los parámetros se mandan como hash ...
	getParameters(){
		return this.getModule("routemanager").getParameters()
		/*var hash= location.hash.substring(1)
		var obj={}
		var p,parts= hash.split("&"), part
		for(var i=0;i<parts.length;i++){
			part= parts[i]
			p=part.split("=")
			obj[p[0]]= p[1]
		}
		return obj
		*/
	}


	hashChanged(f){
		if(!this.$changed){
			window.onhashchange= ()=>{
				var args= arguments
				this.$changed.forEach((f)=>{
					f.apply(this,args)
				})
			}
			this.$changed= []
		}
		this.$changed.push(f)

	}


	getModule(module){

		var name= module.toUpperCase()
		if(!this.$cacheNames){
			this.$cacheNames={}
			for(var id in G){
				this.$cacheNames[id.toUpperCase()]= G[id]
			}
		}

		if(this.$cacheNames[name])
			return this.create(this.$cacheNames[name], name)
	}

	get scope(){
		var scope= $("body").attr("voxs-scope")
		var sc= core.dynvox.v2.Scope.get(scope)
		// BACKWARD COMPATIBILITY 
		// Newer version of dynvox don't expose a observer prop in scope
		if(!sc.observer){
			sc.observer= new G.Observer(sc)
		}
		return sc
	}


	get currentModule(){
		return $("module").text()
	}




	get messageManager(){
		if(!this.$message)
			this.$message= this.create(G.MessageManager)

		return this.$message
	}

	get apiUrl(){
		return global.app.apiUrl
	}

	get uiUrl(){
		return global.app.uiUrl
	}

	get assetUrl(){
		return global.app.assetUrl
	}

	get routes(){
		return window.app.routes
	}


	beginRequest(req){

		// Si el request está marcado como nowait, no deshabilita los botones
		if(!req.nowait)
			$(".await-action").attr("disabled", "disabled")

		var progress= $(".loading-progress")
		progress.removeClass("transitioned")
		progress.css("width", 0)
		$(".loading-container").show()
		progress.addClass("transitioned")
		progress.css("transition-duration", "6s")
		progress.css("width", "80%")

	}

	endRequest(){
		var progress= $(".loading-progress")
		progress.css("transition-duration", "0.7s")
		progress.css("width", "100%")
		setTimeout(function(){
			$(".loading-container").hide()
		}, 800)

		$(".await-action").removeAttr("disabled")
	}

	get remote(){
		if(!this.$remote){
			this.$remote= new U.Remote(this)
			this.$remote.on("request", this.beginRequest.bind(this))
			this.$remote.on("request:complete", this.endRequest.bind(this))
		}
		return this.$remote
	}


	get messageManager(){
		if(!this.$message)
			this.$message= this.create(G.MessageManager)

		return this.$message
	}


	reset(){

		this.$changed= []
		if(this.currentModuleO)
			this.currentModuleO.abort()

		/*var idioma= this.scope.getObservable("idioma")
		var usuarioActual= this.scope.getObservable("usuarioActual")
		*/

		for(var id in this.scope){
			if(id!="usuarioActual" && id!="idioma")
				delete this.scope[id]
		}
		// Esto es para borrar la memoria de Datos de cada página ...
		//this.scope.reset()

		// No borrar estos observables para el correcto funcionamiento del board ...
		/*if(usuarioActual)
			this.scope.add(usuarioActual)

		if(idioma)
			this.scope.add(idioma)
		*/
	}


	isForm(form){
		return typeof form.serialize == "function"
	}

	formArgs(form){
		var data= form.serializeArray()
		var obj={}
		for(var i=0;i<data.length;i++){
			obj[data[i].name]= data[i].value
		}
		return obj
	}

	getAssetUrl(url){
		return this.assetUrl + url
	}


	redirect(route,parameters){

		return this.getModule("routemanager").redirect(route, parameters)

		/*
		var url= this.routes[route], str
		var base= this.uiUrl
		if(!url)
			url= route

		url= "?"+ url
		if(parameters){
			str=[]
			for(var id in parameters){
				str.push(id + "=" + parameters[id])
			}
			url+= "#" + str.join("&")
		}

		location= base +  url
		*/
	}


	async cargarConfigSiesNecesario(){

		/*
		var date= Date.now()
		var config= global.applicationConfig

		try{
			if(!config && localStorage)
				config= JSON.parse(localStorage.applicationConfig)
		}
		catch(e){
			console.error("Error local storage: ", e)
		}
		if(!config || (date - config.__date>=20000)){
			config= await this.remote.get("config/json",{})
			config.__date= Date.now()
			console.info(config)
		}
		global.applicationConfig= config

		try{
			if(localStorage)
				localStorage.applicationConfig= JSON.stringify(config)
		}
		catch(e){
			console.error("Error local storage: ", e)
		}*/

		var config= {}
		if(localStorage){
			config.lang= localStorage.lang
		}

		if(!config.lang){
			config= {lang:'es'} //await this.remote.get("config/lang",{})
			localStorage.lang= config.lang
		}
		global.applicationConfig= config


	}

	async start(){
		var module= this.currentModule
		console.info("Módulo actual: ", module)
		//var scope= this.scope
		this.scope.observer.onValueChanged("app.title", function(ev){
			if(!ev.value)
				return
			$("title").text(ev.value)
		})
		// Poner deshabilitados
		$(".await-action").attr("disabled", "disabled")

		await this.cargarConfigSiesNecesario()
		//global.applicationConfig= config

		this.getModule("configuracion").apply()
		if(module){
			module= this.getModule(module)
			this.currentModuleO= module
			if(module){
				if(module.ui)
					module.ui()
			}
		}

		$("form a.button[type=submit]").click(function(){
			var a= $(this)
			a.parents("form").eq(0).submit()
		})



	}

	uiRequired(){
		var inputs= $(".input-field")
		var self= this
		inputs.each(function(){
			var span,inp= $(this), t
			t= inp.find("input,select").eq(0)
			if(t.data("required")!==undefined){
				span= $("<span>")
				span.text(" * ")
				span.addClass("text-color-warn")
				inp.find("label").append(span)
			}
		})
	}

	create(clase, name){
		if(!this.cached)
			this.cached={}

		if(!name)
			name= clase.name
		if(!this.cached[name])
			this.cached[name]= new clase(this)
		return this.cached[name]
	}

	deleteFromCache(module){

		if(this.cached){
			for(var id in this.cached){
				if(this.cached[id]==module)
					delete this.cached[id]
			}
		}
	}



	clearCache(){
		for(var id in this.cached){
			if(id != "routemanager")
				delete this.cached[id]
		}


		for(var id in this.$cacheNames){
			if(id != "routemanager")
				delete this.$cacheNames[id]
		}

	}

}
export default Application
