
var G= core.org.kodhe.WebSite
var U= core.org.kodhe.Util
var $= core.VW.Web.JQuery

class BoardController extends U.Module{


	async ui(){
		// console.info("Module: " + BoardController.name)
		await this.commonUi()

		// Cargar datos KPI ....
		this.cargarDatosKPI()

	}


	async cargarDatosKPI(){


	}


	async _awaitPromise(promise){
		return await promise
	}




	// Esto es como para no repetir varias veces el mismo request
	// Si ya está realizando la operación (Promise)
	// lo que hace es crear otra promesa que espera a la
	// última promesa creada
	commonUi(options){

		if(this.loading)
			return this.lastPromise= this._awaitPromise(this.lastPromise)

		this.lastPromise= this.__commonUi(options)
		return this.lastPromise
	}


	async __commonUi({noredirect}){




		this.loading= true

		try{
			var userScope= core.dynvox.Scope.get("usuario")
			if(!userScope.getObservable("usuarioActual"))
				userScope.createVariable("usuarioActual", null)

			if(userScope.usuarioActual)
				this.app.scope.usuarioActual= userScope.usuarioActual

			var usuario
			this.attachEvents()
			try{
				usuario=await this.app.getModule("account").usuarioActual()
				userScope.usuarioActual= usuario
				if(usuario){
					/*
					await this.app.getModule("permiso").obtenerPermisos({
						"permisos": this.permisosABuscar
					})*/
				}
				//this.app.getModule("permiso").verificarPermisos()
			}
			catch(e){
				console.error(e)
				this.loading= false
				// Redirigir al login ...
				return
			}

			this.loading= false
			console.info(noredirect)
			if(!usuario && !noredirect)
				return this.app.redirect("login")

			//this.clearLoading()
			//this.app.getModule("publicidad").intentarCargar()
			$(".await-action").removeAttr("disabled")

		}
		catch(e){
			console.error(e)
			this.loading= false
		}
	}





	get uiPreLoadingImg(){
		return $(".pre-loading img")
	}

	get uiPreLoading(){
		return $(".pre-loading")
	}

	/* /// /// REGIÓN EVENTS /// /// */

	attachLoading(){
		var active= true
		/*this.$loading= setInterval(()=>{

			if(active)
				this.uiPreLoadingImg.hide(1000)
			else
					this.uiPreLoadingImg.show(1000)
			active=!active
		}, 1000)
		*/
	}

	clearLoading(){
		if(this.$loading)
			clearInterval(this.$loading)

		this.uiPreLoading.voxanimate('fadeOut')
	}
	attachEvents(){
		//var scope= this.app.scope
		this.app.getModule("sidenav").attachEvents()
		this.attachLoading()
	}


	/* /// /// REGIÓN EVENTS /// /// */


}
export default BoardController
