
class Observer{
	constructor(scope){
		this.scope=scope	
	}
	
	getValue(str){
		console.warn("> Deprecated, please consider not using Observer with newer version of scope")
		var props= str.split(".")
		var o= this.scope
		for(var i=0;i<props.length;i++){
			o=o[props[i]]
			if(!o)
				break 
		}
		return o
	}
	
	
	onValueChanged(name, event){
		console.warn("> Deprecated, please consider not using Observer for attach events with newer version of scope")
		console.warn("> Deprecated, Newer versions of dynvox take care about memory leaks, this method no")
		//console.warn("> For prevent memory leaks, your event will be attached to ")
		this.scope.on("change", function(ev){
			if(ev.name==name)
				return event(ev)
		})
	}
	
	assignValue(args){
		
		console.warn("> Deprecated, please consider not using Observer for attach events with newer version of scope")
		var props= args.name.split(".")
		var o= this.scope
		for(var i=0;i<props.length-1;i++){
			o=o[props[i]]
			if(!o)
				break 
		}
		if(o){
			o[props[props.length-1]]= args.value
		}
		return args.value
	}
	
}
export default Observer