
export function routes(router, app){

	var routeManager= app.getModule("routemanager")
	router.route("", (args)=>{
		return app.redirect("wall")
	})
	router.route("/", (args)=>{
		return app.redirect("wall")
	})
	router.route("/wall", function(args){
		return routeManager.view("/dynamic/"+window.IDIndex+"/main")
	})
}
