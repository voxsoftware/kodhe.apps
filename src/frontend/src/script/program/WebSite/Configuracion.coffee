#global $


G= require(__dirname + "/../default").WebSite
U= require(__dirname + "/../default").Util


class Configuracion extends U.Module 


	constructor: ()->
		super ...arguments

		@idiomaActual = 
			formatException: (e) -> 
				e.message || e.toString()
		
	

	ui: ()->
		@attachEvents()
		await @app.getModule("boardcontroller").commonUi
			"noredirect": true
		
		await @cargarIdiomas()




	apply: ->

		idioma = await @codigoIdiomaActual()
		@cambiarIdioma(idioma)



	cargarIdiomas: ()->

		scope = @app.scope
		idiomas = L.Language.supported()
		idiomaactual = await this.codigoIdiomaActual()
		
		for idioma in idiomas 
			idioma = idiomas[i]
			if idioma.code is idiomaactual
				idioma.actual = yes 
		

		scope.idiomas = idiomas
	

	codigoIdiomaActual: ()->
		return @code if @$code
		return if ( localStorage and localStorage.lang != "null" and localStorage.lang != "undefined" ) then localStorage.lang else "En"
	

	__get_idiomaActual: ->
		@$idioma
	

	attachEvents: ()->
		scope= @app.scope 
		self= @ 
		scope.cambiarIdioma= ()->
			a= $ @
			lang= a.voxscope().idioma 
			self.event_idioma lang.code 
			

	event_idioma: (code)->
		@cambiarIdioma(code)
		scope = @app.scope
		
		await this.app.remote.put "session/lang", 
			value: code
		

		await this.app.remote.put "preferencia/idioma",
			value: code
		
		idiomas = scope.idiomas
		
		for idiomae in idiomas
			idiomae.actual = idiomae.code is code
			scope.idiomas[i] = idiomae
		


	cambiarIdioma: (code)->
		scope = @app.scope
		code = code[0].toUpperCase() + code.substring(1)
		lang = {}

		idioma = new L.Language()
		idioma.$lngInfo = lang.lngInfo
		idioma.name = lang.name
		idioma.code = lang.code
		idioma.load()
		
		@$code = lang.code
		@$idioma = idioma
		scope.idioma = idioma
	
	global.resolveProps @, @::

export default Configuracion
