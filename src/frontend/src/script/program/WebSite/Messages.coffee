
class Message
	constructor: (options)->
		@type= options.type
		@msg= options.message
		@options= options



	show: ->
		type= @type
		if type=="confirm"
			return Message.confirm(@options)
		else if type=="info" or type=="msg"
			return Message.info(@msg)
		else if type=="warning"
			return Message.warning(@msg)
		else if type=="error" 
			return Message.error(@msg)
		throw new core.System.Exception("El tipo no es válido")
	

	@createToast: (jtoast, msg)->
		nj= jtoast.clone()
		nj.text(msg)
		nj.removeClass("template")
		toast= nj.voxtoast()[0]
		toast.on "close", ()->
			nj.remove()
		toast.open()
	

	@info: (msg)->
		jtoast= $(".info.template")
		return Message.createToast(jtoast, msg)
	

	@warning: (msg)->
		jtoast= $(".warning.template")
		return Message.createToast(jtoast, msg)
	

	@error: (msg)->
		jtoast= $(".error.template")
		return Message.createToast(jtoast, msg)
	



	@confirm: (options)->
		m= $(".modal-confirm")
		modalScope= core.dynvox.v2.Scope.get("modal")
		options.text= options.text or options.msg

		if not options.confirmText
			options.confirmText= options.defaults.confirmText
		if not options.cancelText
			options.cancelText=  options.defaults.cancelText

		if not options.cancel
			options.cancel= ()->
				m.voxmodal()[0].close()
			
		
		modalScope.modalConfirm=options
		m.voxmodal()[0].open()

export default Message
