#global $
G= require(__dirname + "/../default").WebSite
U= require(__dirname + "/../default").Util

class MessageManager extends U.Module

	info: ->
		G.Messages.info.apply(G.Messages, arguments)
	
	warning: ->
		G.Messages.warning.apply(G.Messages, arguments)
	
	error: ->
		G.Messages.error.apply(G.Messages, arguments)
		

	confirmClose: ->
		m= $(".modal-confirm")
		m.voxmodal()[0].close()
	


	getDefaults: ->
		idioma= this.app.configuration.idiomaActual
		return 
			"confirmText": "Confirmar",
			"cancelText": "Cancelar"
		
	
	confirm: (args...)->
		args[0]= args[0]||{}
		args[0].defaults= this.getDefaults()
		G.Messages.confirm.apply(G.Messages, args)
	

export default MessageManager
