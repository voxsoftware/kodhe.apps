/*global core,$*/
var G= global.program.WebSite
var U= global.program.Util


class MainController extends U.Module{

	async ui(){
		var scope= this.app.scope
		scope.arguments= window.app
		console.info("Module: " + MainController.name)
		this.attachEvents()
		$(".await-action").removeAttr("disabled")
		this.hashChanged()
	}


	hashChanged(){
		var scope= this.app.scope
		scope.prefix=global.prefix
		this.hashArgs= this.app.getParameters()
		this.getFolder()
	}
	
	
	async getFolder(){
		var scope= this.app.scope 
		
		var data= await this.app.remote.get("function/c/dir.info", {})
		scope.folder= data.appFolder
		if(!scope.app1)
			scope.app1={}	
		
		var apps= await this.app.remote.get("function/c/app.list", {})
		for(var i=0;i<apps.length;i++){
			if(!apps[i].iconclass){
				apps[i].iconclass= 'fa text-yellow darken-1 fa-rocket'
			}
		}
		scope.apps= scope.apps||[]
		if(scope.apps &&scope.apps.length>0){
			scope.apps.splice(0,scope.apps.length)
		}
		for(var i=0;i<apps.length;i++){
			scope.apps.push(apps[i])
		}
		
		scope.apps_length= apps.length
		setTimeout(this.getFolder.bind(this), 12000)
	}


	/* /// /// REGIÓN EVENTS /// /// */
	attachEvents(){
		var scope= this.app.scope, self=this
		scope.opcion=0 
		scope.opcion0= function(){
			scope.opcion=0 
		}
		scope.ejecutarApp= function(ev){
			if(ev && ev.preventDefault)
				ev.preventDefault()
			self.ejecutarApp($(this))
		}
		scope.ejecutarApp1= function(ev){
			if(ev && ev.preventDefault)
				ev.preventDefault()
			self.ejecutarApp1()
		}
		
		scope.cancelar=function(){
			scope.estado=0
		}
		scope.instalarApp=function(){
			scope.estado=1
		}
		
	}
	
	async ejecutarApp1(){
		var scope= this.app.scope
		await this._ejecutarApp(scope.app1)
		scope.estado=0
	}
	
	
	async ejecutarApp(a ){
		var pscope= a.voxscope()
		var app= pscope.app 
		return await this._ejecutarApp(app)
	}
	async _ejecutarApp(app){
		try{
			app.waiting= true
			
			if(!app.name && !app.link)
				throw new core.System.Exception("Invalid URL for install application")
			
			await this.app.remote.post("function/c/app.check", app)
			global._require("electron").remote.getCurrentWindow().minimize()
			
		}catch(e){
			this.app.messageManager.error(e.message)
		}
		finally{
			app.waiting= false
		}
	}
	/* /// /// REGIÓN EVENTS /// /// */


}
export default MainController
