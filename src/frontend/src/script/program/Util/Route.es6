
var exe= (code)=>{
	eval(code)
}


// Esta clase hace posible que se use el proyecto
// como simplemente archivos estáticos
// pero manteniendo unas rutas más agradables para el usuario

// Esto es algo simple por medio de location.search, redirecciona al archivo
// correspondiente ....
class Route{

	async tryAnalize(defaultRoute){
		try{
			await this.analize(defaultRoute)
		}
		catch(e){
			console.error("Error al analizar la ruta: ", e)
		}
	}

	async  analize(defaultRoute){

		var search= location.search.substring(1)
		var items= search.split("&"), item, par
		var object= {}


		for(var i=0;i<items.length;i++){
			item= items[i]
			par= item.split("=")
			object[par[0]]= par[1]
		}

		var keys= Object.keys(object)
		var route= keys[0]
		if(!route && defaultRoute)
			route= defaultRoute


		if(route){
			await this.to(route+".html")
		}
	}


	async to(view){

		var req= new core.VW.Http.Request(view)
		req.async= true
		var res= await req.getResponseAsync()
		this.processHtml(res.body)
	}



	processHtml(html){

		var contenido= html.replace(/(\<html\s+|\<\/html\>|\<head\s+|\<\/head\>|\<body\s+|\<\/body\>)/ig, function(v){
			v= v.toLowerCase()
			//console.info(v)
			if(v.indexOf("html")>=0)
				return v.replace("html", "vhtml")

			if(v.indexOf("head")>=0)
				return v.replace("head", "vhead")

			if(v.indexOf("body")>=0)
				return v.replace("body", "vbody")
		})

		//console.info(contenido)
		var jq= $(contenido)
		var body= jq.find("vbody>*")
		window.pe= body

		for(var i=0;i<body.length;i++){
			$("body").append(body.eq(i))
		}


		document.head.innerHTML= jq.find("vhead").html()
		var scripts= $("script")
		scripts.each(function(){
			var s= $(this)
			var code= s.html()
			if(code)
				exe(code)
		})


	}

}
export default Route
