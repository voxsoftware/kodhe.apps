

class Datetime
	constructor: (moment)->
		@moment= moment
	

	toString: ()->
		@.moment.format(@format  || "DD/MM/YYYY HH:mm:ss ô Z").replace("ô","GMT")
	

	isValid: ->
		@moment.isValid()
	
export default Datetime
