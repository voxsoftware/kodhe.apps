
import {EventEmitter} from 'events'
class Module extends EventEmitter

	constructor: (app)->
		super()
		this._app= app
	


	__get_app: ->
		if @aborted
			throw new core.System.Exception("El módulo al que intenta acceder ha sido abortado."+
				"Para prevenir colisión de información entre módulos ya no puede seguir accediendo a él")
		@_app
	


	abort: ->
		@aborted= yes 
		@_app.deleteFromCache(@)
	
	
	dispose: ->
		for own id of @
			delete @[id]
	
	global.resolveProps @, @::

export default Module
