var v= core.org.kodhe.Util
class Remote extends v.Module{
	async request(method, module, args, events={}){

		var u= this.app.uiUrl
		if(u[u.length-1]=="/")
			u= u.substring(0, u.length-1)


		if(!u)
			u= location.origin


		var url= (events.prefix?events.prefix:this.app.apiUrl + "/") + module + "?src-url=" + encodeURIComponent(u)
		var req
		if(method.toUpperCase()!="GET"){
			req= new core.VW.Http.Request(url)
		}


		if(args && (typeof args.submit == "function")){
			// Significa que es un formulario jQuery
			if(method.toUpperCase()=="GET")
				args= this.app.formArgs(args)
			else{
				req.form= args
			}
		}

		if(args && (typeof args.submit != "function")){

			if(method.toUpperCase()=="GET"){
				for(var id in args){
					url+= "&" + this.encodeURIComponent(id, args[id])
				}
			}
			else{
				req.contentType= "application/json"
				req.body= args
			}
		}

		if(method.toUpperCase()=="GET")
			req= new core.VW.Http.Request(url)
		req.method=method
		req.withCredentials= events.credentials!==undefined?events.credentials: true
		req.nowait= this._nowait
		req.events= events
		req.timeout=99999999999
		this._nowait= false
		this.emit("request",req)
		this.currentRequest= req


		var ee, data, task

		try{
			task= core.VW.Web.Vox.platform.getJsonResponseAsync(req)
			/*if(progress && req.innerRequest.upload){
				req.innerRequest.upload.onprogress=progress
			}*/
			data= await task
			this.procesar(data)
		}
		catch(e){
			ee= e
		}
		this.emit("request:complete",req)
		if(ee)
			throw ee
		return data
	}

	encodeURIComponent(id, arg){
		var str=''
		if(arg instanceof Array){
			for(var i=0;i<arg.length;i++){
				if(i>0)
					str+="&"
				str+= this.encodeURIComponent(id+"[]", arg[i])
			}
		}
		else{
			str= id + "=" + encodeURIComponent(arg)
		}
		return str
	}

	procesar(data){
		var l, d
		if(typeof data=="string"){

			var date
			dr= data.split("-")
			if(dr.length>2 && !isNaN(parseInt(dr[0]))){
				date= new Date(data)
			}
			if(date && !isNaN(date.getTime())){
				data= new v.Datetime(core.VW.Moment(date))
			}

			else{
				l= data.split(" ")
				if(l.length==2){
					d= l[0].split("-")
					if(d.length==3){
						d= l[1].split(":")
						if(d.length==3){
							if(data=="0000-00-00 00:00:00"){

								data=undefined
							}
							else{
								d= new v.Datetime(core.VW.Moment(data+"Z"))
								if(d.isValid())
									data= d
							}
						}
					}
				}
			}

		}
		else if(typeof data=="object"){
			for(var id in data){
				data[id]= this.procesar(data[id])
			}

			if(data instanceof Array){
				for(var y=0;y<data.length;y++){
					data[y]= this.procesar(data[y])
				}
			}
		}

		return data
	}

	get nowait(){
		this._nowait= true
		return this
	}

	get(module, args, options){
		return this.request("GET", module, args, options)
	}

	post(module, args, options){
		return this.request("POST", module, args, options)
	}

	put(module, args, options){
		return this.request("PUT", module, args, options)
	}

	patch(module, args, options){
		return this.request("PATCH", module, args, options)
	}

	delete(module, args, options){
		return this.request("DELETE", module, args, options)
	}
}
export default Remote
