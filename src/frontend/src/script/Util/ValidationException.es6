class ValidationException extends core.System.Exception{
	constructor(message, field, innerException){
		this.field= field
		super(message, innerException)
	}
}
export default ValidationException