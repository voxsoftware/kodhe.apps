

class Datetime{
	constructor(moment){
		this.moment= moment
	}

	toString(){
		return this.moment.format("DD/MM/YYYY HH:mm:ss ô Z").replace("ô","GMT")
	}

	isValid(){
		return this.moment.isValid()
	}
}

export default Datetime