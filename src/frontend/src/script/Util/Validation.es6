
var U= core.org.kodhe.Util
var G= core.org.kodhe.WebSite
var v= U
class Validation{

	constructor(args){
		this.rules={}
		this.getRules(args)

	}


	getRules(args){
		var arg, f
		for(var id in args){
			arg= args[id]
			//if(typeof arg!=="function")
			//	arg= Validation[arg.toString()]
			this.rules[id]= arg
		}

	}

	getFunction(rule){
		if(typeof rule.func=="function")
			return rule.func
		if(typeof rule == "function")
			return rule

		return Validation[rule.func || rule]

	}


	validate(args){
		var f, g
		for(var id in this.rules){
			f= this.rules[id]
			g= this.getFunction(f)
			if(f){
				args[id]=g(id, args[id], f)
			}
		}

		return args
	}


	static ["!empty"](id, value, rule){
		if(!value)
			throw new v.ValidationException((rule.name||id) + " no puede estar vacío", id)
		return value
	}

	static hour(id, value, rule){

		var h= core.VW.Moment("01/01/2000 " + value, "DD/MM/YYYY HH:mm")
		if(!h.isValid())
			throw new v.ValidationException((rule.name||id) + " debe ser hora",id)

		return value
	}

	static dateDMY(id, value, rule){

		if(rule && rule.optional && !value)
			return value

		var h= core.VW.Moment(value, "DD/MM/YYYY HH:mm")
		if(!h.isValid())
			throw new v.ValidationException((rule.name||id) + " debe ser fecha",id)

		return h.toDate()
	}


	static date(id, value, rule){

		if(rule && rule.optional && !value)
			return value

		var d= new Date(value)
		if(isNaN(d.getTime()))
			throw new v.ValidationException((rule.name||id) + " debe ser una fecha",id)

		return value
	}


	static email(id, value, rule){
		if (!/^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/i.test(value))
			throw new v.ValidationException((rule.name||id) + " debe ser una dirección de correo válida",id)

		return value
	}


	static password(id, value, rule){
		if(!value || value.length<6)
			throw new v.ValidationException((rule.name||id) + " debe ser de al menos 6 caracteres")

		return value
	}

	static number(id, value, rule){
		value= parseInt(value)
		if(isNaN(value))
			throw new v.ValidationException((rule.name||id) + " debe ser un número")

		return value
	}

}
export default Validation
