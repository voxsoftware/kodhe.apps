
import {EventEmitter} from 'events'
class Module extends EventEmitter{

	constructor(app){
		this._app= app
	}


	get app(){
		if(this.aborted)
			throw new core.System.Exception("El módulo al que intenta acceder ha sido abortado."+
				"Para prevenir colisión de información entre módulos ya no puede seguir accediendo a él")
		return this._app
	}


	abort(){
		this.aborted= true
		this._app.deleteFromCache(this)
	}




}
export default Module
