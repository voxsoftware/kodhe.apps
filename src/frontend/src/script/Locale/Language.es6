var L= core.org.kodhe.Locale
class Language extends L.Formateable{

	get lngInfo(){
		return this.$lngInfo
	}


	load(){
		this.addObject(this.lngInfo)
	}

	static supported(){
		var items=[]
		for(var id in L){
			if(id!="Language" && id!="Formateable"){
				items.push({
					"code": id,
					"iso": L[id].Language.iso,
					"name": L[id].Language.name
				})
			}
		}
		return items
	}

	param(str, args){
		var self= this
		str= str[0].toUpperCase() + str.substring(1)
		var json, part,parts= str.split(/(\:|\.)/ig)

		json= this.lngInfo
		for(var i=0;i<parts.length;i++){
			part= parts[i]
			if(part!=":" && part!="."){
				json= json[part]
				if(!json)
					break
			}
		}

		if(!json)
			return


		return this.format(json, args)
	}

	format(json, args){
		var reg= /\{\d\}/ig
		json= json.replace(reg, function(s){
			var num= s.substring(1, s.length-1)|0
			return args[num]||""
		})

		reg= /\{[A-Za-z]+\:.*\}/gi
		json= json.replace(reg, (s)=>{
			var num= s.substring(1, s.length-1)
			return this.param(num)
		})
		return json
	}


	formatException(e){
		e= e.innerException || e
		var type= e.type, msg= e.message
		if(this.comun.errores[type])
			msg= this.param("comun.errores."+type, [msg])

		return this.param("comun.mensajes.error",[msg])
	}

}
export default Language
