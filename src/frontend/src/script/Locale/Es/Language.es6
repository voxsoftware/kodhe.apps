var L= core.org.kodhe.Locale
class ESLanguage extends L.Language{

	constructor(){
		this.$lngInfo= L.Es.Data
		this.load()
	}

	get name(){
		return ESLanguage.name
	}

	get iso(){
		return ESLanguage.iso
	}


	static get name(){
		return "Español"
	}

	static get iso(){
		return "es"
	}
}
export default ESLanguage
