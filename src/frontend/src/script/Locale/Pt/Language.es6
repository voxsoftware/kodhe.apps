var L= core.org.kodhe.Locale
class PTLanguage extends L.Language{

	constructor(){
		this.$lngInfo= L.Pt.Data
		this.load()
	}

	get name(){
		return PTLanguage.name
	}

	get iso(){
		return PTLanguage.iso
	}


	static get name(){
		return "Português"
	}

	static get iso(){
		return "pt"
	}
}
export default PTLanguage
