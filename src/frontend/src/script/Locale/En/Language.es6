var L= core.org.kodhe.Locale
class ENLanguage extends L.Language{

	constructor(){
		this.$lngInfo= L.En.Data
		this.load()
	}

	get name(){
		return ENLanguage.name
	}

	get iso(){
		return ENLanguage.iso
	}


	static get name(){
		return "English"
	}

	static get iso(){
		return "en"
	}
}
export default ENLanguage
