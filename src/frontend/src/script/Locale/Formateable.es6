
class Formateable{

	constructor(language){
		this.$lng= language
	}

	get language(){
		return this.$lng
	}

	_createGetter(value){
		return ()=>{
			return this.format ? this.format(value, []) : this.language.format(value, [])
		}
	}


	addObject(json){
		var form
		for(var id in json){
			value= json[id]
			if(typeof value==="object"){
				form= new Formateable(this.language||this)
				form.addObject(value)
				this[id]= form
				if(id != id.toLowerCase())
					this[id.toLowerCase()]= form
			}
			else{
				this.__defineGetter__(id, this._createGetter(value))
				if(id != id.toLowerCase())
					this.__defineGetter__(id.toLowerCase(), this._createGetter(value))
			}
		}
	}

}
export default Formateable
