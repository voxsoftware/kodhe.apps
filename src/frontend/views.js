


var Path= require("path")
var ViewCompiler, compileTools, file
var Fs= core.System.IO.Fs

file= Path.join(__dirname, "context.json")
if(!Fs.sync.exists(file))
	file= Path.join(__dirname, "context.web.json")
var init= function(){

	try{
		compileTools= require("develop-tools")
		ViewCompiler=  compileTools.Tools.ViewCompiler
	}
	catch(e){
		throw new core.System.Exception("Revise que tenga en sus archivos el siguiente repositorio: " +
			"https://gitlab.com/voxsoftware/develop-tools.git " + e.message, e)
	}

	var outpath= require("./outpath").default

	var viewCompiler= new ViewCompiler(Path.join(__dirname, "src", "views"))
	compileTools.Tools.Es6HtmlLoader.env= require(file)
	viewCompiler.process(outpath)
	return viewCompiler.getCompileConfig()


}


module.exports= init()
