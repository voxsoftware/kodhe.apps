var Path= require("path");

var styleloader= require.resolve("vox-webcompiler/node_modules/style-loader")
var cssloader= require.resolve("vox-webcompiler/node_modules/css-loader")
var lessloader= require.resolve("vox-webcompiler/node_modules/less-loader")
var fileloader= require.resolve("vox-webcompiler/node_modules/file-loader")
var ExtractTextPlugin = require("vox-webcompiler/node_modules/extract-text-webpack-plugin");
var minimal= core.VW.Web.Compiler.minimal

var outp= require("./outpath").default

var cssExtract= new ExtractTextPlugin(minimal? "css/vprinter.min.css": "css/vprinter.css");
module.exports = {

	name: "default",
	module: {
	    loaders: [
	        {
	        	test: /\.less$/,
	        	loader: cssExtract.extract(styleloader, cssloader+"!"+lessloader)
	        },
	        { test: /.*\.(gif|jpeg|jpg|png)$/, loader: fileloader+"?hash=sha512&digest=ext&size=16&name=img/[name].[ext]" },
	        { test: /.*\.(eot|woff|eot|woff2|ttf|svg.*)/, loader: fileloader+"?hash=sha512&digest=ext&size=16&name=fonts/[name].[ext]" }

	    ]
	},
    entry:  Path.normalize(__dirname + "/src/style/main.less"),
    output: {
        path:  outp, // __dirname + "/dist",
        "publicPath": "../",
        filename: "temp.js",
        chunkFilename: "[id].js"
    },
    plugins: [
        cssExtract
    ]
}
