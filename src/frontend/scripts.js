
// Archivo para compilar el front-end con vox-webcompiler
var Path= require("path")
var out= Path.join(__dirname, 'out', 'script')
var out2= Path.join(__dirname, 'out', 'script2')
var inPath= Path.join(__dirname, 'src', 'script')
var inPath2= Path.join(__dirname, 'src', 'script2')
var minimal= core.VW.Web.Compiler.minimal
var path= Path.join(out, 'main.js')
var outp= require("./outpath").default




// Utilizar develop-tools para compilar ...
var init= function(){

	try{
		compileTools= require("develop-tools")
		Es6Transpiler=  compileTools.Tools.Es6Transpiler
	}
	catch(e){
		throw new core.System.Exception("Revise que tenga en sus archivos el siguiente repositorio: " +
			"https://gitlab.com/voxsoftware/develop-tools.git " + e.message, e)
	}


	var async= false
	var scriptCompiler= new Es6Transpiler()
	var scriptCompiler2= new Es6Transpiler()

	if(!async){
		scriptCompiler.procesar(inPath, out)
		scriptCompiler2.procesar(inPath2, out2)
		return [
			scriptCompiler.getCompileConfig({
				"input": "main.js",
				"name": minimal? "js/vprinter.min.js": "js/vprinter.js",
				output: outp
			}),
			scriptCompiler2.getCompileConfig({
				"input": "main.js",
				"name": minimal? "js/system.int64.min.js": "js/system.int64.js",
				output: outp
			})
		]


	}
	else{
		scriptCompiler.procesarAsync(inPath, out)
		scriptCompiler2.procesarAsync(inPath2, out2)
		return [
			scriptCompiler.getCompileConfigAsync({
				"input": "main.es6",
				"name": minimal? "js/vprinter.min.js": "js/vprinter.js",
				output: outp
			})
			,scriptCompiler2.getCompileConfigAsync({
				"input": "main.es6",
				"name": minimal? "js/system.int64.min.js": "js/system.int64.js",
				output: outp
			})
		]

	}


}


module.exports= init()
