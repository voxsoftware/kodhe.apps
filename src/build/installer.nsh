!macro customUnInstall
  Delete "$SMSTARTUP\KodheApps.lnk"
!macroend
!macro customInstall
  CreateShortCut "$SMSTARTUP\KodheApps.lnk" "$INSTDIR\KodheApps.exe" "--hide --startup" ""
  #!insertmacro ExecWaitJob '"$INSTDIR\NoTicket.exe -install"'
  #ExecShell "" "$INSTDIR\KodheApps.exe" "-install"
!macroend
