/*global core*/
var Child = require('child_process')
var Path = require("path")
var fs= require("fs")

if(!process.env.VOX_CORE_START){
	process.env.VOX_CORE_START= Path.join(__dirname, "dependencies", "vox-core", "main.js")
}

/*
if(!global.__nocheck){
	require(process.env.VOX_CORE_START)
}*/






var major , version , cversion, appFolder

var cw= require(__dirname + "/config/args")
if(!global._defaultArgs){
	global._defaultArgs= cw
}


var home = require("os").homedir()
var documentFolder = Path.normalize(home + '/Documents/')
var realAppFolder= Path.join(documentFolder, cw.folder)
appFolder = Path.join(documentFolder, cw.folder, 'org.kodhe.apps')
var exeFile = Path.join(documentFolder, cw.folder, 'org.kodhe.apps.run')
var newexe
process.env.BASE_DIR=appFolder 


if(!global.__nocheck){

	try{
		// esto está pensando por si se quiere realizar 
		// actualizaciones de la base en windows sin necesidad 
		// de interacción del usuario 
		newexe= fs.readFileSync(exeFile, 'utf8')
		if(newexe){
			newexe= newexe.trim()
			var p = Child.spawn(newexe, process.argv.slice(1), { detached: true })
			p.unref()
			process.exit()
			return 
		}
	}
	catch(e){}


	if (fs.existsSync(__dirname + "/dependencies/kowix/cli.js")) {
		global.kwFile_1 = __dirname + "/dependencies/kowix/cli"
		global.kwFile_2 = __dirname + "/dependencies/kowix/cli"
	} else {
		global.kwFile_1 = __dirname + "/dependencies/kowix/manualstart"
		global.kwFile_2 = __dirname + "/dependencies/kowix/manualstart"
	}
	
	
	
	
	process.env.KW_FILE_1= global.kwFile_1
	process.env.KW_FILE_2= global.kwFile_2
	
	
	
	if(appFolder &&  fs.existsSync(appFolder)){
		
		version= Path.join(appFolder, "version")
		cversion= fs.readFileSync(Path.join(__dirname, "version"), 'utf8')
		global.installedVersion= cversion 
		
		
		try{
			if(fs.existsSync(version)){
				version= fs.readFileSync(version, 'utf8')
				
				if(cversion<=version)
					major= true 
			}
			else{
				version=null 
			}
		}catch(e){}
		
		
		if(major){
			process.env.DEFAULT_DIR = appFolder 
			console.info("Using folder: ", appFolder)
		}else{
			console.info("Folder con versión menor a la instalada: ", appFolder)
		}
		
	}
	if(!major){
		process.env.DEFAULT_DIR = __dirname
	}
	else{
		global.__nocheck = true	
		try{	
			require(Path.join(appFolder,"electron.js"))
			return 
		}catch(e){
			// ups failing open 
			console.error("Failed opening application from folder in Documents: ", e)
		}
	}

}
else{
	var dir1= __dirname + "/dependencies/kowix"
	if(fs.existsSync(dir1)){
		global.kwFile_1= __dirname + "/dependencies/kowix/cli"		
		global.kwFile_2= __dirname + "/dependencies/kowix/cli"	
	}
}


if (!global.core) {
	var vpath = Path.join(__dirname, "dependencies", "vox-core","main.js")
	if (fs.existsSync(vpath)) {
		if (Path.normalize(process.env.VOX_CORE_START) != vpath) {
			if (!global.core || typeof regeneratorRuntime == "undefined") {
				// version vieja de vox-core
				// cargar la nueva 
				delete global.core
				process.env.VOX_CORE_START = vpath
			}
		}
	}
}



if(!global.core){
	require(process.env.VOX_CORE_START)
}



const electron = require('electron')
// Module to control application life.
const app = electron.app

var single= function(commandLine, workingDirectory, macosActivate) {
	
	console.log("second instance:",commandLine)
	var port=require(__dirname + "/config/0port")
	var req= new core.VW.Http.Request("http://127.0.0.1:"+port+"/api/function/c/app.boot.cmd")	
	req.body={
		commandLine,
		workingDirectory,
		macosActivate
	}
	req.contentType='application/json'
	req.method='POST'
	req.analizeResponse= false 
	req.getResponseAsync()
	
	
}
var shouldQuit
if(typeof app.makeSingleInstance == 'function'){
	shouldQuit= app.makeSingleInstance(single);
}
else{
	// new method 
	shouldQuit= !app.requestSingleInstanceLock()	
	if(!shouldQuit)
		app.on('second-instance', (event, argv, cwd) =>  single(argv, cwd) )
}
app.on('activate', () => single([], __dirname, true))

if (shouldQuit) {
	app.quit();
	process.exit();
}



if (true) {


	
    if(typeof regeneratorRuntime == "undefined"){
		console.log("WARNING: Old version vox-core loaded")
		var reg = core.VW.Ecma2015.Parser
	}

	// executing data fork 
	var other = function(port, reopen, task) {

		var env = {},
			closed
		for (var id in process.env) {
			env[id] = process.env[id]
		}
		env.INTERFACE = "false"
		
		if (!env.SERVER_PORT) {
			env.SERVER_PORT = cw.port || "49602"
		}
		
		env.ELECTRON_RUN_AS_NODE = "1"
		env.SERVER_PORT =  port===undefined ? (parseInt(env.SERVER_PORT) + 1).toString() : port

		//console.info("execpath: ",process.execPath)

		var file = "node-dev.js"
		if (global.PROD_MODE) {
			file = "node.js"
		}
		
		var wn
		var p = Child.spawn(process.execPath, [__dirname + "/" + file], {
			env
		})

		p.stdout.on("data", function(b) {
			process.stdout.write("PID [" + p.pid + "] STDOUT: ")
			
			
			if(wn){
				if(/\d/.test(b.toString()[0])){
					port= parseInt(b.toString().trim())
					task.result= port
					task.finish()
					task=null
					wn= false
				}
			}
			
			else if(task){
				if(b.toString().toUpperCase().indexOf("SERVER HTTP")>=0 &&
					b.toString().toUpperCase().indexOf("127.0.0.1")>=0){
					
					
					var port=0 
					var y= b.toString().lastIndexOf(":")
					port= b.toString().substring(y+1)
					if(!port){
						wn= true
						return 
					}
					//console.info("NEW SERVER?? ", port)
					//process.exit() 
					
					port= parseInt(port)
					task.result= port
					task.finish()
					task=null
				}
			}
			process.stdout.write(b)
		})

		p.stderr.on("data", function(b) {
			process.stderr.write("PID [" + p.pid + "] STDERR: ")
			process.stderr.write(b)
		})


		p.on("exit", function() {
			if (!closed && reopen && !p._force)
				process.nextTick(other)
		})
		
		p.on("error",function(er){
			if(task){
				task.exception= er 
				task.finish()
				task=null
			}
		})

		process.on("exit", function() {
			closed = true
			p.kill()

			setTimeout(function() {
				p.kill('SIGKILL')
			}, 100)

		})
		
		return p 
	}
	global.startNewProcess= other 
	//other(undefined, true, new core.VW.Task())


}




if (process.env.INTERFACE !== "false") {
	
	
	
	app.on('ready', function(){
		var g= function(){
			if(!global.createWindow){
				return setTimeout(g, 10)		
			}
			global.createWindow()
		}
		g()
	})
	app.on('activate', function() {
		// On OS X it's common to re-create a window in the app when the
		// dock icon is clicked and there are no other windows open.
		var g= function(){
			if(!global.createWindow){
				return setTimeout(g, 10)		
			}
			global.createWindow()
		}
		g()
	})

	global.continueElectron=function(){
		//var VaravelNamespace = core.org.voxsoftware.Varavel
		var port  = require(__dirname + "/config/0port")
		var oourl = "http://127.0.0.1:" + port + "/"
	
	
		var req = new core.VW.Http.Request(oourl + "api/function/c/app.boot")
		req.analizeResponse = false
		req.method='POST'
		req.contentType='application/json'
		//req.body=startArgs
		
		
		req.getResponseAsync()
	}


}




global.serverStarted = global.continueElectron

if(cw.port)
	process.env.SERVER_PORT= cw.port
	

if (global.PROD_MODE)
	require(global.kwFile_1)
else
	require(global.kwFile_2)