import Child from 'child_process'
import Os from 'os'
var Fs= core.System.IO.Fs


var arch= process.argv[3]
if(!arch)
	arch= process.arch
	
var iswin= process.argv[4] && process.argv[4].indexOf("--win")>=0

if(arch && arch.startsWith("--"))
	arch= arch.substring(2)




//Child.execSync("npm update")
require(__dirname+"/env.es6")



Child.execSync(`node ${__dirname}/src/dependencies/vox-core/cli -transpile --in ${__dirname}/src/functions --out ${__dirname}/src/functions-compiled`)
//Child.execSync(`node ${__dirname}/src/vox-core/cli -transpile --in ${__dirname}/src/backend/Modules --out ${__dirname}/src/backend-dist/Modules`)
//Child.execSync(`node ${__dirname}/src/vox-core/cli -transpile --in ${__dirname}/src/win32_lib --out ${__dirname}/src/win32_lib`)




import Path from 'path'
var dirini= Path.join(__dirname,"src")
var dirfin= Path.join(__dirname,"src-out")
var fs= require("fs-extra")
var appid= "kodhe-apps"


var all= async ()=>{
	
	var files1= await Fs.async.readdir(dirini)
	var files2= await Fs.async.readdir(dirfin)
	var stat
	for(var i=0;i<files2.length;i++){
		if(files2[i]!=".gitignore")
			await fs.remove(Path.join(dirfin,files2[i]))
	}
	var h
	for(var i=0;i<files1.length;i++){
		if(files1[i]=="functions-compiled"){
			await fs.copy(Path.join(dirini, files1[i]), Path.join(dirfin, "functions"))
			await fs.copy(Path.join(dirini, "functions", "vars.json"), Path.join(dirfin, "functions","vars.json"))
		}
		else if(files1[i]=="dependencies"){
			
			
			
			var of1= Path.join(dirfin, "dependencies")
			if(!fs.existsSync(of1))
				fs.mkdirSync(of1)
				
			await fs.copy(Path.join(dirini, files1[i], "kowix"), Path.join(dirfin, files1[i], "kowix"))
			//await fs.copy(Path.join(dirini, files1[i], "native-deps"), Path.join(dirfin, files1[i], "native-deps"))
			//await fs.copy(Path.join(dirini, files1[i], "varavel"), Path.join(dirfin, files1[i], "varavel"))
			await fs.copy(Path.join(dirini, files1[i], "vox-core"), Path.join(dirfin, files1[i], "vox-core"))
			await fs.copy(Path.join(dirini, files1[i], "node_modules"), Path.join(dirfin, files1[i], "node_modules"))
			await fs.copy(Path.join(dirini, files1[i], "package.json"), Path.join(dirfin, files1[i], "package.json"))
			await fs.copy(Path.join(dirini, files1[i], "kowix.json"), Path.join(dirfin, files1[i], "kowix.json"))
			
				
			await fs.copy(Path.join(dirini, "..", "native-deps", arch), Path.join(dirfin,  "dependencies", "native-deps"))
		}
		
		else if(files1[i]=="frontend"){
			var of1= Path.join(dirfin, "frontend")
			if(!fs.existsSync(of1))
				fs.mkdirSync(of1)
			
			of1=Path.join(of1, "src")
			if(!fs.existsSync(of1))
				fs.mkdirSync(of1)
				
			await fs.copy(Path.join(dirini, "frontend", "src","views"), Path.join(dirfin, "frontend","src","views"))
		}
		/*
		else if(files1[i]=="install"){
			await fs.copy(Path.join(dirini, "..", "driver", arch), Path.join(dirfin, "install"))
		}
		else if(files1[i]=="bin"){
			//h= Os.platform()=="win32"? "win": (Os.platform()=="linux"? "linux64": "osx")
			await fs.copy(Path.join(dirini, "..", "bin"), Path.join(dirfin, "bin"))
		}*/
		
		else if(files1[i]!="functions" && files1[i]!=".gitignore"/* && files1[i]!="node_modules"*/){
			if(files1[i]=="win32_lib"){
				await fs.mkdir(Path.join(dirfin, files1[i]))
				await fs.copy(Path.join(dirini, files1[i]), Path.join(dirfin, files1[i]))
			}
			else {
				await fs.copy(Path.join(dirini, files1[i]), Path.join(dirfin, files1[i]))
			}
		}
	}
	
	// disable obfuscate
	//await require(__dirname+"/obfuscate")
	
	
	var find=function(path ){
		var PATH= process.env.PATH 
		var PATHS= PATH.split(";")
		var p
		for(var i=0;i<PATHS.length;i++){
			PATH= PATHS[i]
			if(Os.platform()!="win32"){
				p= Path.join(PATH, path)
				if(Fs.sync.exists(p))
					return p
			}
			else{
				p= Path.join(PATH, Path.basename(path,".exe") + ".exe")
				if(Fs.sync.exists(p))
					return p
				
				p= Path.join(PATH, Path.basename(path,".exe") + ".cmd")
				if(Fs.sync.exists(p))
					return p
				
				p= Path.join(PATH, Path.basename(path,".exe") + ".bat")
				if(Fs.sync.exists(p))
					return p
			}
		}
		if(Os.platform()!="win32"){
			return path
		}
		throw new core.System.Exception("File not found: "+path)
	}
	
	if(arch=="x64")
		Child.execSync(JSON.stringify(find("yarn"))+  " dist-"+(iswin?"win-":"")+"64")
	else 
		Child.execSync(JSON.stringify(find("yarn"))+ " dist-"+(iswin?"win-":"")+"32")
		
		
	/*
	
	
	var distUpdate
	distUpdate= __dirname+"/dist-update/win-"+arch
	if(!Fs.sync.exists(distUpdate))
		Fs.sync.mkdir(distUpdate)
		
		
	if(Fs.sync.exists(distUpdate +"/KodheApps-Setup-"+arch+".exe"))
		Fs.sync.unlink(distUpdate +"/KodheApps-Setup-"+arch+".exe")
	
	if(Fs.sync.exists(__dirname+"/dist/KodheApps Setup "+require("./src/package.json").version+".exe"))
		Fs.sync.rename(__dirname+"/dist/KodheApps Setup "+require("./src/package.json").version+".exe", distUpdate +"/KodheApps-Setup-"+arch+".exe")
	
	*/
	
	var distUpdate
	if(Os.platform()=="win32" || iswin){
		distUpdate= __dirname+"/dist-update/win-"+arch
		if(!Fs.sync.exists(distUpdate))
			Fs.sync.mkdir(distUpdate)
			
			
		
		if(Fs.sync.exists(distUpdate +"/KodheApps-Setup-"+arch+".exe"))
			await fs.remove(distUpdate +"/KodheApps-Setup-"+arch+".exe")
			
			
		if(Fs.sync.exists(__dirname+"/dist/KodheApps Setup "+require("./src/package.json").version+".exe"))
			Fs.sync.rename(__dirname+"/dist/KodheApps Setup "+require("./src/package.json").version+".exe", distUpdate +"/KodheApps-Setup-"+arch+".exe")
	}
	else if(Os.platform()=="darwin"){
		distUpdate= __dirname+"/dist-update/darwin"
		if(!Fs.sync.exists(distUpdate))
			Fs.sync.mkdir(distUpdate)
			
			
		
		if(Fs.sync.exists(distUpdate +"/KodheApps.dmg"))
			await fs.remove(distUpdate +"/KodheApps.dmg")
		
		if(Fs.sync.exists(__dirname+"/dist/KodheApps-"+require("./src/package.json").version+".dmg"))
			Fs.sync.rename(__dirname+"/dist/KodheApps-"+require("./src/package.json").version+".dmg", distUpdate +"/KodheApps.dmg")
		
	}
	else if(Os.platform()=="linux"){
		distUpdate= __dirname+"/dist-update/linux-" + arch
		if(!Fs.sync.exists(distUpdate))
			Fs.sync.mkdir(distUpdate)
			
		
		
		var deb1= arch=="x64" ? "amd64" : "i386"
		if(Fs.sync.exists(distUpdate +"/KodheApps-"+deb1+".deb"))
			await fs.remove(distUpdate +"/KodheApps-"+deb1+".deb")
			
			
		if(Fs.sync.exists(__dirname+"/dist/"+appid+"_"+require("./src/package.json").version+"_"+ deb1 +".deb"))
			Fs.sync.rename(__dirname+"/dist/"+appid+"_"+require("./src/package.json").version+"_"+ deb1 +".deb", distUpdate +"/KodheApps-"+deb1+".deb")
		
		if (Fs.sync.exists(__dirname + "/dist/KodheApps "+ require("./src/package.json").version + ".AppImage"))
			Fs.sync.rename(__dirname + "/dist/KodheApps " + require("./src/package.json").version + ".AppImage", distUpdate + "/KodheApps")
		
	}
	
	
	
}

all()